<?php

namespace Lerp\Purchase;

use Laminas\I18n\Translator\Loader\PhpArray;
use Lerp\Purchase\Controller\Ajax\PurchaseOrder\PurchaseOrderMailAjaxController;
use Lerp\Purchase\Controller\Ajax\PurchaseRequest\PurchaseRequestMailAjaxController;
use Lerp\Purchase\Controller\Ajax\SupplierPurchaseAjaxController;
use Lerp\Purchase\Controller\Rest\PurchaseOrder\Files\FilePurchaseOrderRestController;
use Lerp\Purchase\Controller\Rest\PurchaseRequest\Files\FilePurchaseRequestRestController;
use Lerp\Purchase\Controller\Stream\FileStreamController;
use Lerp\Purchase\Factory\Controller\Ajax\PurchaseOrder\PurchaseOrderMailAjaxControllerFactory;
use Lerp\Purchase\Factory\Controller\Ajax\PurchaseRequest\PurchaseRequestMailAjaxControllerFactory;
use Lerp\Purchase\Factory\Controller\Ajax\SupplierPurchaseAjaxControllerFactory;
use Lerp\Purchase\Factory\Controller\Rest\PurchaseOrder\Files\FilePurchaseOrderRestControllerFactory;
use Lerp\Purchase\Factory\Controller\Rest\PurchaseRequest\Files\FilePurchaseRequestRestControllerFactory;
use Lerp\Purchase\Factory\Controller\Rest\PurchaseRequest\PurchaseRequestRestControllerFactory;
use Lerp\Purchase\Controller\Ajax\PurchaseOrder\PurchaseOrderAttachAjaxController;
use Lerp\Purchase\Controller\Ajax\PurchaseOrder\PurchaseOrderAjaxController;
use Lerp\Purchase\Controller\Ajax\PurchaseRequest\PurchaseRequestAjaxController;
use Lerp\Purchase\Controller\Rest\PurchaseOrder\PurchaseOrderItemAttachRestController;
use Lerp\Purchase\Controller\Rest\PurchaseOrder\PurchaseOrderRestController;
use Lerp\Purchase\Controller\Rest\PurchaseOrder\PurchaseOrderItemRestController;
use Lerp\Purchase\Controller\Rest\PurchaseRequest\PurchaseRequestItemRestController;
use Lerp\Purchase\Controller\Rest\PurchaseRequest\PurchaseRequestRestController;
use Lerp\Purchase\Factory\Controller\Ajax\PurchaseOrder\PurchaseOrderAttachAjaxControllerFactory;
use Lerp\Purchase\Factory\Controller\Ajax\PurchaseOrder\PurchaseOrderAjaxControllerFactory;
use Lerp\Purchase\Factory\Controller\Ajax\PurchaseRequest\PurchaseRequestAjaxControllerFactory;
use Lerp\Purchase\Factory\Controller\Rest\PurchaseOrder\PurchaseOrderItemAttachRestControllerFactory;
use Lerp\Purchase\Factory\Controller\Rest\PurchaseOrder\PurchaseOrderRestControllerFactory;
use Lerp\Purchase\Factory\Controller\Rest\PurchaseOrder\PurchaseOrderItemRestControllerFactory;
use Lerp\Purchase\Factory\Controller\Rest\PurchaseRequest\PurchaseRequestItemRestControllerFactory;
use Lerp\Purchase\Factory\Controller\Stream\FileStreamControllerFactory;
use Lerp\Purchase\Factory\Form\PurchaseOrder\FilePurchaseOrderFormFactory;
use Lerp\Purchase\Factory\Form\PurchaseOrderFormFactory;
use Lerp\Purchase\Factory\Form\PurchaseRequest\FilePurchaseRequestFormFactory;
use Lerp\Purchase\Factory\Form\PurchaseRequestFormFactory;
use Lerp\Purchase\Factory\Service\PurchaseGodServiceFactory;
use Lerp\Purchase\Factory\Service\PurchaseOrder\Files\FilePurchaseOrderRelServiceFactory;
use Lerp\Purchase\Factory\Service\PurchaseOrder\PurchaseOrderAttachServiceFactory;
use Lerp\Purchase\Factory\Service\PurchaseOrder\PurchaseOrderItemServiceFactory;
use Lerp\Purchase\Factory\Service\PurchaseOrder\PurchaseOrderMailServiceFactory;
use Lerp\Purchase\Factory\Service\PurchaseOrder\PurchaseOrderServiceFactory;
use Lerp\Purchase\Factory\Service\PurchaseRequest\Files\FilePurchaseRequestRelServiceFactory;
use Lerp\Purchase\Factory\Service\PurchaseRequest\PurchaseRequestItemServiceFactory;
use Lerp\Purchase\Factory\Service\PurchaseRequest\PurchaseRequestMailServiceFactory;
use Lerp\Purchase\Factory\Service\PurchaseRequest\PurchaseRequestServiceFactory;
use Lerp\Purchase\Factory\Table\PurchaseOrder\Files\FilePurchaseOrderRelTableFactory;
use Lerp\Purchase\Factory\Table\PurchaseOrder\Mail\MailSendPurchaseOrderRelTableFactory;
use Lerp\Purchase\Factory\Table\PurchaseOrder\PurchaseOrderItemAttachTableFactory;
use Lerp\Purchase\Factory\Table\PurchaseOrder\PurchaseOrderItemRatingTableFactory;
use Lerp\Purchase\Factory\Table\PurchaseOrder\PurchaseOrderItemTableFactory;
use Lerp\Purchase\Factory\Table\PurchaseOrder\PurchaseOrderTableFactory;
use Lerp\Purchase\Factory\Table\PurchaseOrder\ViewSupplierPurchaseOrderItemTableFactory;
use Lerp\Purchase\Factory\Table\PurchaseRequest\Files\FilePurchaseRequestRelTableFactory;
use Lerp\Purchase\Factory\Table\PurchaseRequest\Mail\MailSendPurchaseRequestRelTableFactory;
use Lerp\Purchase\Factory\Table\PurchaseRequest\PurchaseRequestItemTableFactory;
use Lerp\Purchase\Factory\Table\PurchaseRequest\PurchaseRequestTableFactory;
use Lerp\Purchase\Factory\Table\PurchaseRequest\ViewSupplierPurchaseRequestItemTableFactory;
use Lerp\Purchase\Form\PurchaseOrder\FilePurchaseOrderForm;
use Lerp\Purchase\Form\PurchaseOrder\PurchaseOrderForm;
use Lerp\Purchase\Form\PurchaseRequest\FilePurchaseRequestForm;
use Lerp\Purchase\Form\PurchaseRequest\PurchaseRequestForm;
use Lerp\Purchase\Service\PurchaseGodService;
use Lerp\Purchase\Service\PurchaseOrder\Files\FilePurchaseOrderRelService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderAttachService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderItemService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderMailService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderService;
use Lerp\Purchase\Service\PurchaseRequest\Files\FilePurchaseRequestRelService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestItemService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestMailService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestService;
use Lerp\Purchase\Table\PurchaseOrder\Files\FilePurchaseOrderRelTable;
use Lerp\Purchase\Table\PurchaseOrder\Mail\MailSendPurchaseOrderRelTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemAttachTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemRatingTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderTable;
use Lerp\Purchase\Table\PurchaseOrder\ViewSupplierPurchaseOrderItemTable;
use Lerp\Purchase\Table\PurchaseRequest\Files\FilePurchaseRequestRelTable;
use Lerp\Purchase\Table\PurchaseRequest\Mail\MailSendPurchaseRequestRelTable;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestItemTable;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestTable;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Lerp\Purchase\Table\PurchaseRequest\ViewSupplierPurchaseRequestItemTable;
use Lerp\Purchase\Unique\UniqueNumberProviderInterface;
use Lerp\Purchase\Factory\Unique\UniqueNumberProviderFactory;

return [
    'router'          => [
        'routes' => [
            /*
             * REST - PurchaseRequest
             */
            'lerp_purchase_rest_purchaserequest_purchaserequest'            => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-purchase-request[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseRequestRestController::class,
                    ],
                ],
            ],
            'lerp_purchase_rest_purchaserequest_purchaserequestitem'        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-purchase-request-item[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseRequestItemRestController::class,
                    ],
                ],
            ],
            'lerp_purchase_rest_purchaserequest_files'                      => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-file-purchase-request[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FilePurchaseRequestRestController::class,
                    ],
                ],
            ],
            /*
             * REST - PurchaseOrder
             */
            'lerp_purchase_rest_purchaseorder_purchaseorder'                => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-purchase-order[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseOrderRestController::class,
                    ],
                ],
            ],
            'lerp_purchase_rest_purchaseorder_purchaseorderitem'            => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-purchase-order-item[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseOrderItemRestController::class,
                    ],
                ],
            ],
            'lerp_purchase_rest_purchaseorder_purchaseorderitemattach'      => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-purchase-order-item-attach[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseOrderItemAttachRestController::class,
                    ],
                ],
            ],
            'lerp_purchase_rest_purchaseorder_files'                        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-file-purchase-order[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FilePurchaseOrderRestController::class,
                    ],
                ],
            ],
            /*
             * AJAX - PurchaseOrderIncomming
             */
            /*
             * AJAX - PurchaseOrder
             */
            'lerp_purchase_ajax_purchaseorder_markassend'                   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-order-mark-as-send[/:uuid]',
                    'constraints' => [
                        'uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseOrderAjaxController::class,
                        'action'     => 'markAsSend'
                    ],
                ],
            ],
            'lerp_purchase_ajax_purchaseorder_updateitemorderpriority'      => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-order-update-item-orderpriority[/:uuid]',
                    'constraints' => [
                        'uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseOrderAjaxController::class,
                        'action'     => 'updateItemOrderPriority'
                    ],
                ],
            ],
            'lerp_purchase_ajax_purchaseorder_purchaseordersummary'         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-order-summary[/:uuid]',
                    'constraints' => [
                        'uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseOrderAjaxController::class,
                        'action'     => 'purchaseOrderSummary'
                    ],
                ],
            ],
            'lerp_purchase_ajax_purchaseorder_purchaseorderitemsfororder'   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-order-items-for-order[/:order_uuid]',
                    'constraints' => [
                        'order_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseOrderAjaxController::class,
                        'action'     => 'purchaseOrderItemsForOrder'
                    ],
                ],
            ],
            'lerp_purchase_ajax_purchaseorder_supplierpurchaseorderitems'   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-supplier-purchase-order-items[/:product_uuid]',
                    'constraints' => [
                        'product_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseOrderAjaxController::class,
                        'action'     => 'supplierPurchaseOrderItems'
                    ],
                ],
            ],
            'lerp_purchase_ajax_purchaseorder_purchaseorderproductsfiles'   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-order-product-files[/:purchase_order_uuid]',
                    'constraints' => [
                        'purchase_order_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseOrderAjaxController::class,
                        'action'     => 'getPurchaseOrderProductsFiles'
                    ],
                ],
            ],
            /*
             * AJAX - purchaseOrder mail
             */
            'lerp_purchase_ajax_purchaseordermail_senddocuments'            => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-purchase-order-send-documents',
                    'defaults' => [
                        'controller' => PurchaseOrderMailAjaxController::class,
                        'action'     => 'sendDocuments'
                    ],
                ],
            ],
            'lerp_purchase_ajax_purchaseordermail_mails'                    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-order-mail-get-mails/:purchase_order_uuid',
                    'constraints' => [
                        'purchase_order_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseOrderMailAjaxController::class,
                        'action'     => 'getPurchaseOrderMails'
                    ],
                ],
            ],
            'lerp_purchase_ajax_purchaseordermail_mail'                     => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-order-mail-get-mail/:mail_send_purchase_order_rel_uuid',
                    'constraints' => [
                        'mail_send_purchase_order_rel_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseOrderMailAjaxController::class,
                        'action'     => 'getPurchaseOrderMail'
                    ],
                ],
            ],
            /*
             * AJAX - PurchaseOrderAttach
             */
            'lerp_purchase_ajax_purchaseorder_purchaseorderattach'          => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-order-attachs[/:uuid]',
                    'constraints' => [
                        'uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseOrderAttachAjaxController::class,
                        'action'     => 'purchaseOrderAttachs'
                    ],
                ],
            ],
            /*
             * AJAX - PurchaseRequest
             */
            'lerp_purchase_ajax_purchaserequest_markassend'                 => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-request-mark-as-send[/:uuid]',
                    'constraints' => [
                        'uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseRequestAjaxController::class,
                        'action'     => 'markAsSend'
                    ],
                ],
            ],
            'lerp_purchase_ajax_purchaserequest_updateitemorderpriority'    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-request-update-item-orderpriority[/:uuid]',
                    'constraints' => [
                        'uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseRequestAjaxController::class,
                        'action'     => 'updateItemOrderPriority'
                    ],
                ],
            ],
            'lerp_purchase_ajax_purchaserequest_itempricesupdate'           => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-purchase-ajax-request-item-prices-update',
                    'defaults' => [
                        'controller' => PurchaseRequestAjaxController::class,
                        'action'     => 'itemPricesUpdate'
                    ],
                ],
            ],
            'lerp_purchase_ajax_purchaseorder_supplierpurchaserequestitems' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-supplier-purchase-request-items[/:product_uuid]',
                    'constraints' => [
                        'product_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseRequestAjaxController::class,
                        'action'     => 'supplierPurchaseRequestItems'
                    ],
                ],
            ],
            'lerp_purchase_ajax_purchaseorder_purchaserequestproductsfiles' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-request-product-files[/:purchase_request_uuid]',
                    'constraints' => [
                        'purchase_request_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseRequestAjaxController::class,
                        'action'     => 'getPurchaseRequestProductsFiles'
                    ],
                ],
            ],
            /*
             * AJAX - purchaseRequest mail
             */
            'lerp_purchase_ajax_purchaserequestmail_senddocuments'          => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-purchase-request-send-documents',
                    'defaults' => [
                        'controller' => PurchaseRequestMailAjaxController::class,
                        'action'     => 'sendDocuments'
                    ],
                ],
            ],
            'lerp_purchase_ajax_purchaserequestmail_mails'                  => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-request-mail-get-mails/:purchase_request_uuid',
                    'constraints' => [
                        'purchase_request_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseRequestMailAjaxController::class,
                        'action'     => 'getPurchaseRequestMails'
                    ],
                ],
            ],
            'lerp_purchase_ajax_purchaserequestmail_mail'                   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-request-mail-get-mail/:mail_send_purchase_request_rel_uuid',
                    'constraints' => [
                        'mail_send_purchase_request_rel_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => PurchaseRequestMailAjaxController::class,
                        'action'     => 'getPurchaseRequestMail'
                    ],
                ],
            ],
            /*
             * supplier
             */
            'lerp_purchase_ajax_supplier_suppliersrequests'                   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-suppliers-requests/:supplier_uuid',
                    'constraints' => [
                        'supplier_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => SupplierPurchaseAjaxController::class,
                        'action'     => 'suppliersRequests'
                    ],
                ],
            ],
            'lerp_purchase_ajax_supplier_suppliersorders'                   => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-suppliers-orders/:supplier_uuid',
                    'constraints' => [
                        'supplier_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => SupplierPurchaseAjaxController::class,
                        'action'     => 'suppliersOrders'
                    ],
                ],
            ],
            /*
             * Stream
             */
            'lerp_purchase_stream_filestream_stream'                        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-purchase-filestream[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => Controller\Stream\FileStreamController::class,
                        'action'     => 'stream'
                    ],
                ],
            ],
        ],
    ],
    'controllers'     => [
        'factories'  => [
            // rest
            PurchaseOrderRestController::class              => PurchaseOrderRestControllerFactory::class,
            PurchaseOrderItemRestController::class          => PurchaseOrderItemRestControllerFactory::class,
            PurchaseOrderItemAttachRestController::class    => PurchaseOrderItemAttachRestControllerFactory::class,
            FilePurchaseOrderRestController::class          => FilePurchaseOrderRestControllerFactory::class,
            PurchaseRequestRestController::class            => PurchaseRequestRestControllerFactory::class,
            PurchaseRequestItemRestController::class        => PurchaseRequestItemRestControllerFactory::class,
            FilePurchaseRequestRestController::class        => FilePurchaseRequestRestControllerFactory::class,
            // ajax
            PurchaseOrderAjaxController::class              => PurchaseOrderAjaxControllerFactory::class,
            PurchaseOrderAttachAjaxController::class        => PurchaseOrderAttachAjaxControllerFactory::class,
            PurchaseOrderMailAjaxController::class          => PurchaseOrderMailAjaxControllerFactory::class,
            PurchaseRequestAjaxController::class            => PurchaseRequestAjaxControllerFactory::class,
            PurchaseRequestMailAjaxController::class        => PurchaseRequestMailAjaxControllerFactory::class,
            SupplierPurchaseAjaxController::class           => SupplierPurchaseAjaxControllerFactory::class,
            // stream
            FileStreamController::class                     => FileStreamControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories'  => [
            // table
            PurchaseRequestTable::class                 => PurchaseRequestTableFactory::class,
            PurchaseRequestItemTable::class             => PurchaseRequestItemTableFactory::class,
            ViewSupplierPurchaseRequestItemTable::class => ViewSupplierPurchaseRequestItemTableFactory::class,
            FilePurchaseRequestRelTable::class          => FilePurchaseRequestRelTableFactory::class,
            PurchaseOrderTable::class                   => PurchaseOrderTableFactory::class,
            PurchaseOrderItemTable::class               => PurchaseOrderItemTableFactory::class,
            PurchaseOrderItemRatingTable::class         => PurchaseOrderItemRatingTableFactory::class,
            PurchaseOrderItemAttachTable::class         => PurchaseOrderItemAttachTableFactory::class,
            ViewSupplierPurchaseOrderItemTable::class   => ViewSupplierPurchaseOrderItemTableFactory::class,
            FilePurchaseOrderRelTable::class            => FilePurchaseOrderRelTableFactory::class,
            MailSendPurchaseRequestRelTable::class      => MailSendPurchaseRequestRelTableFactory::class,
            MailSendPurchaseOrderRelTable::class        => MailSendPurchaseOrderRelTableFactory::class,
            // service
            PurchaseRequestService::class               => PurchaseRequestServiceFactory::class,
            PurchaseRequestItemService::class           => PurchaseRequestItemServiceFactory::class,
            PurchaseRequestMailService::class           => PurchaseRequestMailServiceFactory::class,
            FilePurchaseRequestRelService::class        => FilePurchaseRequestRelServiceFactory::class,
            PurchaseGodService::class                   => PurchaseGodServiceFactory::class,
            PurchaseOrderService::class                 => PurchaseOrderServiceFactory::class,
            PurchaseOrderItemService::class             => PurchaseOrderItemServiceFactory::class,
            PurchaseOrderMailService::class             => PurchaseOrderMailServiceFactory::class,
            PurchaseOrderAttachService::class           => PurchaseOrderAttachServiceFactory::class,
            FilePurchaseOrderRelService::class          => FilePurchaseOrderRelServiceFactory::class,
            // form
            PurchaseRequestForm::class                  => PurchaseRequestFormFactory::class,
            FilePurchaseRequestForm::class              => FilePurchaseRequestFormFactory::class,
            PurchaseOrderForm::class                    => PurchaseOrderFormFactory::class,
            FilePurchaseOrderForm::class                => FilePurchaseOrderFormFactory::class,
            // unique
            UniqueNumberProviderInterface::class        => UniqueNumberProviderFactory::class,
        ],
        'invokables' => [],
    ],
    'translator'      => [
        'locale'                    => ['de_DE', 'en_GB'],
        'translation_file_patterns' => [
            [
                'type'        => PhpArray::class,
                'base_dir'    => __DIR__ . '/../language',
                'pattern'     => '%s.php',
                'text_domain' => 'lerp_purchase'
            ],
        ],
    ],
    'lerp_purchase'   => [
        'module_brand' => 'purchase',
    ],
];

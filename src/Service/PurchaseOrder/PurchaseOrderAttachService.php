<?php

namespace Lerp\Purchase\Service\PurchaseOrder;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Product\Table\ProductTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemAttachTable;

class PurchaseOrderAttachService extends AbstractService
{
    protected PurchaseOrderItemAttachTable $purchaseOrderItemAttachTable;
    protected ProductTable $productTable;

    public function setPurchaseOrderItemAttachTable(PurchaseOrderItemAttachTable $purchaseOrderItemAttachTable): void
    {
        $this->purchaseOrderItemAttachTable = $purchaseOrderItemAttachTable;
    }

    public function setProductTable(ProductTable $productTable): void
    {
        $this->productTable = $productTable;
    }

    /**
     * @param string $purchaseOrderItemUuid
     * @param string $productUuid
     * @param float $quantity
     * @param string $quantityunitUuid
     * @return string
     */
    public function insertPurchaseOrderItemAttach(string $purchaseOrderItemUuid, string $productUuid, float $quantity, string $quantityunitUuid): string
    {
        $product = $this->productTable->getProduct($productUuid);
        if (empty($product)) {
            return '';
        }
        return $this->purchaseOrderItemAttachTable->insertPurchaseOrderItemAttach($purchaseOrderItemUuid, $productUuid, $quantity, $quantityunitUuid
            , $product['product_text_short'], $product['product_text_long']);
    }

    public function getPurchaseOrderItemAttachsForPurchaseOrderItem(string $purchaseOrderItemUuid): array
    {
        return $this->purchaseOrderItemAttachTable->getPurchaseOrderItemAttachsForPurchaseOrderItem($purchaseOrderItemUuid);
    }

    public function deletePurchaseOrderItemAttach(string $purchaseOrderItemAttachUuid): bool
    {
        return $this->purchaseOrderItemAttachTable->deletePurchaseOrderItemAttach($purchaseOrderItemAttachUuid);
    }

    /**
     * @param string $purchaseOrderUuid
     * @return array Assoc with key = purchase_order_item_uuid; value = [[],[],[]]
     */
    public function getPurchaseOrderAttachAssoc(string $purchaseOrderUuid): array
    {
        return $this->purchaseOrderItemAttachTable->getPurchaseOrderAttachAssoc($purchaseOrderUuid);
    }
}

<?php

namespace Lerp\Purchase\Service\PurchaseOrder\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Service\FileService;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Sql\Select;
use Lerp\Product\Table\Files\FileProductRelTable;
use Lerp\Purchase\Table\PurchaseOrder\Files\FilePurchaseOrderRelTable;

class FilePurchaseOrderRelService extends AbstractService
{
    protected FilePurchaseOrderRelTable $filePurchaseOrderRelTable;
    protected FileProductRelTable $fileProductRelTable;
    protected FileService $fileService;

    public function setFilePurchaseOrderRelTable(FilePurchaseOrderRelTable $filePurchaseOrderRelTable): void
    {
        $this->filePurchaseOrderRelTable = $filePurchaseOrderRelTable;
    }

    public function setFileProductRelTable(FileProductRelTable $fileProductRelTable): void
    {
        $this->fileProductRelTable = $fileProductRelTable;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    /**
     * @param string $filePurchaseOrderRelUuid
     * @return array
     */
    public function getFilePurchaseOrderRel(string $filePurchaseOrderRelUuid): array
    {
        return $this->filePurchaseOrderRelTable->getFilePurchaseOrderRel($filePurchaseOrderRelUuid);
    }

    public function getFilePurchaseOrderRelJoined(string $filePurchaseOrderRelUuid): array
    {
        return $this->filePurchaseOrderRelTable->getFilePurchaseOrderRelJoined($filePurchaseOrderRelUuid);
    }

    /**
     * @param FileEntity $fileEntity
     * @param string $purchaseOrderUuid
     * @param string $folderBrand
     * @return string filePurchaseOrderRelUuid
     */
    public function handleFileUpload(FileEntity $fileEntity, string $purchaseOrderUuid, string $folderBrand = ''): string
    {
        if (empty($fileUuid = $this->fileService->handleFileUpload($fileEntity, $folderBrand))) {
            return '';
        }
        if (empty($filePurchaseOrderRelUuid = $this->filePurchaseOrderRelTable->insertFilePurchaseOrderRel($fileUuid, $purchaseOrderUuid))) {
            $this->fileService->deleteFile($fileUuid);
            return '';
        }
        return $filePurchaseOrderRelUuid;
    }

    public function deleteFile(string $filePurchaseOrderRelUuid): bool
    {
        $connection = $this->beginTransaction($this->filePurchaseOrderRelTable);

        $fileRel = $this->filePurchaseOrderRelTable->getFilePurchaseOrderRel($filePurchaseOrderRelUuid);
        if (
            empty($fileRel) || !isset($fileRel['file_uuid'])
            || $this->filePurchaseOrderRelTable->deleteFile($filePurchaseOrderRelUuid) < 0
            || !$this->fileService->deleteFile($fileRel['file_uuid'])
        ) {
            $connection->rollback();
            return false;
        }
        $connection->commit();
        return true;
    }

    public function getFilesForPurchaseOrder(string $purchaseOrderUuid): array
    {
        return $this->filePurchaseOrderRelTable->getFilesForPurchaseOrder($purchaseOrderUuid);
    }

    public function updateFile(FileEntity $fileEntity, string $purchaseOrderUuid): bool
    {
        return $this->filePurchaseOrderRelTable->updateFile($fileEntity, $purchaseOrderUuid) >= 0;
    }

    public function getProductFilesForPurchaseOrder(string $PurchaseOrderUuid): array
    {
        $select = new Select('purchase_order_item');
        $select->columns(['product_uuid']);
        $select->where(['purchase_order_uuid' => $PurchaseOrderUuid]);
        return $this->fileProductRelTable->getFilesForProductsWithSelect($select);
    }
}

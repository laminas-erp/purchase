<?php

namespace Lerp\Purchase\Service\PurchaseOrder;

use Bitkorn\Files\Service\FileService;
use Bitkorn\Mail\Entity\MailEntity;
use Bitkorn\Mail\Service\MailService;
use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverSendTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderSendTable;
use Lerp\Purchase\Table\PurchaseOrder\Mail\MailSendPurchaseOrderRelTable;

class PurchaseOrderMailService extends AbstractService
{
    protected DocumentService $documentService;
    protected PurchaseDocumentService $purchaseDocumentService;
    protected MailService $mailService;
    protected FileService $fileService;
    protected DocPurchaseOrderSendTable $docPurchaseOrderSendTable;
    protected DocPurchaseDeliverSendTable $docPurchaseDeliverSendTable;
    protected PurchaseOrderService $purchaseOrderService;
    protected MailSendPurchaseOrderRelTable $mailSendPurchaseOrderRelTable;

    public function setDocumentService(DocumentService $documentService): void
    {
        $this->documentService = $documentService;
    }

    public function setPurchaseDocumentService(PurchaseDocumentService $purchaseDocumentService): void
    {
        $this->purchaseDocumentService = $purchaseDocumentService;
    }

    public function setMailService(MailService $mailService): void
    {
        $this->mailService = $mailService;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    public function setDocPurchaseOrderSendTable(DocPurchaseOrderSendTable $docPurchaseOrderSendTable): void
    {
        $this->docPurchaseOrderSendTable = $docPurchaseOrderSendTable;
    }

    public function setDocPurchaseDeliverSendTable(DocPurchaseDeliverSendTable $docPurchaseDeliverSendTable): void
    {
        $this->docPurchaseDeliverSendTable = $docPurchaseDeliverSendTable;
    }

    public function setPurchaseOrderService(PurchaseOrderService $purchaseOrderService): void
    {
        $this->purchaseOrderService = $purchaseOrderService;
    }

    public function setMailSendPurchaseOrderRelTable(MailSendPurchaseOrderRelTable $mailSendPurchaseOrderRelTable): void
    {
        $this->mailSendPurchaseOrderRelTable = $mailSendPurchaseOrderRelTable;
    }

    /**
     * @param MailEntity $mailEntity
     * @param string $purchaseOrderUuid
     * @param array $docPurchaseOrderUuids
     * @param array $docPurchaseDeliverUuids
     * @param array $fileUuids
     * @param string $userUuid
     * @return bool
     */
    public function sendDocuments(MailEntity $mailEntity, string $purchaseOrderUuid, array $docPurchaseOrderUuids, array $docPurchaseDeliverUuids, array $fileUuids, string $userUuid): bool
    {
        if (empty($purchaseOrderUuid)) {
            return false;
        }
        $conn = $this->beginTransaction($this->docPurchaseOrderSendTable);
        if (empty($mailSendUuid = $this->mailService->saveMail($mailEntity, $fileUuids, $userUuid))
            || empty($mailSendPurchaseOrderRelUuid = $this->mailSendPurchaseOrderRelTable->insertMailSendPurchaseOrderRel($mailSendUuid, $purchaseOrderUuid))) {
            $conn->rollback();
            return false;
        }
        $recipients = implode(',', $mailEntity->getRecipients());
        foreach ($fileUuids as $uuid) {
            if (empty($file = $this->fileService->getFileForOutput($uuid))) {
                continue;
            }
            $filepath = realpath($file['fqfn']);
            if (!file_exists($filepath)) {
                continue;
            }
            $mailEntity->addAttachment($filepath, $file['file_mimetype']);
        }
        foreach ($docPurchaseOrderUuids as $uuid) {
            if (empty($doc = $this->purchaseDocumentService->getDocPurchaseOrder($uuid))) {
                continue;
            }
            $filepath = $this->documentService->getDocumentFolderAbsolute(
                    DocumentService::DOC_TYPE_PURCHASEORDER
                    , $doc['purchase_order_time_create_unix']
                    , $doc['purchase_order_uuid']
                ) . DIRECTORY_SEPARATOR . $doc['doc_purchase_order_filename'];
            $filepath = realpath($filepath);
            if (!file_exists($filepath)) {
                continue;
            }
            $mailEntity->addAttachment($filepath, 'application/pdf');
            if (!$this->docPurchaseOrderSendTable->selectIntoSend($uuid, $userUuid, $recipients, $mailSendPurchaseOrderRelUuid)) {
                $conn->rollback();
                return false;
            }
        }
        foreach ($docPurchaseDeliverUuids as $uuid) {
            if (empty($doc = $this->purchaseDocumentService->getDocPurchaseDeliver($uuid))) {
                continue;
            }
            $filepath = $this->documentService->getDocumentFolderAbsolute(
                    DocumentService::DOC_TYPE_PURCHASEORDER_DELIVERY
                    , $doc['purchase_order_time_create_unix']
                    , $doc['purchase_order_uuid']
                ) . DIRECTORY_SEPARATOR . $doc['doc_purchase_deliver_filename'];
            $filepath = realpath($filepath);
            if (!file_exists($filepath)) {
                continue;
            }
            $mailEntity->addAttachment($filepath, 'application/pdf');
            if (!$this->docPurchaseDeliverSendTable->selectIntoSend($uuid, $userUuid, $recipients, $mailSendPurchaseOrderRelUuid)) {
                $conn->rollback();
                return false;
            }
        }
        if ($this->mailService->sendMail($mailEntity) < 1) {
            $conn->rollback();
            return false;
        }
        // update if there is a purchaseOrder document
        if (!empty($docPurchaseOrderUuids) && !$this->purchaseOrderService->updatePurchaseOrderSend($purchaseOrderUuid)) {
            $conn->rollback();
            return false;
        }
        $conn->commit();
        return true;
    }

    /**
     * @param string $purchaseOrderUuid
     * @return array From view_mail_send_purchase_order
     */
    public function getMailsSendPurchaseOrder(string $purchaseOrderUuid): array
    {
        return $this->mailSendPurchaseOrderRelTable->getMailsSendPurchaseOrder($purchaseOrderUuid);
    }

    /**
     * @param string $mailSendPurchaseOrderRelUuid
     * @return array From view_mail_send_purchase_order
     */
    public function getMailSendPurchaseOrder(string $mailSendPurchaseOrderRelUuid): array
    {
        return $this->mailSendPurchaseOrderRelTable->getMailSendPurchaseOrder($mailSendPurchaseOrderRelUuid);
    }

}

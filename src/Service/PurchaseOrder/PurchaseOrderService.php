<?php

namespace Lerp\Purchase\Service\PurchaseOrder;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Product\Table\ProductTable;
use Lerp\Product\Table\ProductTextTable;
use Lerp\Purchase\Entity\PurchaseOrderEntity;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderTable;
use Lerp\Purchase\Unique\UniqueNumberProvider;
use Lerp\Purchase\Unique\UniqueNumberProviderInterface;
use Lerp\Supplier\Table\SupplierTable;

class PurchaseOrderService extends AbstractService
{
    protected PurchaseOrderTable $purchaseOrderTable;
    protected PurchaseOrderItemTable $purchaseOrderItemTable;
    protected ProductTable $productTable;
    protected ProductTextTable $productTextTable;
    protected SupplierTable $supplierTable;
    protected UniqueNumberProviderInterface $uniqueNumberProvider;
    protected int $purchaseOrderCount = 0;

    public function setPurchaseOrderTable(PurchaseOrderTable $purchaseOrderTable): void
    {
        $this->purchaseOrderTable = $purchaseOrderTable;
    }

    public function setPurchaseOrderItemTable(PurchaseOrderItemTable $purchaseOrderItemTable): void
    {
        $this->purchaseOrderItemTable = $purchaseOrderItemTable;
    }

    public function setProductTable(ProductTable $productTable): void
    {
        $this->productTable = $productTable;
    }

    public function setProductTextTable(ProductTextTable $productTextTable): void
    {
        $this->productTextTable = $productTextTable;
    }

    public function setSupplierTable(SupplierTable $supplierTable): void
    {
        $this->supplierTable = $supplierTable;
    }

    public function setUniqueNumberProvider(UniqueNumberProviderInterface $uniqueNumberProvider): void
    {
        $this->uniqueNumberProvider = $uniqueNumberProvider;
    }

    /**
     * @param PurchaseOrderEntity $poe
     * @param string $userUuid
     * @return string
     */
    public function createPurchaseOrder(PurchaseOrderEntity $poe, string $userUuid): string
    {
        if (empty($supplier = $this->supplierTable->getSupplierBase($poe->getSupplierUuid()))) {
            return false;
        }
        $poe->setUserUuidCreate($userUuid);
        $poe->setPurchaseOrderNoCompl($this->uniqueNumberProvider->computeGetNumberComplete(UniqueNumberProvider::TYPE_ORDER));
        $poe->setPurchaseOrderNo($this->uniqueNumberProvider->getNumber());
        $connection = $this->beginTransaction($this->purchaseOrderTable);
        $purchaseOrderUuid = $this->purchaseOrderTable->insertPurchaseOrder($poe);
        if (empty($purchaseOrderUuid)) {
            return '';
        }

        if (empty($productUuid = $poe->getProductUuid()) || empty($product = $this->productTable->getProduct($productUuid))) {
            $connection->commit();
            return $purchaseOrderUuid;
        }
        if (!empty($productText = $this->productTextTable->getProductTextsForProductAndLang($productUuid, $supplier['supplier_lang_iso']))) {
            $product['product_text_short'] = $productText['product_text_text_short'];
            $product['product_text_long'] = $productText['product_text_text_long'];
        }
        if (
            empty($this->purchaseOrderItemTable->insertPurchaseOrderItem($purchaseOrderUuid, $productUuid
                , $product['product_text_short'], $product['product_text_long'], $product['quantityunit_uuid']
                , 1, $product['product_calc_price_set'] ?? 0, $product['product_taxp']))
        ) {
            $connection->rollback();
            return '';
        }
        $connection->commit();
        return $purchaseOrderUuid;
    }

    /**
     * @param string $supplierNo
     * @param string $supplierName
     * @param string $industryCategoryUuid
     * @param int $productNo
     * @param string $orderNoCompl
     * @param bool $onlyOpen
     * @param string $orderField
     * @param string $orderDirec
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function searchPurchaseOrder(string $supplierNo, string $supplierName, string $industryCategoryUuid
        , int                                  $productNo, string $orderNoCompl
        , bool                                 $onlyOpen, string $orderField, string $orderDirec, int $offset, int $limit): array
    {
        $this->purchaseOrderCount = $this->purchaseOrderTable->searchPurchaseOrder($supplierNo, $supplierName, $industryCategoryUuid
            , $productNo, $orderNoCompl, $onlyOpen, $orderField, $orderDirec, $offset, $limit, true)['count_purchase_order'];
        return $this->purchaseOrderTable->searchPurchaseOrder($supplierNo, $supplierName, $industryCategoryUuid
            , $productNo, $orderNoCompl, $onlyOpen, $orderField, $orderDirec, $offset, $limit);
    }

    /**
     * @return int
     */
    public function getPurchaseOrderCount(): int
    {
        return $this->purchaseOrderCount;
    }

    /**
     * @param string $purchaseOrderUuid
     * @return array
     */
    public function getPurchaseOrder(string $purchaseOrderUuid): array
    {
        return $this->purchaseOrderTable->getPurchaseOrder($purchaseOrderUuid);
    }

    /**
     * @param string $purchaseOrderUuid
     * @return bool
     */
    public function updatePurchaseOrderSend(string $purchaseOrderUuid): bool
    {
        return $this->purchaseOrderTable->updatePurchaseOrderSend($purchaseOrderUuid) >= 0;
    }

    /**
     * If purchase-order allready send, then nothing may be changed.
     *
     * @param string $purchaseOrderUuid
     * @return bool
     */
    public function isPurchaseOrderSend(string $purchaseOrderUuid): bool
    {
        return !empty($this->getPurchaseOrder($purchaseOrderUuid)['purchase_order_time_send']);
    }

    /**
     * @param string $purchaseOrderUuid
     * @return array|null
     */
    public function getPurchaseOrderSummary(string $purchaseOrderUuid): ?array
    {
        return $this->purchaseOrderTable->getPurchaseOrderSummary($purchaseOrderUuid);
    }

    /**
     * @param string $orderUuid
     * @return array From view_purchase_order_item.
     */
    public function getPurchaseOrderItemsForOrder(string $orderUuid): array
    {
        return $this->purchaseOrderItemTable->getPurchaseOrderItemsForOrder($orderUuid);
    }

    /**
     * @param string $supplierUuid
     * @return array From view_purchase_order
     */
    public function getPurchaseOrdersForSupplier(string $supplierUuid): array
    {
        return $this->purchaseOrderTable->getPurchaseOrdersForSupplier($supplierUuid);
    }
}

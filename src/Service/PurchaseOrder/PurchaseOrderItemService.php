<?php

namespace Lerp\Purchase\Service\PurchaseOrder;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\I18n\Translator\Translator;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Product\Service\Calc\ProductCalcServiceInterface;
use Lerp\Product\Table\ProductTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderTable;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Lerp\Purchase\Table\PurchaseOrder\ViewSupplierPurchaseOrderItemTable;

class PurchaseOrderItemService extends AbstractService
{
    protected PurchaseDocumentService $purchaseDocumentService;
    protected PurchaseOrderItemTable $purchaseOrderItemTable;
    protected PurchaseOrderTable $purchaseOrderTable;
    protected ProductTable $productTable;
    protected ProductCalcServiceInterface $productCalcService;
    protected Translator $translator;
    protected ViewSupplierPurchaseOrderItemTable $viewSupplierPurchaseOrderItemTable;

    public function setPurchaseDocumentService(PurchaseDocumentService $purchaseDocumentService): void
    {
        $this->purchaseDocumentService = $purchaseDocumentService;
    }

    public function setPurchaseOrderItemTable(PurchaseOrderItemTable $purchaseOrderItemTable): void
    {
        $this->purchaseOrderItemTable = $purchaseOrderItemTable;
    }

    public function setPurchaseOrderTable(PurchaseOrderTable $purchaseOrderTable): void
    {
        $this->purchaseOrderTable = $purchaseOrderTable;
    }

    public function setProductTable(ProductTable $productTable): void
    {
        $this->productTable = $productTable;
    }

    public function setProductCalcService(ProductCalcServiceInterface $productCalcService): void
    {
        $this->productCalcService = $productCalcService;
    }

    public function setTranslator(Translator $translator): void
    {
        $this->translator = $translator;
    }

    public function setViewSupplierPurchaseOrderItemTable(ViewSupplierPurchaseOrderItemTable $viewSupplierPurchaseOrderItemTable): void
    {
        $this->viewSupplierPurchaseOrderItemTable = $viewSupplierPurchaseOrderItemTable;
    }

    /**
     * @param string $purchaseOrderUuid
     * @return array From db.view_purchase_order_item
     */
    public function getPurchaseOrderItemsForPurchaseOrder(string $purchaseOrderUuid): array
    {
        return $this->purchaseOrderItemTable->getPurchaseOrderItemsForPurchaseOrder($purchaseOrderUuid);
    }

    /**
     * @param array $purchaseOrderItemUuids
     * @return array
     */
    public function getPurchaseOrderItemsWithUuids(array $purchaseOrderItemUuids): array
    {
        return $this->purchaseOrderItemTable->getPurchaseOrderItemsWithUuids($purchaseOrderItemUuids);
    }

    /**
     * @param string $purchaseOrderItemUuid
     * @return array
     */
    public function getPurchaseOrderItem(string $purchaseOrderItemUuid): array
    {
        return $this->purchaseOrderItemTable->getPurchaseOrderItem($purchaseOrderItemUuid);
    }

    /**
     * @param string $purchaseOrderItemUuid
     * @return bool
     */
    public function isPurchaseOrderSend(string $purchaseOrderItemUuid): bool
    {
        $po = $this->purchaseOrderItemTable->getPurchaseOrderItem($purchaseOrderItemUuid);
        if (empty($po)) {
            return false;
        }
        return !empty($po['purchase_order_time_send']);
    }

    /**
     * @param string $purchaseOrderUuid
     * @param string $productUuid
     * @param float $quantity
     * @param float $price
     * @return string
     */
    public function insertPurchaseOrderItem(string $purchaseOrderUuid, string $productUuid, float $quantity = 1, float $price = 0): string
    {
        $product = $this->productTable->getProduct($productUuid);
        if (empty($product)) {
            return '';
        }
        $productCalc = $this->productCalcService->getProductCalcLastForProduct($productUuid);
        if (!empty($productCalc)) {
            $price = floatval($productCalc['product_calc_cost_base']);
        }
        return $this->purchaseOrderItemTable->insertPurchaseOrderItem($purchaseOrderUuid, $productUuid
            , $product['product_text_short'], $product['product_text_long'], $product['quantityunit_uuid']
            , $quantity, $price, $product['product_taxp']);
    }

    /**
     * @param string $purchaseOrderItemUuid
     * @return bool
     */
    public function deletePurchaseOrderItem(string $purchaseOrderItemUuid): bool
    {
        if ($this->purchaseDocumentService->existDocForPurchaseOrderItem($purchaseOrderItemUuid)) {
            $this->message = $this->translator->translate('can_not_delete_item_doc_exist', 'lerp_purchase');
            return false;
        }
        return $this->purchaseOrderItemTable->deletePurchaseOrderItem($purchaseOrderItemUuid) >= 0;
    }

    /**
     * @param string $purchaseOrderItemUuid
     * @param string $quantityUuid
     * @param float $itemQuantity
     * @param float $itemPrice
     * @param float $minQntt
     * @param string $textShort
     * @param string $textLong
     * @return bool
     */
    public function updatePurchaseOrderItem(
        string $purchaseOrderItemUuid, string $quantityUuid, float $itemQuantity, float $itemPrice, float $minQntt, string $textShort, string $textLong
    ): bool
    {
        $itemPriceTotal = $itemPrice * $itemQuantity;
        return $this->purchaseOrderItemTable->updatePurchaseOrderItem($purchaseOrderItemUuid, $quantityUuid, $itemQuantity, $itemPrice, $minQntt, $itemPriceTotal, $textShort, $textLong) >= 0;
    }

    /**
     * @param string $purchaseOrderItemUuid
     * @param int $orderPrioPrevious
     * @param int $orderPrioCurrent
     * @return bool
     */
    public function updatePurchaseOrderItemOrderPriority(string $purchaseOrderItemUuid, int $orderPrioPrevious, int $orderPrioCurrent): bool
    {
        if ($orderPrioPrevious == $orderPrioCurrent) {
            return false;
        }
        $purchaseOrderItem = $this->purchaseOrderItemTable->getPurchaseOrderItem($purchaseOrderItemUuid);
        if (empty($purchaseOrderItem)) {
            return false;
        }
        /** @var Adapter $adapter */
        $adapter = $this->purchaseOrderItemTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $success = true;
        if ($orderPrioPrevious > $orderPrioCurrent) {
            // down
            $orderPrio = $orderPrioCurrent;
            $listItems = $this->purchaseOrderItemTable->getPurchaseOrderItemsForOrderPriority($purchaseOrderItem['purchase_order_uuid'], $orderPrioCurrent, false);
            foreach ($listItems as $listItem) {
                $orderPrio += 10;
                if ($this->purchaseOrderItemTable->updatePurchaseOrderItemsOrderPriority($listItem['purchase_order_item_uuid'], $orderPrio) < 0) {
                    $success = false;
                }
            }
            if ($this->purchaseOrderItemTable->updatePurchaseOrderItemsOrderPriority($purchaseOrderItemUuid, $orderPrioCurrent) < 0) {
                $success = false;
            }
        } else {
            // up
            $orderPrio = $orderPrioCurrent;
            $listItems = $this->purchaseOrderItemTable->getPurchaseOrderItemsForOrderPriority($purchaseOrderItem['purchase_order_uuid'], $orderPrioCurrent, true);
            foreach ($listItems as $listItem) {
                $orderPrio -= 10;
                if ($this->purchaseOrderItemTable->updatePurchaseOrderItemsOrderPriority($listItem['purchase_order_item_uuid'], $orderPrio) < 0) {
                    $success = false;
                }
            }
            if ($this->purchaseOrderItemTable->updatePurchaseOrderItemsOrderPriority($purchaseOrderItemUuid, $orderPrioCurrent) < 0) {
                $success = false;
            }
        }
        if ($success) {
            $connection->commit();
            return true;
        } else {
            $connection->rollback();
            return false;
        }
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getSupplierPurchaseOrderItems(string $productUuid): array
    {
        return $this->viewSupplierPurchaseOrderItemTable->getSupplierPurchaseOrderItems($productUuid);
    }
}

<?php

namespace Lerp\Purchase\Service\PurchaseRequest\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Service\FileService;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Sql\Select;
use Lerp\Product\Table\Files\FileProductRelTable;
use Lerp\Purchase\Table\PurchaseRequest\Files\FilePurchaseRequestRelTable;

class FilePurchaseRequestRelService extends AbstractService
{
    protected FilePurchaseRequestRelTable $filePurchaseRequestRelTable;
    protected FileProductRelTable $fileProductRelTable;
    protected FileService $fileService;

    public function setFilePurchaseRequestRelTable(FilePurchaseRequestRelTable $filePurchaseRequestRelTable): void
    {
        $this->filePurchaseRequestRelTable = $filePurchaseRequestRelTable;
    }

    public function setFileProductRelTable(FileProductRelTable $fileProductRelTable): void
    {
        $this->fileProductRelTable = $fileProductRelTable;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    /**
     * @param string $filePurchaseRequestRelUuid
     * @return array
     */
    public function getFilePurchaseRequestRel(string $filePurchaseRequestRelUuid): array
    {
        return $this->filePurchaseRequestRelTable->getFilePurchaseRequestRel($filePurchaseRequestRelUuid);
    }

    public function getFilePurchaseRequestRelJoined(string $filePurchaseRequestRelUuid): array
    {
        return $this->filePurchaseRequestRelTable->getFilePurchaseRequestRelJoined($filePurchaseRequestRelUuid);
    }

    /**
     * @param FileEntity $fileEntity
     * @param string $purchaseRequestUuid
     * @param string $folderBrand
     * @return string filePurchaseRequestRelUuid
     */
    public function handleFileUpload(FileEntity $fileEntity, string $purchaseRequestUuid, string $folderBrand = ''): string
    {
        if (empty($fileUuid = $this->fileService->handleFileUpload($fileEntity, $folderBrand))) {
            return '';
        }
        if (empty($filePurchaseRequestRelUuid = $this->filePurchaseRequestRelTable->insertFilePurchaseRequestRel($fileUuid, $purchaseRequestUuid))) {
            $this->fileService->deleteFile($fileUuid);
            return '';
        }
        return $filePurchaseRequestRelUuid;
    }

    public function deleteFile(string $filePurchaseRequestRelUuid): bool
    {
        $connection = $this->beginTransaction($this->filePurchaseRequestRelTable);

        $fileRel = $this->filePurchaseRequestRelTable->getFilePurchaseRequestRel($filePurchaseRequestRelUuid);
        if (
            empty($fileRel) || !isset($fileRel['file_uuid'])
            || $this->filePurchaseRequestRelTable->deleteFile($filePurchaseRequestRelUuid) < 0
            || !$this->fileService->deleteFile($fileRel['file_uuid'])
        ) {
            $connection->rollback();
            return false;
        }
        $connection->commit();
        return true;
    }

    public function getFilesForPurchaseRequest(string $purchaseRequestUuid): array
    {
        return $this->filePurchaseRequestRelTable->getFilesForPurchaseRequest($purchaseRequestUuid);
    }

    public function updateFile(FileEntity $fileEntity, string $purchaseRequestUuid): bool
    {
        return $this->filePurchaseRequestRelTable->updateFile($fileEntity, $purchaseRequestUuid) >= 0;
    }

    public function getProductFilesForPurchaseRequest(string $PurchaseRequestUuid): array
    {
        $select = new Select('purchase_request_item');
        $select->columns(['product_uuid']);
        $select->where(['purchase_request_uuid' => $PurchaseRequestUuid]);
        return $this->fileProductRelTable->getFilesForProductsWithSelect($select);
    }
}

<?php

namespace Lerp\Purchase\Service\PurchaseRequest;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Product\Table\ProductTable;
use Lerp\Product\Table\ProductTextTable;
use Lerp\Purchase\Entity\PurchaseRequestEntity;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestItemTable;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestTable;
use Lerp\Purchase\Unique\UniqueNumberProvider;
use Lerp\Purchase\Unique\UniqueNumberProviderInterface;
use Lerp\Supplier\Table\SupplierTable;

class PurchaseRequestService extends AbstractService
{
    protected PurchaseRequestTable $purchaseRequestTable;
    protected PurchaseRequestItemTable $purchaseOrderItemTable;
    protected ProductTable $productTable;
    protected ProductTextTable $productTextTable;
    protected SupplierTable $supplierTable;
    protected UniqueNumberProviderInterface $uniqueNumberProvider;
    protected int $purchaseOrderCount = 0;

    public function setPurchaseRequestTable(PurchaseRequestTable $purchaseRequestTable): void
    {
        $this->purchaseRequestTable = $purchaseRequestTable;
    }

    public function setPurchaseRequestItemTable(PurchaseRequestItemTable $purchaseOrderItemTable): void
    {
        $this->purchaseOrderItemTable = $purchaseOrderItemTable;
    }

    public function setProductTable(ProductTable $productTable): void
    {
        $this->productTable = $productTable;
    }

    public function setProductTextTable(ProductTextTable $productTextTable): void
    {
        $this->productTextTable = $productTextTable;
    }

    public function setSupplierTable(SupplierTable $supplierTable): void
    {
        $this->supplierTable = $supplierTable;
    }

    public function setUniqueNumberProvider(UniqueNumberProviderInterface $uniqueNumberProvider): void
    {
        $this->uniqueNumberProvider = $uniqueNumberProvider;
    }

    /**
     * @param PurchaseRequestEntity $pre
     * @param string $userUuid
     * @return string
     */
    public function createPurchaseRequest(PurchaseRequestEntity $pre, string $userUuid): string
    {
        if (empty($supplier = $this->supplierTable->getSupplierBase($pre->getSupplierUuid()))) {
            return false;
        }
        $pre->setUserUuidCreate($userUuid);
        $pre->setPurchaseRequestNoCompl($this->uniqueNumberProvider->computeGetNumberComplete(UniqueNumberProvider::TYPE_REQUEST));
        $pre->setPurchaseRequestNo($this->uniqueNumberProvider->getNumber());
        $connection = $this->beginTransaction($this->purchaseRequestTable);
        $purchaseOrderUuid = $this->purchaseRequestTable->insertPurchaseRequest($pre);
        if (empty($purchaseOrderUuid)) {
            return '';
        }

        if (empty($productUuid = $pre->getProductUuid()) || empty($product = $this->productTable->getProduct($productUuid))) {
            $connection->commit();
            return $purchaseOrderUuid;
        }
        if (!empty($productText = $this->productTextTable->getProductTextsForProductAndLang($productUuid, $supplier['supplier_lang_iso']))) {
            $product['product_text_short'] = $productText['product_text_text_short'];
            $product['product_text_long'] = $productText['product_text_text_long'];
        }
        if (
            empty($this->purchaseOrderItemTable->insertPurchaseRequestItem($purchaseOrderUuid, $productUuid
                , $product['product_text_short'], $product['product_text_long'], $product['quantityunit_uuid'], 1))
        ) {
            $connection->rollback();
            return '';
        }
        $connection->commit();
        return $purchaseOrderUuid;
    }

    /**
     * @param string $supplierNo
     * @param string $supplierName
     * @param string $industryCategoryUuid
     * @param int $productNo
     * @param string $orderField
     * @param string $orderDirec
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function searchPurchaseRequest(string $supplierNo, string $supplierName, string $industryCategoryUuid
        , int                                    $productNo, string $orderField, string $orderDirec, int $offset, int $limit): array
    {
        $this->purchaseOrderCount = $this->purchaseRequestTable->searchPurchaseRequest($supplierNo, $supplierName, $productNo, $industryCategoryUuid
            , $orderField, $orderDirec, $offset, $limit, true)['count_purchase_request'];
        return $this->purchaseRequestTable->searchPurchaseRequest($supplierNo, $supplierName, $productNo, $industryCategoryUuid
            , $orderField, $orderDirec, $offset, $limit);
    }

    /**
     * @return int
     */
    public function getPurchaseRequestCount(): int
    {
        return $this->purchaseOrderCount;
    }

    /**
     * @param string $purchaseOrderUuid
     * @return array
     */
    public function getPurchaseRequest(string $purchaseOrderUuid): array
    {
        return $this->purchaseRequestTable->getPurchaseRequest($purchaseOrderUuid);
    }

    /**
     * @param string $purchaseRequestUuid
     * @return bool
     */
    public function updatePurchaseRequestSend(string $purchaseRequestUuid): bool
    {
        return $this->purchaseRequestTable->updatePurchaseRequestSend($purchaseRequestUuid) >= 0;
    }

    /**
     * If purchase-request already send, then nothing may be changed.
     *
     * @param string $purchaseOrderUuid
     * @return bool
     */
    public function isPurchaseRequestSend(string $purchaseOrderUuid): bool
    {
        return !empty($this->getPurchaseRequest($purchaseOrderUuid)['purchase_request_time_send']);
    }

    /**
     * @param string $supplierUuid
     * @return array From view_purchase_request
     */
    public function getPurchaseRequestsForSupplier(string $supplierUuid): array
    {
        return $this->purchaseRequestTable->getPurchaseRequestsForSupplier($supplierUuid);
    }
}

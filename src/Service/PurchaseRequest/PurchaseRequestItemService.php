<?php

namespace Lerp\Purchase\Service\PurchaseRequest;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\I18n\Translator\Translator;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Product\Service\Calc\ProductCalcServiceInterface;
use Lerp\Product\Table\ProductTable;
use Lerp\Purchase\Entity\PurchaseItemPriceEntity;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestItemTable;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestTable;
use Lerp\Purchase\Table\PurchaseRequest\ViewSupplierPurchaseRequestItemTable;

class PurchaseRequestItemService extends AbstractService
{
    protected PurchaseDocumentService $purchaseDocumentService;
    protected PurchaseRequestItemTable $purchaseRequestItemTable;
    protected PurchaseRequestTable $purchaseRequestTable;
    protected ProductTable $productTable;
    protected ProductCalcServiceInterface $productCalcService;
    protected Translator $translator;
    protected ViewSupplierPurchaseRequestItemTable $viewSupplierPurchaseRequestItemTable;

    public function setPurchaseDocumentService(PurchaseDocumentService $purchaseDocumentService): void
    {
        $this->purchaseDocumentService = $purchaseDocumentService;
    }

    public function setPurchaseRequestItemTable(PurchaseRequestItemTable $purchaseRequestItemTable): void
    {
        $this->purchaseRequestItemTable = $purchaseRequestItemTable;
    }

    public function setPurchaseRequestTable(PurchaseRequestTable $purchaseRequestTable): void
    {
        $this->purchaseRequestTable = $purchaseRequestTable;
    }

    public function setProductTable(ProductTable $productTable): void
    {
        $this->productTable = $productTable;
    }

    public function setProductCalcService(ProductCalcServiceInterface $productCalcService): void
    {
        $this->productCalcService = $productCalcService;
    }

    public function setTranslator(Translator $translator): void
    {
        $this->translator = $translator;
    }

    public function setViewSupplierPurchaseRequestItemTable(ViewSupplierPurchaseRequestItemTable $viewSupplierPurchaseRequestItemTable): void
    {
        $this->viewSupplierPurchaseRequestItemTable = $viewSupplierPurchaseRequestItemTable;
    }

    /**
     * @param string $purchaseRequestUuid
     * @return array From db.view_purchase_request_item
     */
    public function getPurchaseRequestItemsForPurchaseRequest(string $purchaseRequestUuid): array
    {
        return $this->purchaseRequestItemTable->getPurchaseRequestItemsForPurchaseRequest($purchaseRequestUuid);
    }

    /**
     * @param array $purchaseRequestItemUuids
     * @return array
     */
    public function getPurchaseRequestItemsWithUuids(array $purchaseRequestItemUuids): array
    {
        return $this->purchaseRequestItemTable->getPurchaseRequestItemsWithUuids($purchaseRequestItemUuids);
    }

    /**
     * @param string $purchaseRequestItemUuid
     * @return array
     */
    public function getPurchaseRequestItem(string $purchaseRequestItemUuid): array
    {
        return $this->purchaseRequestItemTable->getPurchaseRequestItem($purchaseRequestItemUuid);
    }

    /**
     * @param string $purchaseRequestItemUuid
     * @return bool
     */
    public function isPurchaseRequestSend(string $purchaseRequestItemUuid): bool
    {
        $po = $this->purchaseRequestItemTable->getPurchaseRequestItem($purchaseRequestItemUuid);
        if (empty($po)) {
            return false;
        }
        return !empty($po['purchase_request_time_send']);
    }

    /**
     * @param string $purchaseRequestUuid
     * @param string $productUuid
     * @param float $quantity
     * @param float $price
     * @return string
     */
    public function insertPurchaseRequestItem(string $purchaseRequestUuid, string $productUuid, float $quantity = 1, float $price = 0): string
    {
        $product = $this->productTable->getProduct($productUuid);
        if (empty($product)) {
            return '';
        }
        return $this->purchaseRequestItemTable->insertPurchaseRequestItem($purchaseRequestUuid, $productUuid
            , $product['product_text_short'], $product['product_text_long'], $product['quantityunit_uuid'], $quantity);
    }

    /**
     * @param string $purchaseRequestItemUuid
     * @return bool
     */
    public function deletePurchaseRequestItem(string $purchaseRequestItemUuid): bool
    {
        if ($this->purchaseDocumentService->existDocForPurchaseRequestItem($purchaseRequestItemUuid)) {
            $this->message = $this->translator->translate('can_not_delete_item_doc_exist', 'lerp_purchase');
            return false;
        }
        return $this->purchaseRequestItemTable->deletePurchaseRequestItem($purchaseRequestItemUuid) >= 0;
    }

    /**
     * @param string $purchaseRequestItemUuid
     * @param string $quantityUuid
     * @param float $itemQuantity
     * @param string $textShort
     * @param string $textLong
     * @return bool
     */
    public function updatePurchaseRequestItem(
        string $purchaseRequestItemUuid, string $quantityUuid, float $itemQuantity, string $textShort, string $textLong
    ): bool
    {
        return $this->purchaseRequestItemTable->updatePurchaseRequestItem($purchaseRequestItemUuid, $quantityUuid, $itemQuantity, $textShort, $textLong) >= 0;
    }

    /**
     * @param string $purchaseRequestItemUuid
     * @param int $orderPrioPrevious
     * @param int $orderPrioCurrent
     * @return bool
     */
    public function updatePurchaseRequestItemOrderPriority(string $purchaseRequestItemUuid, int $orderPrioPrevious, int $orderPrioCurrent): bool
    {
        if ($orderPrioPrevious == $orderPrioCurrent) {
            return false;
        }
        $purchaseRequestItem = $this->purchaseRequestItemTable->getPurchaseRequestItem($purchaseRequestItemUuid);
        if (empty($purchaseRequestItem)) {
            return false;
        }
        /** @var Adapter $adapter */
        $adapter = $this->purchaseRequestItemTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $success = true;
        if ($orderPrioPrevious > $orderPrioCurrent) {
            // down
            $orderPrio = $orderPrioCurrent;
            $listItems = $this->purchaseRequestItemTable->getPurchaseRequestItemsForOrderPriority($purchaseRequestItem['purchase_request_uuid'], $orderPrioCurrent, false);
            foreach ($listItems as $listItem) {
                $orderPrio += 10;
                if ($this->purchaseRequestItemTable->updatePurchaseRequestItemsOrderPriority($listItem['purchase_request_item_uuid'], $orderPrio) < 0) {
                    $success = false;
                }
            }
            if ($this->purchaseRequestItemTable->updatePurchaseRequestItemsOrderPriority($purchaseRequestItemUuid, $orderPrioCurrent) < 0) {
                $success = false;
            }
        } else {
            // up
            $orderPrio = $orderPrioCurrent;
            $listItems = $this->purchaseRequestItemTable->getPurchaseRequestItemsForOrderPriority($purchaseRequestItem['purchase_request_uuid'], $orderPrioCurrent, true);
            foreach ($listItems as $listItem) {
                $orderPrio -= 10;
                if ($this->purchaseRequestItemTable->updatePurchaseRequestItemsOrderPriority($listItem['purchase_request_item_uuid'], $orderPrio) < 0) {
                    $success = false;
                }
            }
            if ($this->purchaseRequestItemTable->updatePurchaseRequestItemsOrderPriority($purchaseRequestItemUuid, $orderPrioCurrent) < 0) {
                $success = false;
            }
        }
        if ($success) {
            $connection->commit();
            return true;
        } else {
            $connection->rollback();
            return false;
        }
    }

    /**
     * @param PurchaseItemPriceEntity $purchaseItemPriceEntity
     * @return bool
     */
    public function updatePurchaseRequestItemPrice(PurchaseItemPriceEntity $purchaseItemPriceEntity): bool
    {
        return $this->purchaseRequestItemTable->updatePurchaseRequestItemPrice($purchaseItemPriceEntity) >= 0;
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getSupplierPurchaseRequestItems(string $productUuid): array
    {
        return $this->viewSupplierPurchaseRequestItemTable->getSupplierPurchaseRequestItems($productUuid);
    }
}

<?php

namespace Lerp\Purchase\Service\PurchaseRequest;

use Bitkorn\Files\Service\FileService;
use Bitkorn\Mail\Entity\MailEntity;
use Bitkorn\Mail\Service\MailService;
use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseRequestSendTable;
use Lerp\Purchase\Table\PurchaseRequest\Mail\MailSendPurchaseRequestRelTable;

class PurchaseRequestMailService extends AbstractService
{
    protected DocumentService $documentService;
    protected PurchaseDocumentService $purchaseDocumentService;
    protected MailService $mailService;
    protected FileService $fileService;
    protected DocPurchaseRequestSendTable $docPurchaseRequestSendTable;
    protected PurchaseRequestService $purchaseRequestService;
    protected MailSendPurchaseRequestRelTable $mailSendPurchaseRequestRelTable;

    public function setDocumentService(DocumentService $documentService): void
    {
        $this->documentService = $documentService;
    }

    public function setPurchaseDocumentService(PurchaseDocumentService $purchaseDocumentService): void
    {
        $this->purchaseDocumentService = $purchaseDocumentService;
    }

    public function setMailService(MailService $mailService): void
    {
        $this->mailService = $mailService;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    public function setDocPurchaseRequestSendTable(DocPurchaseRequestSendTable $docPurchaseRequestSendTable): void
    {
        $this->docPurchaseRequestSendTable = $docPurchaseRequestSendTable;
    }

    public function setPurchaseRequestService(PurchaseRequestService $purchaseRequestService): void
    {
        $this->purchaseRequestService = $purchaseRequestService;
    }

    public function setMailSendPurchaseRequestRelTable(MailSendPurchaseRequestRelTable $mailSendPurchaseRequestRelTable): void
    {
        $this->mailSendPurchaseRequestRelTable = $mailSendPurchaseRequestRelTable;
    }

    /**
     * @param MailEntity $mailEntity
     * @param string $purchaseRequestUuid
     * @param array $docPurchaseRequestUuids
     * @param array $fileUuids
     * @param string $userUuid
     * @return bool
     */
    public function sendDocuments(MailEntity $mailEntity, string $purchaseRequestUuid, array $docPurchaseRequestUuids, array $fileUuids, string $userUuid): bool
    {
        if (empty($purchaseRequestUuid)) {
            return false;
        }
        $conn = $this->beginTransaction($this->docPurchaseRequestSendTable);
        if (empty($mailSendUuid = $this->mailService->saveMail($mailEntity, $fileUuids, $userUuid))
            || empty($mailSendPurchaseRequestRelUuid = $this->mailSendPurchaseRequestRelTable->insertMailSendPurchaseRequestRel($mailSendUuid, $purchaseRequestUuid))) {
            $conn->rollback();
            return false;
        }
        $recipients = implode(',', $mailEntity->getRecipients());
        foreach ($fileUuids as $uuid) {
            if (empty($file = $this->fileService->getFileForOutput($uuid))) {
                continue;
            }
            $filepath = realpath($file['fqfn']);
            if (!file_exists($filepath)) {
                continue;
            }
            $mailEntity->addAttachment($filepath, $file['file_mimetype']);
        }
        foreach ($docPurchaseRequestUuids as $uuid) {
            if (empty($doc = $this->purchaseDocumentService->getDocPurchaseRequest($uuid))) {
                continue;
            }
            $filepath = $this->documentService->getDocumentFolderAbsolute(
                    DocumentService::DOC_TYPE_PURCHASEORDER_REQUEST
                    , $doc['purchase_request_time_create_unix']
                    , $doc['purchase_request_uuid']
                ) . DIRECTORY_SEPARATOR . $doc['doc_purchase_request_filename'];
            $filepath = realpath($filepath);
            if (!file_exists($filepath)) {
                continue;
            }
            $mailEntity->addAttachment($filepath, 'application/pdf');
            if (!$this->docPurchaseRequestSendTable->selectIntoSend($uuid, $userUuid, $recipients, $mailSendPurchaseRequestRelUuid)) {
                $conn->rollback();
                return false;
            }
        }
        if ($this->mailService->sendMail($mailEntity) < 1) {
            $conn->rollback();
            return false;
        }
        if (!empty($docPurchaseRequestUuids) && !$this->purchaseRequestService->updatePurchaseRequestSend($purchaseRequestUuid)) {
            $conn->rollback();
            return false;
        }
        $conn->commit();
        return true;
    }

    /**
     * @param string $purchaseRequestUuid
     * @return array From view_mail_send_purchase_request
     */
    public function getMailsSendPurchaseRequest(string $purchaseRequestUuid): array
    {
        return $this->mailSendPurchaseRequestRelTable->getMailsSendPurchaseRequest($purchaseRequestUuid);
    }

    /**
     * @param string $mailSendPurchaseRequestRelUuid
     * @return array From view_mail_send_purchase_request
     */
    public function getMailSendPurchaseRequest(string $mailSendPurchaseRequestRelUuid): array
    {
        return $this->mailSendPurchaseRequestRelTable->getMailSendPurchaseRequest($mailSendPurchaseRequestRelUuid);
    }
}

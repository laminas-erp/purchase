<?php

namespace Lerp\Purchase\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Log\Logger;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverSendTable;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderItemTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderSendTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderTable;

class PurchaseGodService extends AbstractService
{
    protected PurchaseOrderTable $purchaseOrderTable;
    protected PurchaseOrderItemTable $purchaseOrderItemTable;
}

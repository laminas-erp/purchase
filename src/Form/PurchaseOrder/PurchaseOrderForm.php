<?php

namespace Lerp\Purchase\Form\PurchaseOrder;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

/**
 * Class PurchaseOrderForm
 * @package Lerp\Purchase\Form
 * @todo Also for editing purchaseOrder - currently only for new purchaseOrder.
 */
class PurchaseOrderForm extends AbstractForm implements InputFilterProviderInterface, AdapterAwareInterface
{
    protected Adapter $adapter;

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'purchase_order_uuid']);
        }
        $this->add(['name' => 'supplier_uuid']);
        $this->add(['name' => 'product_uuid']);
        $this->add(['name' => 'order_uuid']);
    }


    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];
        if ($this->primaryKeyAvailable) {
            $filter['purchase_order_uuid'] = [
                'required' => true,
                'filters' => [
                    ['name' => StringTrim::class],
                    ['name' => StripTags::class],
                    ['name' => HtmlEntities::class],
                ], 'validators' => [
                    ['name' => Uuid::class]
                ]
            ];
        }
        $filter['supplier_uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                ['name' => Uuid::class],
                [
                    'name' => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table' => 'supplier',
                        'field' => 'supplier_uuid',
                    ]
                ]
            ]
        ];
        $filter['product_uuid'] = [
            'required' => false,
            'filters' => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                ['name' => Uuid::class],
                [
                    'name' => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table' => 'product',
                        'field' => 'product_uuid',
                    ]
                ]
            ]
        ];
        $filter['order_uuid'] = [
            'required' => false,
            'filters' => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                ['name' => Uuid::class],
                [
                    'name' => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table' => 'order',
                        'field' => 'order_uuid',
                    ]
                ]
            ]
        ];
        return $filter;
    }
}

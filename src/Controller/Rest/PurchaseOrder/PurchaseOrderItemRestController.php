<?php

namespace Lerp\Purchase\Controller\Rest\PurchaseOrder;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\I18n\Translator\Translator;
use Laminas\Validator\Uuid;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderItemService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;

class PurchaseOrderItemRestController extends AbstractUserRestController
{
    protected PurchaseOrderService $purchaseOrderService;
    protected PurchaseOrderItemService $purchaseOrderItemService;
    protected PurchaseDocumentService $purchaseDocumentService;
    protected Translator $translator;

    public function setPurchaseOrderService(PurchaseOrderService $purchaseOrderService): void
    {
        $this->purchaseOrderService = $purchaseOrderService;
    }

    public function setPurchaseOrderItemService(PurchaseOrderItemService $purchaseOrderItemService): void
    {
        $this->purchaseOrderItemService = $purchaseOrderItemService;
    }

    public function setPurchaseDocumentService(PurchaseDocumentService $purchaseDocumentService): void
    {
        $this->purchaseDocumentService = $purchaseDocumentService;
    }

    public function setTranslator(Translator $translator): void
    {
        $this->translator = $translator;
    }

    /**
     * GET PurchaseOrderItemss for PurchaseOrder UUID (GET parameter).
     *
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        $purchaseOrderUuid = (new FilterChainStringSanitize())->filter($request->getQuery('purchase_order_uuid', ''));
        if (!(new Uuid())->isValid($purchaseOrderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $arr = $this->purchaseOrderItemService->getPurchaseOrderItemsForPurchaseOrder($purchaseOrderUuid);
        $jsonModel->setArr($arr);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET one purchase order list item.
     *
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $itemUuid = $this->params('id');
        if (!(new Uuid())->isValid($itemUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        if (!empty($listItem = $this->purchaseOrderItemService->getPurchaseOrderItem($itemUuid))) {
            $jsonModel->setObj($listItem);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * POST Add product to purchase_order_item.
     *
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($data['purchase_order_uuid']) || !(new Uuid())->isValid($data['product_uuid'])) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->purchaseOrderService->isPurchaseOrderSend($data['purchase_order_uuid'])) {
            $jsonModel->addMessage($this->translator->translate('can_not_add_item_root_already_send', 'lerp_purchase'));
            return $jsonModel;
        }
        $purchaseOrderUuid = $data['purchase_order_uuid'];
        $productUuid = $data['product_uuid'];

        if (!empty($listItemUuid = $this->purchaseOrderItemService->insertPurchaseOrderItem($purchaseOrderUuid, $productUuid))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setUuid($listItemUuid);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id) || $this->purchaseOrderItemService->isPurchaseOrderSend($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->purchaseDocumentService->existDocForPurchaseOrderItem($id)) {
            $jsonModel->addMessage($this->translator->translate('can_not_delete_item_doc_exist', 'lerp_purchase'));
            return $jsonModel;
        }
        if ($this->purchaseOrderItemService->deletePurchaseOrderItem($id)) {
            $jsonModel->setSuccess(1);
        } else {
            $jsonModel->addMessage($this->purchaseOrderItemService->getMessage());
        }
        return $jsonModel;
    }

    /**
     * PUT Update on purchase-order-item.
     *
     * @param string $id purchase_order_item_uuid
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $purchaseOrderUuid = filter_var($data['purchase_order_uuid'], FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $quantityUuid = filter_var($data['quantityunit_uuid'], FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $itemQuantity = filter_var($data['purchase_order_item_quantity'], FILTER_SANITIZE_NUMBER_FLOAT, ['flags' => FILTER_FLAG_ALLOW_FRACTION]);
        $itemPrice = filter_var($data['purchase_order_item_price'], FILTER_SANITIZE_NUMBER_FLOAT, ['flags' => FILTER_FLAG_ALLOW_FRACTION]);
        $minQntt = filter_var($data['purchase_order_item_price_min_qntt'], FILTER_SANITIZE_NUMBER_FLOAT, ['flags' => FILTER_FLAG_ALLOW_FRACTION]);
        $textShort = filter_var($data['purchase_order_item_text_short'], FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $textLong = filter_var($data['purchase_order_item_text_long'], FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (empty($quantityUuid) || !(new Uuid())->isValid($quantityUuid) || $this->purchaseOrderItemService->isPurchaseOrderSend($purchaseOrderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->purchaseOrderItemService->updatePurchaseOrderItem($id, $quantityUuid, $itemQuantity, $itemPrice, $minQntt, $textShort, $textLong)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}

<?php

namespace Lerp\Purchase\Controller\Rest\PurchaseOrder;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Validator\Uuid;
use Lerp\Purchase\Entity\PurchaseOrderEntity;
use Lerp\Purchase\Form\PurchaseOrder\PurchaseOrderForm;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;

class PurchaseOrderRestController extends AbstractUserRestController
{
    protected PurchaseOrderForm $purchaseOrderForm;
    protected PurchaseOrderService $purchaseOrderService;

    public function setPurchaseOrderForm(PurchaseOrderForm $purchaseOrderForm): void
    {
        $this->purchaseOrderForm = $purchaseOrderForm;
    }

    public function setPurchaseOrderService(PurchaseOrderService $purchaseOrderService): void
    {
        $this->purchaseOrderService = $purchaseOrderService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->purchaseOrderForm->init();
        $this->purchaseOrderForm->setData($data);
        $poe = new PurchaseOrderEntity();
        if (
            !$this->purchaseOrderForm->isValid()
            || !$poe->exchangeArrayFromRequest($this->purchaseOrderForm->getData())
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            $jsonModel->addMessages($this->purchaseOrderForm->getMessages());
            return $jsonModel;
        }
        if (empty($purchaseOrderUuid = $this->purchaseOrderService->createPurchaseOrder($poe, $this->userService->getUserUuid()))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setUuid($purchaseOrderUuid);
        $jsonModel->setSuccess(1);
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        return $jsonModel;
    }

    /**
     * GET
     * PurchaseOrder Search
     *
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        $filter = new FilterChainStringSanitize();
        $supplierNo = $filter->filter($request->getQuery('supplier_no', ''));
        $supplierName = $filter->filter($request->getQuery('supplier_name', ''));
        $industryCategoryUuid = $filter->filter($request->getQuery('industry_category_uuid', ''));
        $productNo = (int)filter_input(INPUT_GET, 'product_no_no', FILTER_SANITIZE_NUMBER_INT);
        $orderNoCompl = filter_input(INPUT_GET, 'order_no_compl', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $onlyOpen = filter_input(INPUT_GET, 'only_open_orders', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]) == 'true';

        $orderField = $filter->filter($request->getQuery('order_field', ''));
        $orderDirec = $filter->filter($request->getQuery('order_direc', ''));

        $offset = (int)filter_input(INPUT_GET, 'offset', FILTER_SANITIZE_NUMBER_INT);
        $limit = (int)filter_input(INPUT_GET, 'limit', FILTER_SANITIZE_NUMBER_INT);

        $jsonModel->setArr($this->purchaseOrderService->searchPurchaseOrder($supplierNo, $supplierName, $industryCategoryUuid
            , $productNo, $orderNoCompl
            , $onlyOpen, $orderField, $orderDirec, $offset, $limit));
        $jsonModel->setCount($this->purchaseOrderService->getPurchaseOrderCount());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->purchaseOrderService->getPurchaseOrder($id));
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}

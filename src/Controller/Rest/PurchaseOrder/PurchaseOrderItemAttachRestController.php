<?php

namespace Lerp\Purchase\Controller\Rest\PurchaseOrder;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderAttachService;

class PurchaseOrderItemAttachRestController extends AbstractUserRestController
{
    protected PurchaseOrderAttachService $purchaseOrderAttachService;

    public function setPurchaseOrderAttachService(PurchaseOrderAttachService $purchaseOrderAttachService): void
    {
        $this->purchaseOrderAttachService = $purchaseOrderAttachService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $filter = new FilterChainStringSanitize();
        $pOrderItemUuid = $filter->filter($data['purchase_order_item_uuid']);
        $productUuid = $filter->filter($data['product_uuid']);
        $quantity = floatval($data['quantity']);
        $quantityUuid = $filter->filter($data['quantityunit_uuid']);
        $v = new Uuid();
        if (!$v->isValid($pOrderItemUuid) || !$v->isValid($productUuid) || !$v->isValid($quantityUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($uuid = $this->purchaseOrderAttachService->insertPurchaseOrderItemAttach($pOrderItemUuid, $productUuid, $quantity, $quantityUuid))) {
            $jsonModel->setUuid($uuid);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET ?purchase_order_item_uuid=
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        $purchaseOrderItemUuid = (new FilterChainStringSanitize())->filter($request->getQuery('purchase_order_item_uuid', ''));
        if (!(new Uuid())->isValid($purchaseOrderItemUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->purchaseOrderAttachService->getPurchaseOrderItemAttachsForPurchaseOrderItem($purchaseOrderItemUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id purchase_order_item_attach_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->purchaseOrderAttachService->deletePurchaseOrderItemAttach($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}

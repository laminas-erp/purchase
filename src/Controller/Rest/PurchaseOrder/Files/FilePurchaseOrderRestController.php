<?php

namespace Lerp\Purchase\Controller\Rest\PurchaseOrder\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\Uuid;
use Lerp\Purchase\Form\PurchaseOrder\FilePurchaseOrderForm;
use Lerp\Purchase\Service\PurchaseOrder\Files\FilePurchaseOrderRelService;

class FilePurchaseOrderRestController extends AbstractUserRestController
{
    protected string $moduleBrand = '';
    protected FilePurchaseOrderForm $filePurchaseOrderForm;
    protected FilePurchaseOrderRelService $filePurchaseOrderRelService;

    public function setModuleBrand(string $moduleBrand): void
    {
        $this->moduleBrand = $moduleBrand;
    }

    public function setFilePurchaseOrderForm(FilePurchaseOrderForm $filePurchaseOrderForm): void
    {
        $this->filePurchaseOrderForm = $filePurchaseOrderForm;
    }

    public function setFilePurchaseOrderRelService(FilePurchaseOrderRelService $filePurchaseOrderRelService): void
    {
        $this->filePurchaseOrderRelService = $filePurchaseOrderRelService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request instanceof Request) {
            return $jsonModel;
        }
        $purchaseOrderUuid = $data['purchase_order_uuid'];
        if (!(new Uuid())->isValid($purchaseOrderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->filePurchaseOrderForm->getFileFieldset()->setFileInputAvailable(true);
        $this->filePurchaseOrderForm->init();
        if ($request->isPost()) {
            $post = ArrayUtils::merge($data, $request->getFiles()->toArray());
            $this->filePurchaseOrderForm->setData($post);
            if ($this->filePurchaseOrderForm->isValid()) {
                $formData = $this->filePurchaseOrderForm->getData();
                $fileEntity = new FileEntity();
                if (!$fileEntity->exchangeArrayFromDatabase($formData)) {
                    throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() can not exchange array for entity');
                }
                if (!empty($fileUuid = $this->filePurchaseOrderRelService->handleFileUpload($fileEntity, $purchaseOrderUuid, $this->moduleBrand))) {
                    $jsonModel->setVariable('fileUuid', $fileUuid);
                    $jsonModel->setSuccess(1);
                }
            } else {
                $jsonModel->addMessages($this->filePurchaseOrderForm->getMessages());
            }
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id file_purchase_order_rel_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->filePurchaseOrderRelService->deleteFile($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $purchaseOrderUuid = filter_input(INPUT_GET, 'purchase_order_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if ((new Uuid())->isValid($purchaseOrderUuid) && !empty($files = $this->filePurchaseOrderRelService->getFilesForPurchaseOrder($purchaseOrderUuid))) {
            $jsonModel->setArr($files);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $data['purchase_order_uuid'] = $id; // currently unused
        $this->filePurchaseOrderForm->getFileFieldset()->setPrimaryKeyAvailable(true);
        $this->filePurchaseOrderForm->getFileFieldset()->setFileInputAvailable(false);
        $this->filePurchaseOrderForm->init();
        $this->filePurchaseOrderForm->setData($data);
        $fileEntity = new FileEntity();
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->filePurchaseOrderForm->isValid()) {
            $jsonModel->addMessages($this->filePurchaseOrderForm->getMessages());
            return $jsonModel;
        }
        if (!$fileEntity->exchangeArrayFromDatabase($this->filePurchaseOrderForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if ($this->filePurchaseOrderRelService->updateFile($fileEntity, $id)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($file = $this->filePurchaseOrderRelService->getFilePurchaseOrderRelJoined($id))) {
            $jsonModel->setObj($file);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}

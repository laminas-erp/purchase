<?php

namespace Lerp\Purchase\Controller\Rest\PurchaseRequest;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Purchase\Entity\PurchaseRequestEntity;
use Lerp\Purchase\Form\PurchaseRequest\PurchaseRequestForm;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestService;

class PurchaseRequestRestController extends AbstractUserRestController
{
    protected PurchaseRequestForm $purchaseRequestForm;
    protected PurchaseRequestService $purchaseRequestService;

    public function setPurchaseRequestForm(PurchaseRequestForm $purchaseRequestForm): void
    {
        $this->purchaseRequestForm = $purchaseRequestForm;
    }

    public function setPurchaseRequestService(PurchaseRequestService $purchaseRequestService): void
    {
        $this->purchaseRequestService = $purchaseRequestService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->purchaseRequestForm->init();
        $this->purchaseRequestForm->setData($data);
        $pre = new PurchaseRequestEntity();
        if (
            !$this->purchaseRequestForm->isValid()
            || !$pre->exchangeArrayFromRequest($this->purchaseRequestForm->getData())
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            $jsonModel->addMessages($this->purchaseRequestForm->getMessages());
            return $jsonModel;
        }
        if (empty($purchaseOrderUuid = $this->purchaseRequestService->createPurchaseRequest($pre, $this->userService->getUserUuid()))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setUuid($purchaseOrderUuid);
        $jsonModel->setSuccess(1);
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        $filter = new FilterChainStringSanitize();
        $supplierNo = $filter->filter($request->getQuery('supplier_no', ''));
        $supplierName = $filter->filter($request->getQuery('supplier_name', ''));
        $industryCategoryUuid = $filter->filter($request->getQuery('industry_category_uuid', ''));
        $productNo = (int)filter_input(INPUT_GET, 'product_no_no', FILTER_SANITIZE_NUMBER_INT);

        $orderField = $filter->filter($request->getQuery('order_field', ''));
        $orderDirec = $filter->filter($request->getQuery('order_direc', ''));

        $offset = (int)filter_input(INPUT_GET, 'offset', FILTER_SANITIZE_NUMBER_INT);
        $limit = (int)filter_input(INPUT_GET, 'limit', FILTER_SANITIZE_NUMBER_INT);

        $jsonModel->setArr($this->purchaseRequestService->searchPurchaseRequest($supplierNo, $supplierName, $industryCategoryUuid
            , $productNo, $orderField, $orderDirec, $offset, $limit));
        $jsonModel->setCount($this->purchaseRequestService->getPurchaseRequestCount());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->purchaseRequestService->getPurchaseRequest($id));
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}

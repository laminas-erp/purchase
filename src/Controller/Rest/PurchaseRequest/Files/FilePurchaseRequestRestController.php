<?php

namespace Lerp\Purchase\Controller\Rest\PurchaseRequest\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\Uuid;
use Lerp\Purchase\Form\PurchaseRequest\FilePurchaseRequestForm;
use Lerp\Purchase\Service\PurchaseRequest\Files\FilePurchaseRequestRelService;

class FilePurchaseRequestRestController extends AbstractUserRestController
{
    protected string $moduleBrand = '';
    protected FilePurchaseRequestForm $filePurchaseRequestForm;
    protected FilePurchaseRequestRelService $filePurchaseRequestRelService;

    public function setModuleBrand(string $moduleBrand): void
    {
        $this->moduleBrand = $moduleBrand;
    }

    public function setFilePurchaseRequestForm(FilePurchaseRequestForm $filePurchaseRequestForm): void
    {
        $this->filePurchaseRequestForm = $filePurchaseRequestForm;
    }

    public function setFilePurchaseRequestRelService(FilePurchaseRequestRelService $filePurchaseRequestRelService): void
    {
        $this->filePurchaseRequestRelService = $filePurchaseRequestRelService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request instanceof Request) {
            return $jsonModel;
        }
        $purchaseRequestUuid = $data['purchase_request_uuid'];
        if (!(new Uuid())->isValid($purchaseRequestUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->filePurchaseRequestForm->getFileFieldset()->setFileInputAvailable(true);
        $this->filePurchaseRequestForm->init();
        if ($request->isPost()) {
            $post = ArrayUtils::merge($data, $request->getFiles()->toArray());
            $this->filePurchaseRequestForm->setData($post);
            if ($this->filePurchaseRequestForm->isValid()) {
                $formData = $this->filePurchaseRequestForm->getData();
                $fileEntity = new FileEntity();
                if (!$fileEntity->exchangeArrayFromDatabase($formData)) {
                    throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() can not exchange array for entity');
                }
                if (!empty($fileUuid = $this->filePurchaseRequestRelService->handleFileUpload($fileEntity, $purchaseRequestUuid, $this->moduleBrand))) {
                    $jsonModel->setVariable('fileUuid', $fileUuid);
                    $jsonModel->setSuccess(1);
                }
            } else {
                $jsonModel->addMessages($this->filePurchaseRequestForm->getMessages());
            }
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id file_purchase_request_rel_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->filePurchaseRequestRelService->deleteFile($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $purchaseRequestUuid = filter_input(INPUT_GET, 'purchase_request_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if ((new Uuid())->isValid($purchaseRequestUuid) && !empty($files = $this->filePurchaseRequestRelService->getFilesForPurchaseRequest($purchaseRequestUuid))) {
            $jsonModel->setArr($files);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $data['purchase_request_uuid'] = $id; // currently unused
        $this->filePurchaseRequestForm->getFileFieldset()->setPrimaryKeyAvailable(true);
        $this->filePurchaseRequestForm->getFileFieldset()->setFileInputAvailable(false);
        $this->filePurchaseRequestForm->init();
        $this->filePurchaseRequestForm->setData($data);
        $fileEntity = new FileEntity();
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->filePurchaseRequestForm->isValid()) {
            $jsonModel->addMessages($this->filePurchaseRequestForm->getMessages());
            return $jsonModel;
        }
        if (!$fileEntity->exchangeArrayFromDatabase($this->filePurchaseRequestForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if ($this->filePurchaseRequestRelService->updateFile($fileEntity, $id)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($file = $this->filePurchaseRequestRelService->getFilePurchaseRequestRelJoined($id))) {
            $jsonModel->setObj($file);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}

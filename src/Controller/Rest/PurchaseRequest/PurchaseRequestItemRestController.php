<?php

namespace Lerp\Purchase\Controller\Rest\PurchaseRequest;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\I18n\Translator\Translator;
use Laminas\Validator\Uuid;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestItemService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestService;

class PurchaseRequestItemRestController extends AbstractUserRestController
{
    protected PurchaseRequestService $purchaseRequestService;
    protected PurchaseRequestItemService $purchaseRequestItemService;
    protected PurchaseDocumentService $purchaseDocumentService;
    protected Translator $translator;

    public function setPurchaseRequestService(PurchaseRequestService $purchaseOrderService): void
    {
        $this->purchaseRequestService = $purchaseOrderService;
    }

    public function setPurchaseRequestItemService(PurchaseRequestItemService $purchaseOrderItemService): void
    {
        $this->purchaseRequestItemService = $purchaseOrderItemService;
    }

    public function setPurchaseDocumentService(PurchaseDocumentService $purchaseDocumentService): void
    {
        $this->purchaseDocumentService = $purchaseDocumentService;
    }

    public function setTranslator(Translator $translator): void
    {
        $this->translator = $translator;
    }

    /**
     * GET PurchaseRequestItemss for PurchaseRequest UUID (GET parameter).
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        $purchaseRequestUuid = (new FilterChainStringSanitize())->filter($request->getQuery('purchase_request_uuid', ''));
        if (!(new Uuid())->isValid($purchaseRequestUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $arr = $this->purchaseRequestItemService->getPurchaseRequestItemsForPurchaseRequest($purchaseRequestUuid);
        $jsonModel->setArr($arr);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $itemUuid = $this->params('id');
        if (!(new Uuid())->isValid($itemUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }

        if (!empty($listItem = $this->purchaseRequestItemService->getPurchaseRequestItem($itemUuid))) {
            $jsonModel->setObj($listItem);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($data['purchase_request_uuid']) || !(new Uuid())->isValid($data['product_uuid'])) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->purchaseRequestService->isPurchaseRequestSend($data['purchase_request_uuid'])) {
            $jsonModel->addMessage($this->translator->translate('can_not_add_item_root_already_send', 'lerp_purchase'));
            return $jsonModel;
        }
        $purchaseRequestUuid = $data['purchase_request_uuid'];
        $productUuid = $data['product_uuid'];

        if (!empty($listItemUuid = $this->purchaseRequestItemService->insertPurchaseRequestItem($purchaseRequestUuid, $productUuid))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setUuid($listItemUuid);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id) || $this->purchaseRequestItemService->isPurchaseRequestSend($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->purchaseDocumentService->existDocForPurchaseRequestItem($id)) {
            $jsonModel->addMessage($this->translator->translate('can_not_delete_item_doc_exist', 'lerp_purchase'));
            return $jsonModel;
        }
        if ($this->purchaseRequestItemService->deletePurchaseRequestItem($id)) {
            $jsonModel->setSuccess(1);
        } else {
            $jsonModel->addMessage($this->purchaseRequestItemService->getMessage());
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $purchaseRequestUuid = filter_var($data['purchase_request_uuid'], FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $quantityUuid = filter_var($data['quantityunit_uuid'], FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $itemQuantity = filter_var($data['purchase_request_item_quantity'], FILTER_SANITIZE_NUMBER_FLOAT, ['flags' => FILTER_FLAG_ALLOW_FRACTION]);
        $textShort = filter_var($data['purchase_request_item_text_short'], FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $textLong = filter_var($data['purchase_request_item_text_long'], FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if (empty($quantityUuid) || !(new Uuid())->isValid($quantityUuid) || $this->purchaseRequestItemService->isPurchaseRequestSend($purchaseRequestUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->purchaseRequestItemService->updatePurchaseRequestItem($id, $quantityUuid, $itemQuantity, $textShort, $textLong)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}

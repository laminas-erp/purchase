<?php

namespace Lerp\Purchase\Controller\Ajax\PurchaseOrder;

use Bitkorn\Mail\Entity\MailEntity;
use Bitkorn\Trinket\Tools\FilterTools\UuidsArrayFilter;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Purchase\Service\PurchaseOrder\Files\FilePurchaseOrderRelService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderMailService;

class PurchaseOrderMailAjaxController extends AbstractUserController
{
    protected Uuid $uuid;
    protected UuidsArrayFilter $uuidsArrayFilter;
    protected PurchaseOrderMailService $purchaseOrderMailService;
    protected FilePurchaseOrderRelService $filePurchaseOrderRelService;

    public function __construct()
    {
        $this->uuid = new Uuid();
        $this->uuidsArrayFilter = new UuidsArrayFilter();
    }

    public function setPurchaseOrderMailService(PurchaseOrderMailService $purchaseOrderMailService): void
    {
        $this->purchaseOrderMailService = $purchaseOrderMailService;
    }

    public function setFilePurchaseOrderRelService(FilePurchaseOrderRelService $filePurchaseOrderRelService): void
    {
        $this->filePurchaseOrderRelService = $filePurchaseOrderRelService;
    }

    /**
     * @return JsonModel
     */
    public function sendDocumentsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request->isPost() || !$request instanceof Request) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $mailEntity = new MailEntity(new \HTMLPurifier());
        $mailEntity->handleHttpPost();
        if (!$this->uuid->isValid($purchaseOrderUuid = filter_input(INPUT_POST, 'purchase_order_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $docPurchaseOrderUuids = $this->uuidsArrayFilter->filterUuids($request->getPost()->toArray(), 'doc_purchase_order_uuids');
        $docPurchaseDeliverUuids = $this->uuidsArrayFilter->filterUuids($request->getPost()->toArray(), 'doc_purchase_deliver_uuids');
        $fileUuids = $this->uuidsArrayFilter->filterUuids($request->getPost()->toArray(), 'file_uuids');

        if ($this->purchaseOrderMailService->sendDocuments($mailEntity, $purchaseOrderUuid, $docPurchaseOrderUuids, $docPurchaseDeliverUuids, $fileUuids, $this->userService->getUserUuid())) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function getPurchaseOrderMailsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $purchaseOrderUuid = $this->params('purchase_order_uuid');
        if (!$this->uuid->isValid($purchaseOrderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->purchaseOrderMailService->getMailsSendPurchaseOrder($purchaseOrderUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function getPurchaseOrderMailAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $mailSendPurchaseOrderRelUuid = $this->params('mail_send_purchase_order_rel_uuid');
        if (!$this->uuid->isValid($mailSendPurchaseOrderRelUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->purchaseOrderMailService->getMailSendPurchaseOrder($mailSendPurchaseOrderRelUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

}

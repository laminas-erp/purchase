<?php

namespace Lerp\Purchase\Controller\Ajax\PurchaseOrder;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderAttachService;

class PurchaseOrderAttachAjaxController extends AbstractUserController
{
    protected PurchaseOrderAttachService $purchaseOrderAttachService;

    public function setPurchaseOrderAttachService(PurchaseOrderAttachService $purchaseOrderAttachService): void
    {
        $this->purchaseOrderAttachService = $purchaseOrderAttachService;
    }

    /**
     * Url param 'uuid' = purchase_order_uuid
     * @return JsonModel
     */
    public function purchaseOrderAttachsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $poUuid = $this->params('uuid');
        if (!(new Uuid())->isValid($poUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->purchaseOrderAttachService->getPurchaseOrderAttachAssoc($poUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}

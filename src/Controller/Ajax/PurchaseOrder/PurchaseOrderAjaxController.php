<?php

namespace Lerp\Purchase\Controller\Ajax\PurchaseOrder;

use Bitkorn\Mail\Service\MailService;
use Bitkorn\Trinket\Tools\FilterTools\UuidsArrayFilter;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Validator\Uuid;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;
use Lerp\Purchase\Service\PurchaseOrder\Files\FilePurchaseOrderRelService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderItemService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderMailService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderService;
use Laminas\Http\Response;

class PurchaseOrderAjaxController extends AbstractUserController
{
    protected PurchaseOrderService $purchaseOrderService;
    protected PurchaseOrderItemService $purchaseOrderItemService;
    protected PurchaseOrderMailService $purchaseOrderMailService;
    protected DocPurchaseOrderTable $docPurchaseOrderTable;
    protected DocumentService $documentService;
    protected MailService $mailService;
    protected Uuid $uuid;
    protected UuidsArrayFilter $uuidsArrayFilter;
    protected FilePurchaseOrderRelService $filePurchaseOrderRelService;

    public function __construct()
    {
        $this->uuid = new Uuid();
        $this->uuidsArrayFilter = new UuidsArrayFilter();
    }

    public function setPurchaseOrderService(PurchaseOrderService $purchaseOrderService): void
    {
        $this->purchaseOrderService = $purchaseOrderService;
    }

    public function setPurchaseOrderItemService(PurchaseOrderItemService $purchaseOrderItemService): void
    {
        $this->purchaseOrderItemService = $purchaseOrderItemService;
    }

    public function setPurchaseOrderMailService(PurchaseOrderMailService $purchaseOrderMailService): void
    {
        $this->purchaseOrderMailService = $purchaseOrderMailService;
    }

    public function setDocPurchaseOrderTable(DocPurchaseOrderTable $docPurchaseOrderTable): void
    {
        $this->docPurchaseOrderTable = $docPurchaseOrderTable;
    }

    public function setDocumentService(DocumentService $documentService): void
    {
        $this->documentService = $documentService;
    }

    public function setMailService(MailService $mailService): void
    {
        $this->mailService = $mailService;
    }

    public function setFilePurchaseOrderRelService(FilePurchaseOrderRelService $filePurchaseOrderRelService): void
    {
        $this->filePurchaseOrderRelService = $filePurchaseOrderRelService;
    }

    /**
     * @return JsonModel
     */
    public function markAsSendAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $purchaseOrderUuid = $this->params('uuid');
        if (!$this->uuid->isValid($purchaseOrderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->purchaseOrderService->updatePurchaseOrderSend($purchaseOrderUuid)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function updateItemOrderPriorityAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $purchaseOrderItemUuid = $this->params('uuid');
        $orderPrioPrevious = filter_input(INPUT_POST, 'order_prio_previous', FILTER_SANITIZE_NUMBER_INT);
        $orderPrioCurrent = filter_input(INPUT_POST, 'order_prio_current', FILTER_SANITIZE_NUMBER_INT);
        if (!$this->uuid->isValid($purchaseOrderItemUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->purchaseOrderItemService->updatePurchaseOrderItemOrderPriority($purchaseOrderItemUuid, $orderPrioPrevious, $orderPrioCurrent)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function purchaseOrderSummaryAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $purchaseOrderUuid = $this->params('uuid');
        if (!(new Uuid())->isValid($purchaseOrderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->purchaseOrderService->getPurchaseOrderSummary($purchaseOrderUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function purchaseOrderItemsForOrderAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = $this->params('order_uuid');
        if (!(new Uuid())->isValid($orderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->purchaseOrderService->getPurchaseOrderItemsForOrder($orderUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function supplierPurchaseOrderItemsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $productUuid = $this->params('product_uuid');
        if (!(new Uuid())->isValid($productUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->purchaseOrderItemService->getSupplierPurchaseOrderItems($productUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET productFiles from products within the purchaseOrder (purchase_order_uuid).
     * @return JsonModel
     */
    public function getPurchaseOrderProductsFilesAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $purchaseOrderUuid = $this->params('purchase_order_uuid');
        if (!$this->uuid->isValid($purchaseOrderUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->filePurchaseOrderRelService->getProductFilesForPurchaseOrder($purchaseOrderUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}

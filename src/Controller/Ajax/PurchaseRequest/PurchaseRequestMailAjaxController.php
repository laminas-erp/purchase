<?php

namespace Lerp\Purchase\Controller\Ajax\PurchaseRequest;

use Bitkorn\Mail\Entity\MailEntity;
use Bitkorn\Trinket\Tools\FilterTools\UuidsArrayFilter;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Purchase\Service\PurchaseRequest\Files\FilePurchaseRequestRelService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestMailService;

class PurchaseRequestMailAjaxController extends AbstractUserController
{
    protected Uuid $uuid;
    protected UuidsArrayFilter $uuidsArrayFilter;
    protected PurchaseRequestMailService $purchaseRequestMailService;
    protected FilePurchaseRequestRelService $filePurchaseRequestRelService;

    public function __construct()
    {
        $this->uuid = new Uuid();
        $this->uuidsArrayFilter = new UuidsArrayFilter();
    }

    public function setPurchaseRequestMailService(PurchaseRequestMailService $purchaseRequestMailService): void
    {
        $this->purchaseRequestMailService = $purchaseRequestMailService;
    }

    public function setFilePurchaseRequestRelService(FilePurchaseRequestRelService $filePurchaseRequestRelService): void
    {
        $this->filePurchaseRequestRelService = $filePurchaseRequestRelService;
    }

    /**
     * @return JsonModel
     */
    public function sendDocumentsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request->isPost() || !$request instanceof Request) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $mailEntity = new MailEntity(new \HTMLPurifier());
        $mailEntity->handleHttpPost();
        if (!$this->uuid->isValid($purchaseRequestUuid = filter_input(INPUT_POST, 'purchase_request_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $docPurchaseRequestUuids = $this->uuidsArrayFilter->filterUuids($request->getPost()->toArray(), 'doc_purchase_request_uuids');
        $fileUuids = $this->uuidsArrayFilter->filterUuids($request->getPost()->toArray(), 'file_uuids');

        if ($this->purchaseRequestMailService->sendDocuments($mailEntity, $purchaseRequestUuid, $docPurchaseRequestUuids, $fileUuids, $this->userService->getUserUuid())) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function getPurchaseRequestMailsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $purchaseRequestUuid = $this->params('purchase_request_uuid');
        if (!$this->uuid->isValid($purchaseRequestUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->purchaseRequestMailService->getMailsSendPurchaseRequest($purchaseRequestUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function getPurchaseRequestMailAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $mailSendPurchaseRequestRelUuid = $this->params('mail_send_purchase_request_rel_uuid');
        if (!$this->uuid->isValid($mailSendPurchaseRequestRelUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->purchaseRequestMailService->getMailSendPurchaseRequest($mailSendPurchaseRequestRelUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

}

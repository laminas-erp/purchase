<?php

namespace Lerp\Purchase\Controller\Ajax\PurchaseRequest;

use Bitkorn\Mail\Service\MailService;
use Bitkorn\Trinket\Tools\FilterTools\UuidsArrayFilter;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseRequestTable;
use Lerp\Purchase\Entity\PurchaseItemPriceEntity;
use Lerp\Purchase\Service\PurchaseRequest\Files\FilePurchaseRequestRelService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestItemService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestMailService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestService;

class PurchaseRequestAjaxController extends AbstractUserController
{
    protected PurchaseRequestService $purchaseRequestService;
    protected PurchaseRequestItemService $purchaseRequestItemService;
    protected PurchaseRequestMailService $purchaseRequestMailService;
    protected DocPurchaseRequestTable $docPurchaseRequestTable;
    protected DocumentService $documentService;
    protected MailService $mailService;
    protected Uuid $uuid;
    protected UuidsArrayFilter $uuidsArrayFilter;
    protected FilePurchaseRequestRelService $filePurchaseRequestRelService;

    public function __construct()
    {
        $this->uuid = new Uuid();
        $this->uuidsArrayFilter = new UuidsArrayFilter();
    }

    public function setPurchaseRequestService(PurchaseRequestService $purchaseRequestService): void
    {
        $this->purchaseRequestService = $purchaseRequestService;
    }

    public function setPurchaseRequestItemService(PurchaseRequestItemService $purchaseRequestItemService): void
    {
        $this->purchaseRequestItemService = $purchaseRequestItemService;
    }

    public function setPurchaseRequestMailService(PurchaseRequestMailService $purchaseRequestMailService): void
    {
        $this->purchaseRequestMailService = $purchaseRequestMailService;
    }

    public function setDocPurchaseRequestTable(DocPurchaseRequestTable $docPurchaseRequestTable): void
    {
        $this->docPurchaseRequestTable = $docPurchaseRequestTable;
    }

    public function setDocumentService(DocumentService $documentService): void
    {
        $this->documentService = $documentService;
    }

    public function setMailService(MailService $mailService): void
    {
        $this->mailService = $mailService;
    }

    public function setFilePurchaseRequestRelService(FilePurchaseRequestRelService $filePurchaseRequestRelService): void
    {
        $this->filePurchaseRequestRelService = $filePurchaseRequestRelService;
    }

    /**
     * @return JsonModel
     */
    public function markAsSendAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $purchaseRequestUuid = $this->params('uuid');
        if (!$this->uuid->isValid($purchaseRequestUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->purchaseRequestService->updatePurchaseRequestSend($purchaseRequestUuid)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function updateItemOrderPriorityAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $purchaseRequestItemUuid = $this->params('uuid');
        $orderPrioPrevious = filter_input(INPUT_POST, 'order_prio_previous', FILTER_SANITIZE_NUMBER_INT);
        $orderPrioCurrent = filter_input(INPUT_POST, 'order_prio_current', FILTER_SANITIZE_NUMBER_INT);
        if (!$this->uuid->isValid($purchaseRequestItemUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->purchaseRequestItemService->updatePurchaseRequestItemOrderPriority($purchaseRequestItemUuid, $orderPrioPrevious, $orderPrioCurrent)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function itemPricesUpdateAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request->isPut() || !$request instanceof Request) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        /** @var array $items */
        $items = json_decode($request->getContent());
        if (!is_array($items) || empty($items)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $success = 1;
        $entity = new PurchaseItemPriceEntity();
        foreach ($items as $item) {
            if (!$entity->computeArray($item) || !$this->purchaseRequestItemService->updatePurchaseRequestItemPrice($entity)) {
                $success = 0;
                break;
            }
        }
        $jsonModel->setSuccess($success);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function supplierPurchaseRequestItemsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $productUuid = $this->params('product_uuid');
        if (!(new Uuid())->isValid($productUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->purchaseRequestItemService->getSupplierPurchaseRequestItems($productUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET productFiles from products within the purchaseRequest (purchase_request_uuid).
     * @return JsonModel
     */
    public function getPurchaseRequestProductsFilesAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $purchaseRequestUuid = $this->params('purchase_request_uuid');
        if (!$this->uuid->isValid($purchaseRequestUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->filePurchaseRequestRelService->getProductFilesForPurchaseRequest($purchaseRequestUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}

<?php

namespace Lerp\Purchase\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestService;

class SupplierPurchaseAjaxController extends AbstractUserController
{
    protected Uuid $uuid;
    protected PurchaseRequestService $purchaseRequestService;
    protected PurchaseOrderService $purchaseOrderService;

    public function __construct()
    {
        $this->uuid = new Uuid();
    }

    public function setPurchaseRequestService(PurchaseRequestService $purchaseRequestService): void
    {
        $this->purchaseRequestService = $purchaseRequestService;
    }

    public function setPurchaseOrderService(PurchaseOrderService $purchaseOrderService): void
    {
        $this->purchaseOrderService = $purchaseOrderService;
    }

    /**
     * @return JsonModel
     */
    public function suppliersRequestsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->uuid->isValid($supplierUuid = $this->params('supplier_uuid'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->purchaseRequestService->getPurchaseRequestsForSupplier($supplierUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function suppliersOrdersAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->uuid->isValid($supplierUuid = $this->params('supplier_uuid'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->purchaseOrderService->getPurchaseOrdersForSupplier($supplierUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}

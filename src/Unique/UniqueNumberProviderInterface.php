<?php

namespace Lerp\Purchase\Unique;

/**
 * A Unique consists of a number and a string. Together it is one Unique.
 *
 * Providing the following document uniques:
 * - purchase_order_no_compl
 * - purchase_request_no_compl
 *
 * ...with a number and the unique (the number plus additional characters ...company brand dependent)
 */
interface UniqueNumberProviderInterface
{
    /**
     * @param int $type On of the UniqueNumberProvider::TYPE_*
     * @return bool
     */
    public function generate(int $type): bool;

    /**
     * @return string If $this->generate() returns false then there will be a message.
     */
    public function getMessage(): string;

    public function getNumber(): int;

    public function getNumberComplete(): string;

    public function computeGetNumberComplete(int $type): string;
}

<?php

namespace Lerp\Purchase\Table\PurchaseOrder\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Table\AbstractFileTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class FilePurchaseOrderRelTable extends AbstractFileTable
{
    /** @var string */
    protected $table = 'file_purchase_order_rel';

    /**
     * @param string $filePurchaseOrderRelUuid
     * @return array
     */
    public function getFilePurchaseOrderRel(string $filePurchaseOrderRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_purchase_order_rel_uuid' => $filePurchaseOrderRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFilePurchaseOrderRel(string $filePurchaseOrderRelUuid): bool
    {
        return !empty($this->getFilePurchaseOrderRel($filePurchaseOrderRelUuid));
    }

    /**
     * @param string $filePurchaseOrderRelUuid
     * @return array
     */
    public function getFilePurchaseOrderRelJoined(string $filePurchaseOrderRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_purchase_order_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['file_purchase_order_rel_uuid' => $filePurchaseOrderRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getFilePurchaseOrderRelForFileAndPurchaseOrder(string $fileUuid, string $purchaseOrderUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_uuid' => $fileUuid, 'purchase_order_uuid' => $purchaseOrderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFilePurchaseOrderRelForFileAndPurchaseOrder(string $fileUuid, string $purchaseOrderUuid): bool
    {
        $file = $this->getFilePurchaseOrderRelForFileAndPurchaseOrder($fileUuid, $purchaseOrderUuid);
        return !empty($file) && is_array($file);
    }

    public function getFilesForPurchaseOrder(string $purchaseOrderUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_purchase_order_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['purchase_order_uuid' => $purchaseOrderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertFilePurchaseOrderRel(string $fileUuid, string $purchaseOrderUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'file_purchase_order_rel_uuid' => $uuid,
                'file_uuid'                    => $fileUuid,
                'purchase_order_uuid'          => $purchaseOrderUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deleteFile(string $filePurchaseOrderRelUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['file_purchase_order_rel_uuid' => $filePurchaseOrderRelUuid]);
            return $this->executeDelete($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateFile(FileEntity $fileEntity, string $purchaseOrderUuid): int
    {
        $fileUuid = $fileEntity->getFileUuid();
        if (!$this->existFilePurchaseOrderRelForFileAndPurchaseOrder($fileUuid, $purchaseOrderUuid)) {
            return -1;
        }
        $fileEntity->unsetPrimaryKey();
        return $this->fileTable->updateFile($fileUuid, $fileEntity);
    }
}

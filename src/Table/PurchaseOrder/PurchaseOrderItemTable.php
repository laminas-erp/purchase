<?php

namespace Lerp\Purchase\Table\PurchaseOrder;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class PurchaseOrderItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'purchase_order_item';

    /**
     * @param string $purchaseOrderItemUuid
     * @return array
     */
    public function getPurchaseOrderItem(string $purchaseOrderItemUuid): array
    {
        $select = new Select('view_purchase_order_item');
        try {
            $select->where(['purchase_order_item_uuid' => $purchaseOrderItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $purchaseOrderUuid
     * @return array
     */
    public function getPurchaseOrderItemsForPurchaseOrder(string $purchaseOrderUuid): array
    {
        $select = new Select('view_purchase_order_item');
        try {
            $select->where(['purchase_order_uuid' => $purchaseOrderUuid]);
            $select->order('purchase_order_item_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param array $purchaseOrderItemUuids
     * @return array
     */
    public function getPurchaseOrderItemsWithUuids(array $purchaseOrderItemUuids): array
    {
        $select = new Select('view_purchase_order_item');
        try {
            $select->where->in('purchase_order_item_uuid', $purchaseOrderItemUuids);
            $select->order('purchase_order_item_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getPurchaseOrderItemOrderPriority(string $purchaseOrderUuid, bool $next = false): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_order_priority' => new Expression('MAX(purchase_order_item_order_priority)')]);
            $select->where(['purchase_order_uuid' => $purchaseOrderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                if (!$next) {
                    return intval($result->current()['max_order_priority']);
                }
                return 10 + intval($result->current()['max_order_priority']);
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getPurchaseOrderItemIdNext(string $purchaseOrderUuid): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_purchase_order_item_id' => new Expression('MAX(purchase_order_item_id)')]);
            $select->where(['purchase_order_uuid' => $purchaseOrderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $maxId = intval($result->current()['max_purchase_order_item_id']);
                if ($maxId < 1) {
                    return 2;
                }
                return ++$maxId;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return 2;
    }

    public function validPurchaseOrderItemIdNext(string $purchaseOrderUuid, int $purchaseOrderItemId): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['purchase_order_uuid' => $purchaseOrderUuid, 'purchase_order_item_id' => $purchaseOrderItemId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return false;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return true;
    }

    /**
     * @param string $purchaseOrderUuid
     * @param string $productUuid
     * @param string $textShort
     * @param string $textLong
     * @param string $quantityunitUuid
     * @param float $quantity
     * @param float $price
     * @param int $taxp
     * @return string
     */
    public function insertPurchaseOrderItem(string $purchaseOrderUuid
        , string $productUuid, string $textShort, string $textLong
        , string $quantityunitUuid, float $quantity = 1, float $price = 0, int $taxp = 19): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'purchase_order_item_uuid' => $uuid,
                'purchase_order_item_id' => $this->getPurchaseOrderItemIdNext($purchaseOrderUuid),
                'purchase_order_uuid' => $purchaseOrderUuid,
                'product_uuid' => $productUuid,
                'purchase_order_item_text_short' => $textShort,
                'purchase_order_item_text_long' => $textLong,
                'quantityunit_uuid' => $quantityunitUuid,
                'purchase_order_item_quantity' => $quantity,
                'purchase_order_item_price' => $price,
                'purchase_order_item_price_total' => $quantity * $price,
                'purchase_order_item_order_priority' => $this->getPurchaseOrderItemOrderPriority($purchaseOrderUuid, true),
                'purchase_order_item_taxp' => $taxp,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $purchaseOrderItemUuid
     * @return int
     */
    public function deletePurchaseOrderItem(string $purchaseOrderItemUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['purchase_order_item_uuid' => $purchaseOrderItemUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $purchaseOrderItemUuid
     * @param string $quantityUuid
     * @param float $itemQuantity
     * @param float $itemPrice
     * @param float $minQntt
     * @param float $itemPriceTotal
     * @param string $textShort
     * @param string $textLong
     * @return int
     */
    public function updatePurchaseOrderItem(
        string $purchaseOrderItemUuid, string $quantityUuid, float $itemQuantity, float $itemPrice, float $minQntt, float $itemPriceTotal, string $textShort, string $textLong
    ): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'quantityunit_uuid' => $quantityUuid,
                'purchase_order_item_quantity' => $itemQuantity,
                'purchase_order_item_price' => $itemPrice,
                'purchase_order_item_price_min_qntt' => $minQntt,
                'purchase_order_item_price_total' => $itemPriceTotal,
                'purchase_order_item_time_update' => new Expression('CURRENT_TIMESTAMP'),
                'purchase_order_item_text_short' => $textShort,
                'purchase_order_item_text_long' => $textLong,
            ]);
            $update->where(['purchase_order_item_uuid' => $purchaseOrderItemUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $purchaseOrderUuid
     * @param int $orderPriority
     * @param bool $ifUp
     * @return array
     */
    public function getPurchaseOrderItemsForOrderPriority(string $purchaseOrderUuid, int $orderPriority, bool $ifUp): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['purchase_order_uuid' => $purchaseOrderUuid]);
            if ($ifUp) {
                $select->where->lessThanOrEqualTo('purchase_order_item_order_priority', $orderPriority);
                $select->order('purchase_order_item_order_priority DESC');
            } else {
                $select->where->greaterThanOrEqualTo('purchase_order_item_order_priority', $orderPriority);
                $select->order('purchase_order_item_order_priority ASC');
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $purchaseOrderItemUuid
     * @param int $purchaseOrderItemOrderPriority
     * @return int
     */
    public function updatePurchaseOrderItemsOrderPriority(string $purchaseOrderItemUuid, int $purchaseOrderItemOrderPriority): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'purchase_order_item_order_priority' => $purchaseOrderItemOrderPriority
            ]);
            $update->where(['purchase_order_item_uuid' => $purchaseOrderItemUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $orderUuid
     * @return array From view_purchase_order_item.
     */
    public function getPurchaseOrderItemsForOrder(string $orderUuid): array
    {
        $select = new Select('view_purchase_order_item');
        try {
            $select->where(['order_uuid' => $orderUuid]);
            $select->order('supplier_name');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

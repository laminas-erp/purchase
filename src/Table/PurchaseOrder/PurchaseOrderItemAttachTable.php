<?php

namespace Lerp\Purchase\Table\PurchaseOrder;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class PurchaseOrderItemAttachTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'purchase_order_item_attach';

    /**
     * @param string $purchaseOrderItemAttachUuid
     * @return array
     */
    public function getPurchaseOrderItemAttach(string $purchaseOrderItemAttachUuid): array
    {
        $select = new Select('view_purchase_order_item_attach');
        try {
            $select->where(['purchase_order_item_attach_uuid' => $purchaseOrderItemAttachUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * Insert a new db.purchase_order_item_attach.
     *
     * @param string $purchaseOrderItemUuid
     * @param string $productUuid
     * @param float $quantity
     * @param string $quantityunitUuid
     * @param string $productTextShort
     * @param string $productTextLong
     * @return string
     */
    public function insertPurchaseOrderItemAttach(string $purchaseOrderItemUuid, string $productUuid, float $quantity, string $quantityunitUuid
        , string $productTextShort, string $productTextLong): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'purchase_order_item_attach_uuid' => $uuid,
                'purchase_order_item_uuid' => $purchaseOrderItemUuid,
                'product_uuid' => $productUuid,
                'purchase_order_item_attach_quantity' => $quantity,
                'quantityunit_uuid' => $quantityunitUuid,
                'purchase_order_item_attach_text_short' => $productTextShort,
                'purchase_order_item_attach_text_long' => $productTextLong,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $purchaseOrderItemUuid
     * @return array
     */
    public function getPurchaseOrderItemAttachsForPurchaseOrderItem(string $purchaseOrderItemUuid): array
    {
        $select = new Select('view_purchase_order_item_attach');
        try {
            $select->where(['purchase_order_item_uuid' => $purchaseOrderItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $purchaseOrderItemAttachUuid
     * @return bool
     */
    public function deletePurchaseOrderItemAttach(string $purchaseOrderItemAttachUuid): bool
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['purchase_order_item_attach_uuid' => $purchaseOrderItemAttachUuid]);
            return $this->deleteWith($delete) > 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $purchaseOrderUuid
     * @return array Assoc with key = purchase_order_item_uuid; value = [[],[],[]]
     */
    public function getPurchaseOrderAttachAssoc(string $purchaseOrderUuid): array
    {
        $select = new Select('view_purchase_order_item_attach');
        $assoc = [];
        try {
            $selectIn = new Select('purchase_order_item');
            $selectIn->columns(['purchase_order_item_uuid']);
            $selectIn->where(['purchase_order_uuid' => $purchaseOrderUuid]);

            $select->where->in('purchase_order_item_uuid', $selectIn);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                foreach($result->toArray() as $row) {
                    if(!isset($assoc[$row['purchase_order_item_uuid']])) {
                        $assoc[$row['purchase_order_item_uuid']] = [];
                    }
                    $assoc[$row['purchase_order_item_uuid']][] = $row;
                }

            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $assoc;
    }

    public function getPurchaseOrderItemAttachsWithUuids(array $purchaseOrderItemAttachUuids): array
    {
        $select = new Select('view_purchase_order_item_attach');
        try {
            $select->where->in('purchase_order_item_attach_uuid', $purchaseOrderItemAttachUuids);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

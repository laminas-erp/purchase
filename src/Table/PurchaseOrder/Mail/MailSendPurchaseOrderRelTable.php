<?php

namespace Lerp\Purchase\Table\PurchaseOrder\Mail;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class MailSendPurchaseOrderRelTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'mail_send_purchase_order_rel';

    /**
     * @param string $mailSendUuid
     * @param string $purchaseOrderUuid
     * @return string
     */
    public function insertMailSendPurchaseOrderRel(string $mailSendUuid, string $purchaseOrderUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'mail_send_purchase_order_rel_uuid' => $uuid,
                'mail_send_uuid'                    => $mailSendUuid,
                'purchase_order_uuid'               => $purchaseOrderUuid,
            ]);
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $purchaseOrderUuid
     * @return array From view_mail_send_purchase_order
     */
    public function getMailsSendPurchaseOrder(string $purchaseOrderUuid): array
    {
        $select = new Select('view_mail_send_purchase_order');
        try {
            $select->where(['purchase_order_uuid' => $purchaseOrderUuid]);
            $select->order('mail_send_time DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $rows = $result->toArray();
                foreach ($rows as &$row) {
                    $row['mail_send_attach_file_json'] = json_decode($row['mail_send_attach_file_json']);
                    $row['doc_purchase_order_json'] = json_decode($row['doc_purchase_order_json']);
                    $row['doc_purchase_deliver_json'] = json_decode($row['doc_purchase_deliver_json']);
                }
                return $rows;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $mailSendPurchaseOrderRelUuid
     * @return array From view_mail_send_purchase_order
     */
    public function getMailSendPurchaseOrder(string $mailSendPurchaseOrderRelUuid): array
    {
        $select = new Select('view_mail_send_purchase_order');
        try {
            $select->where(['mail_send_purchase_order_rel_uuid' => $mailSendPurchaseOrderRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $row = $result->toArray()[0];
                $row['mail_send_cc_arr'] = explode(',', $row['mail_send_cc_csv']);
                $row['mail_send_bcc_arr'] = explode(',', $row['mail_send_bcc_csv']);
                $row['mail_send_attach_file_json'] = json_decode($row['mail_send_attach_file_json']);
                $row['doc_purchase_order_json'] = json_decode($row['doc_purchase_order_json']);
                $row['doc_purchase_deliver_json'] = json_decode($row['doc_purchase_deliver_json']);
                return $row;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

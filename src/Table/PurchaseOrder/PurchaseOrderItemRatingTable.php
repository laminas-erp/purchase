<?php

namespace Lerp\Purchase\Table\PurchaseOrder;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class PurchaseOrderItemRatingTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'purchase_order_item_rating';

    /**
     * @param string $purchaseOrderRatingUuid
     * @return array
     */
    public function getPurchaseOrderItemRating(string $purchaseOrderRatingUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('purchase_order_item', 'purchase_order_item.purchase_order_item_uuid = purchase_order_item_rating.purchase_order_item_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->join('product', 'product.product_uuid = purchase_order_item.product_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->join('purchase_order', 'purchase_order.purchase_order_uuid = purchase_order_item.purchase_order_uuid', Select::SQL_STAR, Select::JOIN_LEFT);
            $select->where(['purchase_order_rating_uuid' => $purchaseOrderRatingUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

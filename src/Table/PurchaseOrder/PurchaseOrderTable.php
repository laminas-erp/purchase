<?php

namespace Lerp\Purchase\Table\PurchaseOrder;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Driver\Pdo\Result;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Lerp\Purchase\Entity\PurchaseOrderEntity;
use Lerp\Purchase\Unique\UniqueNumberProvider;

class PurchaseOrderTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'purchase_order';

    /**
     * @param string $purchaseOrderUuid
     * @return array
     */
    public function getPurchaseOrder(string $purchaseOrderUuid): array
    {
        $select = new Select('view_purchase_order');
        try {
            $select->where(['purchase_order_uuid' => $purchaseOrderUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param PurchaseOrderEntity $purchaseOrderEntity
     * @return string
     */
    public function insertPurchaseOrder(PurchaseOrderEntity $purchaseOrderEntity): string
    {
        $insert = $this->sql->insert();
        $purchaseOrderEntity->setPrimaryKeyValue($this->uuid());
        $purchaseOrderEntity->unsetEmptyValues(); // perhaps there is no 'order_uuid'
        try {
            $insert->values($purchaseOrderEntity->getStorageInsert());
            if ($this->insertWith($insert) > 0) {
                return $purchaseOrderEntity->getPrimaryKeyValue();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $supplierNo
     * @param string $supplierName
     * @param string $industryCategoryUuid
     * @param int $productNo
     * @param string $orderNoCompl
     * @param bool $onlyOpen
     * @param string $orderField
     * @param string $orderDirec
     * @param int $offset
     * @param int $limit
     * @param bool $count
     * @return array
     */
    public function searchPurchaseOrder(string $supplierNo, string $supplierName, string $industryCategoryUuid
        , int                                  $productNo, string $orderNoCompl
        , bool                                 $onlyOpen, string $orderField, string $orderDirec, int $offset, int $limit, bool $count = false): array
    {
        $select = new Select('view_purchase_order');
        try {
            if ($count) {
                $select->columns(['count_purchase_order' => new Expression('COUNT(purchase_order_uuid)')]);
            }

            if (!empty($supplierNo)) {
                $selectSupplierUuid = new Select('supplier');
                $selectSupplierUuid->columns(['supplier_uuid']);
                $selectSupplierUuid->where->like(new Expression('CAST(supplier_no AS TEXT)'), '%' . $supplierNo . '%');
                $select->where->in('supplier_uuid', $selectSupplierUuid);
            }
            if (!empty($supplierName)) {
                $select->where->like('supplier_name', '%' . $supplierName . '%');
            }
            if (!empty($industryCategoryUuid)) {
                $selectSupplierUuid2 = new Select('supplier');
                $selectSupplierUuid2->columns(['supplier_uuid']);
                $selectSupplierUuid2->where(['industry_category_uuid' => $industryCategoryUuid]);
                $select->where->in('supplier_uuid', $selectSupplierUuid2);
            }
            if (!empty($productNo)) {
                $selectProduct = new Select('view_product');
                $selectProduct->columns(['product_uuid']);
                $selectProduct->where->like(new Expression('CAST(product_no_no AS TEXT)'), '%' . $productNo . '%');
                $selectPoUuid = new Select('purchase_order_item');
                $selectPoUuid->columns(['purchase_order_uuid']);
                $selectPoUuid->where->in('product_uuid', $selectProduct);

                $select->where->in('purchase_order_uuid', $selectPoUuid);
            }
            if (!empty($orderNoCompl)) {
                $select->where->like(new Expression('order_no_compl'), '%' . $orderNoCompl . '%');
            }
            if ($onlyOpen) {
                $select->where->isNull('purchase_order_time_send');
            }

            if (!$count && isset($offset) && isset($limit)) {
                $select->limit($limit)->offset($offset);
            }
            if (!$count && !empty($orderField) && !empty($orderDirec)) {
                $select->order($orderField . ' ' . $orderDirec);
            }

            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if (!$count) {
                    return $result->toArray();
                } else {
                    return ['count_purchase_order' => $result->current()['count_purchase_order']];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $purchaseOrderUuid
     * @return int
     */
    public function updatePurchaseOrderSend(string $purchaseOrderUuid): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'purchase_order_time_send' => new Expression('CURRENT_TIMESTAMP')
            ]);
            $update->where(['purchase_order_uuid' => $purchaseOrderUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getPurchaseOrderSummary(string $purchaseOrderUuid): ?array
    {
        $params = new ParameterContainer([$purchaseOrderUuid]);
        $stmt = $this->getAdapter()->getDriver()->createStatement('SELECT * FROM lerp_get_purchaseorder_summary(:purchase_order_uuid)');
        /** @var Result $result */
        $result = $stmt->execute($params);
        if ($result->valid() && $result->count() == 1) {
            return $result->current();
        }
        return [];
    }

    /**
     * The maximum of fetched number is `UniqueNumberProvider::PAD_LENGTH` digits long (e.g. 4 => max: 9999).
     * @param \DateTime $dateTimeFrom
     * @param \DateTime $dateTimeTo
     * @return int
     */
    public function getMaxPurchaseOrderNoInPeriod(\DateTime $dateTimeFrom, \DateTime $dateTimeTo): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(purchase_order_no)')]);
            $select->where->greaterThanOrEqualTo('purchase_order_time_create', $dateTimeFrom->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('purchase_order_time_create', $dateTimeTo->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('purchase_order_no', intval(str_pad('9', UniqueNumberProvider::PAD_LENGTH, '9')));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $supplierUuid
     * @return array From view_purchase_order
     */
    public function getPurchaseOrdersForSupplier(string $supplierUuid): array
    {
        $select = new Select('view_purchase_order');
        try {
            $select->where(['supplier_uuid' => $supplierUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

<?php

namespace Lerp\Purchase\Table\PurchaseRequest\Mail;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class MailSendPurchaseRequestRelTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'mail_send_purchase_request_rel';

    /**
     * @param string $mailSendUuid
     * @param string $purchaseRequestUuid
     * @return string
     */
    public function insertMailSendPurchaseRequestRel(string $mailSendUuid, string $purchaseRequestUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'mail_send_purchase_request_rel_uuid' => $uuid,
                'mail_send_uuid'                      => $mailSendUuid,
                'purchase_request_uuid'               => $purchaseRequestUuid,
            ]);
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $purchaseRequestUuid
     * @return array From view_mail_send_purchase_request
     */
    public function getMailsSendPurchaseRequest(string $purchaseRequestUuid): array
    {
        $select = new Select('view_mail_send_purchase_request');
        try {
            $select->where(['purchase_request_uuid' => $purchaseRequestUuid]);
            $select->order('mail_send_time DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $rows = $result->toArray();
                foreach ($rows as &$row) {
                    $row['mail_send_attach_file_json'] = json_decode($row['mail_send_attach_file_json']);
                    $row['doc_purchase_request_json'] = json_decode($row['doc_purchase_request_json']);
                }
                return $rows;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $mailSendPurchaseRequestRelUuid
     * @return array From view_mail_send_purchase_request
     */
    public function getMailSendPurchaseRequest(string $mailSendPurchaseRequestRelUuid): array
    {
        $select = new Select('view_mail_send_purchase_request');
        try {
            $select->where(['mail_send_purchase_request_rel_uuid' => $mailSendPurchaseRequestRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $row = $result->toArray()[0];
                $row['mail_send_cc_arr'] = explode(',', $row['mail_send_cc_csv']);
                $row['mail_send_bcc_arr'] = explode(',', $row['mail_send_bcc_csv']);
                $row['mail_send_attach_file_json'] = json_decode($row['mail_send_attach_file_json']);
                $row['doc_purchase_request_json'] = json_decode($row['doc_purchase_request_json']);
                return $row;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

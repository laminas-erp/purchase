<?php

namespace Lerp\Purchase\Table\PurchaseRequest;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ViewSupplierPurchaseRequestItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_supplier_purchase_request_item';

    /**
     * @param string $productUuid
     * @return array
     */
    public function getSupplierPurchaseRequestItems(string $productUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_uuid' => $productUuid]);
            $select->order('purchase_request_time_create DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

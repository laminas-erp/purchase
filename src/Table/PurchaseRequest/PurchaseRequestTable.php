<?php

namespace Lerp\Purchase\Table\PurchaseRequest;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Lerp\Purchase\Entity\PurchaseRequestEntity;
use Lerp\Purchase\Unique\UniqueNumberProvider;

class PurchaseRequestTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'purchase_request';

    /**
     * @param string $purchaseRequestUuid
     * @return array
     */
    public function getPurchaseRequest(string $purchaseRequestUuid): array
    {
        $select = new Select('view_purchase_request');
        try {
            $select->where(['purchase_request_uuid' => $purchaseRequestUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param PurchaseRequestEntity $purchaseRequestEntity
     * @return string
     */
    public function insertPurchaseRequest(PurchaseRequestEntity $purchaseRequestEntity): string
    {
        $insert = $this->sql->insert();
        $purchaseRequestEntity->setPrimaryKeyValue($this->uuid());
        try {
            $insert->values($purchaseRequestEntity->getStorageInsert());
            if ($this->insertWith($insert) > 0) {
                return $purchaseRequestEntity->getPrimaryKeyValue();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function searchPurchaseRequest(string $supplierNo, string $supplierName, int $productNo, string $industryCategoryUuid
        , string                                 $orderField, string $orderDirec, int $offset, int $limit, bool $count = false): array
    {
        $select = new Select('view_purchase_request');
        try {
            if ($count) {
                $select->columns(['count_purchase_request' => new Expression('COUNT(*)')]);
            }

            if (!empty($supplierNo)) {
                $selectSupplierUuid = new Select('supplier');
                $selectSupplierUuid->columns(['supplier_uuid']);
                $selectSupplierUuid->where->like(new Expression('CAST(supplier_no AS TEXT)'), '%' . $supplierNo . '%');
                $select->where->in('supplier_uuid', $selectSupplierUuid);
            }
            if (!empty($supplierName)) {
                $select->where->like('supplier_name', '%' . $supplierName . '%');
            }
            if (!empty($productNo)) {
                $selectProduct = new Select('view_product');
                $selectProduct->columns(['product_uuid']);
                $selectProduct->where->like(new Expression('CAST(product_no_no AS TEXT)'), '%' . $productNo . '%');
                $selectPoUuid = new Select('purchase_request_item');
                $selectPoUuid->columns(['purchase_request_uuid']);
                $selectPoUuid->where->in('product_uuid', $selectProduct);

                $select->where->in('purchase_request_uuid', $selectPoUuid);
            }
            if (!empty($industryCategoryUuid)) {
                $selectSupplierUuid2 = new Select('supplier');
                $selectSupplierUuid2->columns(['supplier_uuid']);
                $selectSupplierUuid2->where(['industry_category_uuid' => $industryCategoryUuid]);
                $select->where->in('supplier_uuid', $selectSupplierUuid2);
            }

            if (!$count && isset($offset) && isset($limit)) {
                $select->limit($limit)->offset($offset);
            }
            if (!$count && !empty($orderField) && !empty($orderDirec)) {
                $select->order($orderField . ' ' . $orderDirec);
            }

            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if (!$count) {
                    return $result->toArray();
                } else {
                    return ['count_purchase_request' => $result->current()['count_purchase_request']];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $purchaseRequestUuid
     * @return int
     */
    public function updatePurchaseRequestSend(string $purchaseRequestUuid): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'purchase_request_time_send' => new Expression('CURRENT_TIMESTAMP')
            ]);
            $update->where(['purchase_request_uuid' => $purchaseRequestUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * The maximum of fetched number is `UniqueNumberProvider::PAD_LENGTH` digits long (e.g. 4 => max: 9999).
     * @param \DateTime $dateTimeFrom
     * @param \DateTime $dateTimeTo
     * @return int
     */
    public function getMaxPurchaseRequestNoInPeriod(\DateTime $dateTimeFrom, \DateTime $dateTimeTo): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_no' => new Expression('MAX(purchase_request_no)')]);
            $select->where->greaterThanOrEqualTo('purchase_request_time_create', $dateTimeFrom->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('purchase_request_time_create', $dateTimeTo->format('Y-m-d H:i:s'));
            $select->where->lessThanOrEqualTo('purchase_request_no', intval(str_pad('9', UniqueNumberProvider::PAD_LENGTH, '9')));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1 && ($maxNo = $result->current()->getArrayCopy()['max_no'])) {
                return $maxNo;
            }
            return 0;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $supplierUuid
     * @return array From view_purchase_request
     */
    public function getPurchaseRequestsForSupplier(string $supplierUuid): array
    {
        $select = new Select('view_purchase_request');
        try {
            $select->where(['supplier_uuid' => $supplierUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

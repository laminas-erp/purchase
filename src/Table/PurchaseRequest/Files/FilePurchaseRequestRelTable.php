<?php

namespace Lerp\Purchase\Table\PurchaseRequest\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Table\AbstractFileTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class FilePurchaseRequestRelTable extends AbstractFileTable
{
    /** @var string */
    protected $table = 'file_purchase_request_rel';

    /**
     * @param string $filePurchaseRequestRelUuid
     * @return array
     */
    public function getFilePurchaseRequestRel(string $filePurchaseRequestRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_purchase_request_rel_uuid' => $filePurchaseRequestRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFilePurchaseRequestRel(string $filePurchaseRequestRelUuid): bool
    {
        return !empty($this->getFilePurchaseRequestRel($filePurchaseRequestRelUuid));
    }

    /**
     * @param string $filePurchaseRequestRelUuid
     * @return array
     */
    public function getFilePurchaseRequestRelJoined(string $filePurchaseRequestRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_purchase_request_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['file_purchase_request_rel_uuid' => $filePurchaseRequestRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getFilePurchaseRequestRelForFileAndPurchaseRequest(string $fileUuid, string $purchaseRequestUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_uuid' => $fileUuid, 'purchase_request_uuid' => $purchaseRequestUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFilePurchaseRequestRelForFileAndPurchaseRequest(string $fileUuid, string $purchaseRequestUuid): bool
    {
        $file = $this->getFilePurchaseRequestRelForFileAndPurchaseRequest($fileUuid, $purchaseRequestUuid);
        return !empty($file) && is_array($file);
    }

    public function getFilesForPurchaseRequest(string $purchaseRequestUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_purchase_request_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['purchase_request_uuid' => $purchaseRequestUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertFilePurchaseRequestRel(string $fileUuid, string $purchaseRequestUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'file_purchase_request_rel_uuid' => $uuid,
                'file_uuid'                      => $fileUuid,
                'purchase_request_uuid'          => $purchaseRequestUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deleteFile(string $filePurchaseRequestRelUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['file_purchase_request_rel_uuid' => $filePurchaseRequestRelUuid]);
            return $this->executeDelete($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateFile(FileEntity $fileEntity, string $purchaseRequestUuid): int
    {
        $fileUuid = $fileEntity->getFileUuid();
        if (!$this->existFilePurchaseRequestRelForFileAndPurchaseRequest($fileUuid, $purchaseRequestUuid)) {
            return -1;
        }
        $fileEntity->unsetPrimaryKey();
        return $this->fileTable->updateFile($fileUuid, $fileEntity);
    }
}

<?php

namespace Lerp\Purchase\Table\PurchaseRequest;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Purchase\Entity\PurchaseItemPriceEntity;

class PurchaseRequestItemTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'purchase_request_item';

    /**
     * @param string $purchaseRequestItemUuid
     * @return array
     */
    public function getPurchaseRequestItem(string $purchaseRequestItemUuid): array
    {
        $select = new Select('view_purchase_request_item');
        try {
            $select->where(['purchase_request_item_uuid' => $purchaseRequestItemUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getPurchaseRequestItemsForPurchaseRequest(string $purchaseRequestUuid): array
    {
        $select = new Select('view_purchase_request_item');
        try {
            $select->where(['purchase_request_uuid' => $purchaseRequestUuid]);
            $select->order('purchase_request_item_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getPurchaseRequestItemsWithUuids(array $purchaseRequestItemUuids): array
    {
        $select = new Select('view_purchase_request_item');
        try {
            $select->where->in('purchase_request_item_uuid', $purchaseRequestItemUuids);
            $select->order('purchase_request_item_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getPurchaseRequestItemOrderPriority(string $purchaseRequestUuid, bool $next = false): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_order_priority' => new Expression('MAX(purchase_request_item_order_priority)')]);
            $select->where(['purchase_request_uuid' => $purchaseRequestUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                if (!$next) {
                    return intval($result->current()['max_order_priority']);
                }
                return 10 + intval($result->current()['max_order_priority']);
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getPurchaseRequestItemIdNext(string $purchaseRequestUuid): int
    {
        $select = $this->sql->select();
        try {
            $select->columns(['max_purchase_request_item_id' => new Expression('MAX(purchase_request_item_id)')]);
            $select->where(['purchase_request_uuid' => $purchaseRequestUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $maxId = intval($result->current()['max_purchase_request_item_id']);
                if ($maxId < 1) {
                    return 2;
                }
                return ++$maxId;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return 2;
    }

    public function validPurchaseRequestItemIdNext(string $purchaseRequestUuid, int $purchaseRequestItemId): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['purchase_request_uuid' => $purchaseRequestUuid, 'purchase_request_item_id' => $purchaseRequestItemId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return false;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return true;
    }

    public function insertPurchaseRequestItem(string $purchaseRequestUuid
        , string $productUuid, string $textShort, string $textLong
        , string $quantityunitUuid, float $quantity = 1): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'purchase_request_item_uuid'           => $uuid,
                'purchase_request_item_id'             => $this->getPurchaseRequestItemIdNext($purchaseRequestUuid),
                'purchase_request_uuid'                => $purchaseRequestUuid,
                'product_uuid'                         => $productUuid,
                'purchase_request_item_text_short'     => $textShort,
                'purchase_request_item_text_long'      => $textLong,
                'quantityunit_uuid'                    => $quantityunitUuid,
                'purchase_request_item_quantity'       => $quantity,
                'purchase_request_item_order_priority' => $this->getPurchaseRequestItemOrderPriority($purchaseRequestUuid, true),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deletePurchaseRequestItem(string $purchaseRequestItemUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['purchase_request_item_uuid' => $purchaseRequestItemUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updatePurchaseRequestItem(
        string $purchaseRequestItemUuid, string $quantityUuid, float $itemQuantity, string $textShort, string $textLong
    ): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'quantityunit_uuid'                 => $quantityUuid,
                'purchase_request_item_quantity'    => $itemQuantity,
                'purchase_request_item_time_update' => new Expression('CURRENT_TIMESTAMP'),
                'purchase_request_item_text_short'  => $textShort,
                'purchase_request_item_text_long'   => $textLong,
            ]);
            $update->where(['purchase_request_item_uuid' => $purchaseRequestItemUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getPurchaseRequestItemsForOrderPriority(string $purchaseRequestUuid, int $orderPriority, bool $ifUp): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['purchase_request_uuid' => $purchaseRequestUuid]);
            if ($ifUp) {
                $select->where->lessThanOrEqualTo('purchase_request_item_order_priority', $orderPriority);
                $select->order('purchase_request_item_order_priority DESC');
            } else {
                $select->where->greaterThanOrEqualTo('purchase_request_item_order_priority', $orderPriority);
                $select->order('purchase_request_item_order_priority ASC');
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function updatePurchaseRequestItemsOrderPriority(string $purchaseRequestItemUuid, int $purchaseRequestItemOrderPriority): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'purchase_request_item_order_priority' => $purchaseRequestItemOrderPriority
            ]);
            $update->where(['purchase_request_item_uuid' => $purchaseRequestItemUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param PurchaseItemPriceEntity $purchaseItemPriceEntity
     * @return int
     */
    public function updatePurchaseRequestItemPrice(PurchaseItemPriceEntity $purchaseItemPriceEntity): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'purchase_request_item_price'          => $purchaseItemPriceEntity->getPrice(),
                'purchase_request_item_price_total'    => $purchaseItemPriceEntity->getTotal(),
                'purchase_request_item_price_min_qntt' => $purchaseItemPriceEntity->getMin(),
                'purchase_request_item_taxp'           => $purchaseItemPriceEntity->getTaxp(),
            ]);
            $update->where(['purchase_request_item_uuid' => $purchaseItemPriceEntity->getUuid()]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}

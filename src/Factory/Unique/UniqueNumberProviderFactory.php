<?php

namespace Lerp\Purchase\Factory\Unique;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderTable;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestTable;
use Lerp\Purchase\Unique\UniqueNumberProvider;

class UniqueNumberProviderFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $unique = new UniqueNumberProvider();
        $unique->setLogger($container->get('logger'));
        $unique->setPurchaseOrderTable($container->get(PurchaseOrderTable::class));
        $unique->setPurchaseRequestTable($container->get(PurchaseRequestTable::class));
        return $unique;
    }
}

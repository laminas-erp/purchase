<?php

namespace Lerp\Purchase\Factory\Service\PurchaseRequest;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Product\Table\ProductTable;
use Lerp\Product\Table\ProductTextTable;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestService;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestItemTable;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestTable;
use Lerp\Purchase\Unique\UniqueNumberProviderInterface;
use Lerp\Supplier\Table\SupplierTable;

class PurchaseRequestServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new PurchaseRequestService();
        $service->setLogger($container->get('logger'));
        $service->setPurchaseRequestTable($container->get(PurchaseRequestTable::class));
        $service->setPurchaseRequestItemTable($container->get(PurchaseRequestItemTable::class));
        $service->setProductTable($container->get(ProductTable::class));
        $service->setProductTextTable($container->get(ProductTextTable::class));
        $service->setSupplierTable($container->get(SupplierTable::class));
        $service->setUniqueNumberProvider($container->get(UniqueNumberProviderInterface::class));
        return $service;
    }
}

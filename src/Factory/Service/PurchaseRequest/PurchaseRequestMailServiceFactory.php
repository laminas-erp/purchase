<?php

namespace Lerp\Purchase\Factory\Service\PurchaseRequest;

use Bitkorn\Files\Service\FileService;
use Bitkorn\Mail\Service\MailService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseRequestSendTable;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestMailService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestService;
use Lerp\Purchase\Table\PurchaseRequest\Mail\MailSendPurchaseRequestRelTable;

class PurchaseRequestMailServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new PurchaseRequestMailService();
        $service->setLogger($container->get('logger'));
        $service->setDocumentService($container->get(DocumentService::class));
        $service->setPurchaseDocumentService($container->get(PurchaseDocumentService::class));
        $service->setMailService($container->get(MailService::class));
        $service->setFileService($container->get(FileService::class));
        $service->setDocPurchaseRequestSendTable($container->get(DocPurchaseRequestSendTable::class));
        $service->setPurchaseRequestService($container->get(PurchaseRequestService::class));
        $service->setMailSendPurchaseRequestRelTable($container->get(MailSendPurchaseRequestRelTable::class));
        return $service;
    }
}

<?php

namespace Lerp\Purchase\Factory\Service\PurchaseRequest;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Product\Service\Calc\ProductCalcService;
use Lerp\Product\Table\ProductTable;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestItemService;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestItemTable;
use Lerp\Purchase\Table\PurchaseRequest\PurchaseRequestTable;
use Lerp\Purchase\Table\PurchaseRequest\ViewSupplierPurchaseRequestItemTable;

class PurchaseRequestItemServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new PurchaseRequestItemService();
        $service->setLogger($container->get('logger'));
        $service->setPurchaseDocumentService($container->get(PurchaseDocumentService::class));
        $service->setPurchaseRequestItemTable($container->get(PurchaseRequestItemTable::class));
        $service->setPurchaseRequestTable($container->get(PurchaseRequestTable::class));
        $service->setProductTable($container->get(ProductTable::class));
        $service->setProductCalcService($container->get(ProductCalcService::class));
        $service->setTranslator($container->get('translator'));
        $service->setViewSupplierPurchaseRequestItemTable($container->get(ViewSupplierPurchaseRequestItemTable::class));
        return $service;
    }
}

<?php

namespace Lerp\Purchase\Factory\Service\PurchaseOrder;

use Bitkorn\Files\Service\FileService;
use Bitkorn\Mail\Service\MailService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseDeliverSendTable;
use Lerp\Document\Table\Purchase\DocPurchaseOrderSendTable;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderMailService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderService;
use Lerp\Purchase\Table\PurchaseOrder\Mail\MailSendPurchaseOrderRelTable;

class PurchaseOrderMailServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new PurchaseOrderMailService();
        $service->setLogger($container->get('logger'));
        $service->setDocumentService($container->get(DocumentService::class));
        $service->setPurchaseDocumentService($container->get(PurchaseDocumentService::class));
        $service->setMailService($container->get(MailService::class));
        $service->setFileService($container->get(FileService::class));
        $service->setDocPurchaseOrderSendTable($container->get(DocPurchaseOrderSendTable::class));
        $service->setDocPurchaseDeliverSendTable($container->get(DocPurchaseDeliverSendTable::class));
        $service->setPurchaseOrderService($container->get(PurchaseOrderService::class));
        $service->setMailSendPurchaseOrderRelTable($container->get(MailSendPurchaseOrderRelTable::class));
        return $service;
    }
}

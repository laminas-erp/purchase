<?php

namespace Lerp\Purchase\Factory\Service\PurchaseOrder;

use Interop\Container\ContainerInterface;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Product\Service\Calc\ProductCalcService;
use Lerp\Product\Table\ProductTable;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderItemService;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Purchase\Table\PurchaseOrder\ViewSupplierPurchaseOrderItemTable;

class PurchaseOrderItemServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new PurchaseOrderItemService();
        $service->setLogger($container->get('logger'));
        $service->setPurchaseDocumentService($container->get(PurchaseDocumentService::class));
        $service->setPurchaseOrderItemTable($container->get(PurchaseOrderItemTable::class));
        $service->setPurchaseOrderTable($container->get(PurchaseOrderTable::class));
        $service->setProductTable($container->get(ProductTable::class));
        $service->setProductCalcService($container->get(ProductCalcService::class));
        $service->setTranslator($container->get('translator'));
        $service->setViewSupplierPurchaseOrderItemTable($container->get(ViewSupplierPurchaseOrderItemTable::class));
        return $service;
    }
}

<?php

namespace Lerp\Purchase\Factory\Service\PurchaseOrder;

use Interop\Container\ContainerInterface;
use Lerp\Product\Table\ProductTable;
use Lerp\Product\Table\ProductTextTable;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderService;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemTable;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderTable;
use Lerp\Purchase\Unique\UniqueNumberProviderInterface;
use Lerp\Supplier\Table\SupplierTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PurchaseOrderServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new PurchaseOrderService();
        $service->setLogger($container->get('logger'));
        $service->setPurchaseOrderTable($container->get(PurchaseOrderTable::class));
        $service->setPurchaseOrderItemTable($container->get(PurchaseOrderItemTable::class));
        $service->setProductTable($container->get(ProductTable::class));
        $service->setProductTextTable($container->get(ProductTextTable::class));
        $service->setSupplierTable($container->get(SupplierTable::class));
        $service->setUniqueNumberProvider($container->get(UniqueNumberProviderInterface::class));
        return $service;
    }
}

<?php

namespace Lerp\Purchase\Factory\Service\PurchaseOrder;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Product\Table\ProductTable;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderAttachService;
use Lerp\Purchase\Table\PurchaseOrder\PurchaseOrderItemAttachTable;

class PurchaseOrderAttachServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new PurchaseOrderAttachService();
        $service->setLogger($container->get('logger'));
        $service->setPurchaseOrderItemAttachTable($container->get(PurchaseOrderItemAttachTable::class));
        $service->setProductTable($container->get(ProductTable::class));
        return $service;
    }
}

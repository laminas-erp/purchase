<?php

namespace Lerp\Purchase\Factory\Form\PurchaseRequest;

use Bitkorn\Files\Form\FileFieldset;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Purchase\Form\PurchaseRequest\FilePurchaseRequestForm;

class FilePurchaseRequestFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new FilePurchaseRequestForm();
        $form->setFileFieldset($container->get(FileFieldset::class));
        return $form;
    }
}

<?php

namespace Lerp\Purchase\Factory\Controller\Ajax\PurchaseRequest;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Purchase\Controller\Ajax\PurchaseRequest\PurchaseRequestMailAjaxController;
use Lerp\Purchase\Service\PurchaseRequest\Files\FilePurchaseRequestRelService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestMailService;

class PurchaseRequestMailAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new PurchaseRequestMailAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setPurchaseRequestMailService($container->get(PurchaseRequestMailService::class));
        $controller->setFilePurchaseRequestRelService($container->get(FilePurchaseRequestRelService::class));
        return $controller;
    }
}

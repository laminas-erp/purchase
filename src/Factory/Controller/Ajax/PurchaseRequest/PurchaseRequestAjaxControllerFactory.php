<?php

namespace Lerp\Purchase\Factory\Controller\Ajax\PurchaseRequest;

use Bitkorn\Mail\Service\MailService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseRequestTable;
use Lerp\Purchase\Controller\Ajax\PurchaseRequest\PurchaseRequestAjaxController;
use Lerp\Purchase\Service\PurchaseRequest\Files\FilePurchaseRequestRelService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestItemService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestMailService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestService;

class PurchaseRequestAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new PurchaseRequestAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setPurchaseRequestService($container->get(PurchaseRequestService::class));
        $controller->setPurchaseRequestItemService($container->get(PurchaseRequestItemService::class));
        $controller->setPurchaseRequestMailService($container->get(PurchaseRequestMailService::class));
        $controller->setDocPurchaseRequestTable($container->get(DocPurchaseRequestTable::class));
        $controller->setDocumentService($container->get(DocumentService::class));
        $controller->setMailService($container->get(MailService::class));
        $controller->setFilePurchaseRequestRelService($container->get(FilePurchaseRequestRelService::class));
        return $controller;
    }
}

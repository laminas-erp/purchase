<?php

namespace Lerp\Purchase\Factory\Controller\Ajax\PurchaseOrder;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Purchase\Controller\Ajax\PurchaseOrder\PurchaseOrderMailAjaxController;
use Lerp\Purchase\Service\PurchaseOrder\Files\FilePurchaseOrderRelService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderMailService;

class PurchaseOrderMailAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new PurchaseOrderMailAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setPurchaseOrderMailService($container->get(PurchaseOrderMailService::class));
        $controller->setFilePurchaseOrderRelService($container->get(FilePurchaseOrderRelService::class));
        return $controller;
    }
}

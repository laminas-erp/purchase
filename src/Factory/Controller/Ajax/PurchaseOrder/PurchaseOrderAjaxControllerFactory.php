<?php

namespace Lerp\Purchase\Factory\Controller\Ajax\PurchaseOrder;

use Bitkorn\Mail\Service\MailService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Lerp\Document\Service\DocumentService;
use Lerp\Document\Table\Purchase\DocPurchaseOrderTable;
use Lerp\Purchase\Controller\Ajax\PurchaseOrder\PurchaseOrderAjaxController;
use Lerp\Purchase\Service\PurchaseOrder\Files\FilePurchaseOrderRelService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderItemService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderMailService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PurchaseOrderAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new PurchaseOrderAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setPurchaseOrderService($container->get(PurchaseOrderService::class));
        $controller->setPurchaseOrderItemService($container->get(PurchaseOrderItemService::class));
        $controller->setPurchaseOrderMailService($container->get(PurchaseOrderMailService::class));
        $controller->setDocPurchaseOrderTable($container->get(DocPurchaseOrderTable::class));
        $controller->setDocumentService($container->get(DocumentService::class));
        $controller->setMailService($container->get(MailService::class));
        $controller->setFilePurchaseOrderRelService($container->get(FilePurchaseOrderRelService::class));
        return $controller;
    }
}

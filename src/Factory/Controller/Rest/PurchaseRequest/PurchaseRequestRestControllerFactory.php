<?php

namespace Lerp\Purchase\Factory\Controller\Rest\PurchaseRequest;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Purchase\Controller\Rest\PurchaseRequest\PurchaseRequestRestController;
use Lerp\Purchase\Form\PurchaseRequest\PurchaseRequestForm;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestService;

class PurchaseRequestRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new PurchaseRequestRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setPurchaseRequestForm($container->get(PurchaseRequestForm::class));
        $controller->setPurchaseRequestService($container->get(PurchaseRequestService::class));
        return $controller;
    }
}

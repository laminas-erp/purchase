<?php

namespace Lerp\Purchase\Factory\Controller\Rest\PurchaseRequest;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Purchase\Controller\Rest\PurchaseRequest\PurchaseRequestItemRestController;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestItemService;
use Lerp\Purchase\Service\PurchaseRequest\PurchaseRequestService;

class PurchaseRequestItemRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new PurchaseRequestItemRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setPurchaseRequestService($container->get(PurchaseRequestService::class));
        $controller->setPurchaseRequestItemService($container->get(PurchaseRequestItemService::class));
        $controller->setPurchaseDocumentService($container->get(PurchaseDocumentService::class));
        $controller->setTranslator($container->get('translator'));
        return $controller;
    }
}

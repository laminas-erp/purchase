<?php

namespace Lerp\Purchase\Factory\Controller\Rest\PurchaseRequest\Files;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Purchase\Controller\Rest\PurchaseRequest\Files\FilePurchaseRequestRestController;
use Lerp\Purchase\Form\PurchaseRequest\FilePurchaseRequestForm;
use Lerp\Purchase\Service\PurchaseRequest\Files\FilePurchaseRequestRelService;

class FilePurchaseRequestRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FilePurchaseRequestRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setModuleBrand($container->get('config')['lerp_purchase']['module_brand']);
        $controller->setFilePurchaseRequestForm($container->get(FilePurchaseRequestForm::class));
        $controller->setFilePurchaseRequestRelService($container->get(FilePurchaseRequestRelService::class));
        return $controller;
    }
}

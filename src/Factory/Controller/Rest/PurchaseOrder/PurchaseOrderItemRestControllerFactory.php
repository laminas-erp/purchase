<?php

namespace Lerp\Purchase\Factory\Controller\Rest\PurchaseOrder;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Lerp\Document\Service\PurchaseDocumentService;
use Lerp\Purchase\Controller\Rest\PurchaseOrder\PurchaseOrderItemRestController;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderItemService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderService;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PurchaseOrderItemRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new PurchaseOrderItemRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setPurchaseOrderService($container->get(PurchaseOrderService::class));
        $controller->setPurchaseOrderItemService($container->get(PurchaseOrderItemService::class));
        $controller->setPurchaseDocumentService($container->get(PurchaseDocumentService::class));
        $controller->setTranslator($container->get('translator'));
        return $controller;
    }
}

<?php

namespace Lerp\Purchase\Factory\Controller\Rest\PurchaseOrder\Files;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Purchase\Controller\Rest\PurchaseOrder\Files\FilePurchaseOrderRestController;
use Lerp\Purchase\Form\PurchaseOrder\FilePurchaseOrderForm;
use Lerp\Purchase\Service\PurchaseOrder\Files\FilePurchaseOrderRelService;

class FilePurchaseOrderRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FilePurchaseOrderRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setModuleBrand($container->get('config')['lerp_purchase']['module_brand']);
        $controller->setFilePurchaseOrderForm($container->get(FilePurchaseOrderForm::class));
        $controller->setFilePurchaseOrderRelService($container->get(FilePurchaseOrderRelService::class));
        return $controller;
    }
}

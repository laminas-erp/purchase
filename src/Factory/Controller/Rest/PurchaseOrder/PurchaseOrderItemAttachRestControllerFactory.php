<?php

namespace Lerp\Purchase\Factory\Controller\Rest\PurchaseOrder;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Purchase\Controller\Rest\PurchaseOrder\PurchaseOrderItemAttachRestController;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderAttachService;

class PurchaseOrderItemAttachRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new PurchaseOrderItemAttachRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setPurchaseOrderAttachService($container->get(PurchaseOrderAttachService::class));
        return $controller;
    }
}

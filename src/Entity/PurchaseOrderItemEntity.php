<?php

namespace Lerp\Purchase\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

/**
 * Class PurchaseOrderItemEntity
 * @package Lerp\Purchase\Entity
 * Use table view_purchase_order_item.
 */
class PurchaseOrderItemEntity extends AbstractEntity
{
    public array $mapping = [
        'purchase_order_item_uuid'               => 'purchase_order_item_uuid',
        'purchase_order_item_id'                 => 'purchase_order_item_id',
        'purchase_order_uuid'                    => 'purchase_order_uuid',
        'product_uuid'                           => 'product_uuid',
        'purchase_order_item_text_short'         => 'purchase_order_item_text_short',
        'purchase_order_item_text_long'          => 'purchase_order_item_text_long',
        'quantityunit_uuid'                      => 'quantityunit_uuid',
        'purchase_order_item_quantity'           => 'purchase_order_item_quantity',
        'purchase_order_item_price'              => 'purchase_order_item_price',
        'purchase_order_item_price_total'        => 'purchase_order_item_price_total',
        'purchase_order_item_price_min_qntt'     => 'purchase_order_item_price_min_qntt',
        'purchase_order_item_time_create'        => 'purchase_order_item_time_create',
        'purchase_order_item_time_update'        => 'purchase_order_item_time_update',
        'purchase_order_item_order_priority'     => 'purchase_order_item_order_priority',
        'purchase_order_item_taxp'               => 'purchase_order_item_taxp',
        // JOIN
        'purchase_order_no'                      => 'purchase_order_no',
        'purchase_order_time_create'             => 'purchase_order_time_create',
        'purchase_order_time_create_unix'        => 'purchase_order_time_create_unix',
        'purchase_order_time_send'               => 'purchase_order_time_send',
        'purchase_order_time_update'             => 'purchase_order_time_update',
        'supplier_uuid'                          => 'supplier_uuid',
        'purchase_order_user_uuid_create'        => 'purchase_order_user_uuid_create',
        'purchase_order_user_uuid_update'        => 'purchase_order_user_uuid_update',
        'product_structure'                      => 'product_structure',
        'product_origin'                         => 'product_origin',
        'product_type'                           => 'product_type',
        'product_text_short'                     => 'product_text_short',
        'product_text_long'                      => 'product_text_long',
        'product_no_no'                          => 'product_no_no',
        'quantityunit_label'                     => 'quantityunit_label',
        'purchase_order_item_rating_uuid'        => 'purchase_order_item_rating_uuid',
        'purchase_order_item_rating_value'       => 'purchase_order_item_rating_value',
        'purchase_order_item_rating_comment'     => 'purchase_order_item_rating_comment',
        'purchase_order_item_rating_time_create' => 'purchase_order_item_rating_time_create',
    ];

    protected $primaryKey = 'purchase_order_item_uuid';

    public function getPurchaseOrderItemUuid(): string
    {
        if (!isset($this->storage['purchase_order_item_uuid'])) {
            return '';
        }
        return $this->storage['purchase_order_item_uuid'];
    }

    public function getPurchaseOrderItemId(): int
    {
        if (!isset($this->storage['purchase_order_item_id'])) {
            return 0;
        }
        return $this->storage['purchase_order_item_id'];
    }

    public function getPurchaseOrderUuid(): string
    {
        if (!isset($this->storage['purchase_order_uuid'])) {
            return '';
        }
        return $this->storage['purchase_order_uuid'];
    }

    public function getProductUuid(): string
    {
        if (!isset($this->storage['product_uuid'])) {
            return '';
        }
        return $this->storage['product_uuid'];
    }

    public function getPurchaseOrderItemTextShort(): string
    {
        if (!isset($this->storage['purchase_order_item_text_short'])) {
            return '';
        }
        return $this->storage['purchase_order_item_text_short'];
    }

    public function getPurchaseOrderItemTextLong(): string
    {
        if (!isset($this->storage['purchase_order_item_text_long'])) {
            return '';
        }
        return $this->storage['purchase_order_item_text_long'];
    }

    public function getQuantityunitUuid(): string
    {
        if (!isset($this->storage['quantityunit_uuid'])) {
            return '';
        }
        return $this->storage['quantityunit_uuid'];
    }

    public function getPurchaseOrderItemQuantity(): float
    {
        if (!isset($this->storage['purchase_order_item_quantity'])) {
            return 0;
        }
        return $this->storage['purchase_order_item_quantity'];
    }

    public function setPurchaseOrderItemQuantity(float $quantity): void
    {
        $this->storage['purchase_order_item_quantity'] = $quantity;
    }

    public function getPurchaseOrderItemPrice(): float
    {
        if (!isset($this->storage['purchase_order_item_price'])) {
            return 0;
        }
        return $this->storage['purchase_order_item_price'];
    }

    public function getPurchaseOrderItemPriceTotal(): float
    {
        if (!isset($this->storage['purchase_order_item_price_total'])) {
            return 0;
        }
        return $this->storage['purchase_order_item_price_total'];
    }

    public function setPurchaseOrderItemPriceTotal(float $itemPriceTotal): void
    {
        $this->storage['purchase_order_item_price_total'] = $itemPriceTotal;
    }

    public function getPurchaseOrderItemPriceMinQntt(): float
    {
        if (!isset($this->storage['purchase_order_item_price_min_qntt'])) {
            return 0;
        }
        return $this->storage['purchase_order_item_price_min_qntt'];
    }

    public function setPurchaseOrderItemPriceMinQntt(float $itemPriceMinQntt): void
    {
        $this->storage['purchase_order_item_price_min_qntt'] = $itemPriceMinQntt;
    }

    public function getPurchaseOrderItemTimeCreate(): string
    {
        if (!isset($this->storage['purchase_order_item_time_create'])) {
            return '';
        }
        return $this->storage['purchase_order_item_time_create'];
    }

    public function getPurchaseOrderItemTimeUpdate(): string
    {
        if (!isset($this->storage['purchase_order_item_time_update'])) {
            return '';
        }
        return $this->storage['purchase_order_item_time_update'];
    }

    public function getPurchaseOrderItemOrderPriority(): int
    {
        if (!isset($this->storage['purchase_order_item_order_priority'])) {
            return 0;
        }
        return $this->storage['purchase_order_item_order_priority'];
    }

    public function getPurchaseOrderItemTaxp(): int
    {
        if (!isset($this->storage['purchase_order_item_taxp'])) {
            return 0;
        }
        return $this->storage['purchase_order_item_taxp'];
    }

    public function getPurchaseOrderNo(): int
    {
        if (!isset($this->storage['purchase_order_no'])) {
            return 0;
        }
        return $this->storage['purchase_order_no'];
    }

    public function getPurchaseOrderTimeCreate(): string
    {
        if (!isset($this->storage['purchase_order_time_create'])) {
            return '';
        }
        return $this->storage['purchase_order_time_create'];
    }

    public function getPurchaseOrderTimeCreateUnix(): string
    {
        if (!isset($this->storage['purchase_order_time_create_unix'])) {
            return '';
        }
        return $this->storage['purchase_order_time_create_unix'];
    }

    public function getPurchaseOrderTimeSend(): string
    {
        if (!isset($this->storage['purchase_order_time_send'])) {
            return '';
        }
        return $this->storage['purchase_order_time_send'];
    }

    public function getPurchaseOrderTimeUpdate(): string
    {
        if (!isset($this->storage['purchase_order_time_update'])) {
            return '';
        }
        return $this->storage['purchase_order_time_update'];
    }

    public function getSupplierUuid(): string
    {
        if (!isset($this->storage['supplier_uuid'])) {
            return '';
        }
        return $this->storage['supplier_uuid'];
    }

    public function getPurchaseOrderUserUuidCreate(): string
    {
        if (!isset($this->storage['purchase_order_user_uuid_create'])) {
            return '';
        }
        return $this->storage['purchase_order_user_uuid_create'];
    }

    public function getPurchaseOrderUserUuidUpdate(): string
    {
        if (!isset($this->storage['purchase_order_user_uuid_update'])) {
            return '';
        }
        return $this->storage['purchase_order_user_uuid_update'];
    }

    public function getProductStructure(): string
    {
        if (!isset($this->storage['product_structure'])) {
            return '';
        }
        return $this->storage['product_structure'];
    }

    public function getProductOrigin(): string
    {
        if (!isset($this->storage['product_origin'])) {
            return '';
        }
        return $this->storage['product_origin'];
    }

    public function getProductType(): string
    {
        if (!isset($this->storage['product_type'])) {
            return '';
        }
        return $this->storage['product_type'];
    }

    public function getProductTextShort(): string
    {
        if (!isset($this->storage['product_text_short'])) {
            return '';
        }
        return $this->storage['product_text_short'];
    }

    public function getProductTextLong(): string
    {
        if (!isset($this->storage['product_text_long'])) {
            return '';
        }
        return $this->storage['product_text_long'];
    }

    public function getProductNoNo(): int
    {
        if (!isset($this->storage['product_no_no'])) {
            return 0;
        }
        return $this->storage['product_no_no'];
    }

    public function getQuantityunitLabel(): string
    {
        if (!isset($this->storage['quantityunit_label'])) {
            return '';
        }
        return $this->storage['quantityunit_label'];
    }

    public function getPurchaseOrderItemRatingUuid(): string
    {
        if (!isset($this->storage['purchase_order_item_rating_uuid'])) {
            return '';
        }
        return $this->storage['purchase_order_item_rating_uuid'];
    }

    public function getPurchaseOrderItemRatingValue(): int
    {
        if (!isset($this->storage['purchase_order_item_rating_value'])) {
            return 0;
        }
        return $this->storage['purchase_order_item_rating_value'];
    }

    public function getPurchaseOrderItemRatingComment(): string
    {
        if (!isset($this->storage['purchase_order_item_rating_comment'])) {
            return '';
        }
        return $this->storage['purchase_order_item_rating_comment'];
    }

    public function getPurchaseOrderItemRatingTimeCreate(): string
    {
        if (!isset($this->storage['purchase_order_item_rating_time_create'])) {
            return '';
        }
        return $this->storage['purchase_order_item_rating_time_create'];
    }
}

<?php

namespace Lerp\Purchase\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class PurchaseRequestItemEntity extends AbstractEntity
{
    public array $mapping = [
        'purchase_request_item_uuid'           => 'purchase_request_item_uuid',
        'purchase_request_item_id'             => 'purchase_request_item_id',
        'purchase_request_uuid'                => 'purchase_request_uuid',
        'product_uuid'                         => 'product_uuid',
        'purchase_request_item_text_short'     => 'purchase_request_item_text_short',
        'purchase_request_item_text_long'      => 'purchase_request_item_text_long',
        'purchase_request_item_quantity'       => 'purchase_request_item_quantity',
        'quantityunit_uuid'                    => 'quantityunit_uuid',
        'purchase_request_item_price'          => 'purchase_request_item_price',
        'purchase_request_item_price_min_qntt' => 'purchase_request_item_price_min_qntt',
        'purchase_request_item_time_create'    => 'purchase_request_item_time_create',
        'purchase_request_item_time_update'    => 'purchase_request_item_time_update',
        'purchase_request_item_order_priority' => 'purchase_request_item_order_priority',
        'purchase_request_no'                  => 'purchase_request_no',
        'purchase_request_time_create'         => 'purchase_request_time_create',
        'purchase_request_time_create_unix'    => 'purchase_request_time_create_unix',
        'purchase_request_time_send'           => 'purchase_request_time_send',
        'purchase_request_time_update'         => 'purchase_request_time_update',
        'supplier_uuid'                        => 'supplier_uuid',
        'purchase_request_user_uuid_create'    => 'purchase_request_user_uuid_create',
        'purchase_request_user_uuid_update'    => 'purchase_request_user_uuid_update',
        'product_structure'                    => 'product_structure',
        'product_origin'                       => 'product_origin',
        'product_type'                         => 'product_type',
        'product_no_no'                        => 'product_no_no',
        'quantityunit_label'                   => 'quantityunit_label',
        'quantityunit_resolution'              => 'quantityunit_resolution',
        'quantityunit_resolution_group'        => 'quantityunit_resolution_group',
    ];

    protected $primaryKey = 'purchase_request_item_uuid';

    public function getPurchaseRequestItemUuid(): string
    {
        if (!isset($this->storage['purchase_request_item_uuid'])) {
            return '';
        }
        return $this->storage['purchase_request_item_uuid'];
    }

    public function setPurchaseRequestItemUuid(string $purchaseRequestItemUuid): void
    {
        $this->storage['purchase_request_item_uuid'] = $purchaseRequestItemUuid;
    }

    public function getPurchaseRequestItemId(): int
    {
        if (!isset($this->storage['purchase_request_item_id'])) {
            return 0;
        }
        return $this->storage['purchase_request_item_id'];
    }

    public function setPurchaseRequestItemId(int $purchaseRequestItemId): void
    {
        $this->storage['purchase_request_item_id'] = $purchaseRequestItemId;
    }

    public function getPurchaseRequestUuid(): string
    {
        if (!isset($this->storage['purchase_request_uuid'])) {
            return '';
        }
        return $this->storage['purchase_request_uuid'];
    }

    public function setPurchaseRequestUuid(string $purchaseRequestUuid): void
    {
        $this->storage['purchase_request_uuid'] = $purchaseRequestUuid;
    }

    public function getProductUuid(): string
    {
        if (!isset($this->storage['product_uuid'])) {
            return '';
        }
        return $this->storage['product_uuid'];
    }

    public function setProductUuid(string $productUuid): void
    {
        $this->storage['product_uuid'] = $productUuid;
    }

    public function getPurchaseRequestItemTextShort(): string
    {
        if (!isset($this->storage['purchase_request_item_text_short'])) {
            return '';
        }
        return $this->storage['purchase_request_item_text_short'];
    }

    public function setPurchaseRequestItemTextShort(string $purchaseRequestItemTextShort): void
    {
        $this->storage['purchase_request_item_text_short'] = $purchaseRequestItemTextShort;
    }

    public function getPurchaseRequestItemTextLong(): string
    {
        if (!isset($this->storage['purchase_request_item_text_long'])) {
            return '';
        }
        return $this->storage['purchase_request_item_text_long'];
    }

    public function setPurchaseRequestItemTextLong(string $purchaseRequestItemTextLong): void
    {
        $this->storage['purchase_request_item_text_long'] = $purchaseRequestItemTextLong;
    }

    public function getPurchaseRequestItemQuantity(): float
    {
        if (!isset($this->storage['purchase_request_item_quantity'])) {
            return 0;
        }
        return $this->storage['purchase_request_item_quantity'];
    }

    public function setPurchaseRequestItemQuantity(float $purchaseRequestItemQuantity): void
    {
        $this->storage['purchase_request_item_quantity'] = $purchaseRequestItemQuantity;
    }

    public function getQuantityunitUuid(): string
    {
        if (!isset($this->storage['quantityunit_uuid'])) {
            return '';
        }
        return $this->storage['quantityunit_uuid'];
    }

    public function setQuantityunitUuid(string $quantityunitUuid): void
    {
        $this->storage['quantityunit_uuid'] = $quantityunitUuid;
    }

    public function getPurchaseRequestItemPrice(): float
    {
        if (!isset($this->storage['purchase_request_item_price'])) {
            return 0;
        }
        return $this->storage['purchase_request_item_price'];
    }

    public function setPurchaseRequestItemPrice(float $purchaseRequestItemPrice): void
    {
        $this->storage['purchase_request_item_price'] = $purchaseRequestItemPrice;
    }

    public function getPurchaseRequestItemPriceMinQntt(): float
    {
        if (!isset($this->storage['purchase_request_item_price_min_qntt'])) {
            return 0;
        }
        return $this->storage['purchase_request_item_price_min_qntt'];
    }

    public function setPurchaseRequestItemPriceMinQntt(float $purchaseRequestItemPriceMinQntt): void
    {
        $this->storage['purchase_request_item_price_min_qntt'] = $purchaseRequestItemPriceMinQntt;
    }

    public function getPurchaseRequestItemTimeCreate(): string
    {
        if (!isset($this->storage['purchase_request_item_time_create'])) {
            return '';
        }
        return $this->storage['purchase_request_item_time_create'];
    }

    public function setPurchaseRequestItemTimeCreate(string $purchaseRequestItemTimeCreate): void
    {
        $this->storage['purchase_request_item_time_create'] = $purchaseRequestItemTimeCreate;
    }

    public function getPurchaseRequestItemTimeUpdate(): string
    {
        if (!isset($this->storage['purchase_request_item_time_update'])) {
            return '';
        }
        return $this->storage['purchase_request_item_time_update'];
    }

    public function setPurchaseRequestItemTimeUpdate(string $purchaseRequestItemTimeUpdate): void
    {
        $this->storage['purchase_request_item_time_update'] = $purchaseRequestItemTimeUpdate;
    }

    public function getPurchaseRequestItemOrderPriority(): int
    {
        if (!isset($this->storage['purchase_request_item_order_priority'])) {
            return 0;
        }
        return $this->storage['purchase_request_item_order_priority'];
    }

    public function setPurchaseRequestItemOrderPriority(int $purchaseRequestItemOrderPriority): void
    {
        $this->storage['purchase_request_item_order_priority'] = $purchaseRequestItemOrderPriority;
    }

    public function getPurchaseRequestNo(): int
    {
        if (!isset($this->storage['purchase_request_no'])) {
            return 0;
        }
        return $this->storage['purchase_request_no'];
    }

    public function setPurchaseRequestNo(int $purchaseRequestNo): void
    {
        $this->storage['purchase_request_no'] = $purchaseRequestNo;
    }

    public function getPurchaseRequestTimeCreate(): string
    {
        if (!isset($this->storage['purchase_request_time_create'])) {
            return '';
        }
        return $this->storage['purchase_request_time_create'];
    }

    public function setPurchaseRequestTimeCreate(string $purchaseRequestTimeCreate): void
    {
        $this->storage['purchase_request_time_create'] = $purchaseRequestTimeCreate;
    }

    public function getPurchaseRequestTimeCreateUnix()
    {
        if (!isset($this->storage['purchase_request_time_create_unix'])) {
            return '';
        }
        return $this->storage['purchase_request_time_create_unix'];
    }

    public function setPurchaseRequestTimeCreateUnix($purchaseRequestTimeCreateUnix): void
    {
        $this->storage['purchase_request_time_create_unix'] = $purchaseRequestTimeCreateUnix;
    }

    public function getPurchaseRequestTimeSend(): string
    {
        if (!isset($this->storage['purchase_request_time_send'])) {
            return '';
        }
        return $this->storage['purchase_request_time_send'];
    }

    public function setPurchaseRequestTimeSend(string $purchaseRequestTimeSend): void
    {
        $this->storage['purchase_request_time_send'] = $purchaseRequestTimeSend;
    }

    public function getPurchaseRequestTimeUpdate(): string
    {
        if (!isset($this->storage['purchase_request_time_update'])) {
            return '';
        }
        return $this->storage['purchase_request_time_update'];
    }

    public function setPurchaseRequestTimeUpdate(string $purchaseRequestTimeUpdate): void
    {
        $this->storage['purchase_request_time_update'] = $purchaseRequestTimeUpdate;
    }

    public function getSupplierUuid(): string
    {
        if (!isset($this->storage['supplier_uuid'])) {
            return '';
        }
        return $this->storage['supplier_uuid'];
    }

    public function setSupplierUuid(string $supplierUuid): void
    {
        $this->storage['supplier_uuid'] = $supplierUuid;
    }

    public function getPurchaseRequestUserUuidCreate(): string
    {
        if (!isset($this->storage['purchase_request_user_uuid_create'])) {
            return '';
        }
        return $this->storage['purchase_request_user_uuid_create'];
    }

    public function setPurchaseRequestUserUuidCreate(string $purchaseRequestUserUuidCreate): void
    {
        $this->storage['purchase_request_user_uuid_create'] = $purchaseRequestUserUuidCreate;
    }

    public function getPurchaseRequestUserUuidUpdate(): string
    {
        if (!isset($this->storage['purchase_request_user_uuid_update'])) {
            return '';
        }
        return $this->storage['purchase_request_user_uuid_update'];
    }

    public function setPurchaseRequestUserUuidUpdate(string $purchaseRequestUserUuidUpdate): void
    {
        $this->storage['purchase_request_user_uuid_update'] = $purchaseRequestUserUuidUpdate;
    }

    public function getProductStructure(): string
    {
        if (!isset($this->storage['product_structure'])) {
            return '';
        }
        return $this->storage['product_structure'];
    }

    public function setProductStructure(string $productStructure): void
    {
        $this->storage['product_structure'] = $productStructure;
    }

    public function getProductOrigin(): string
    {
        if (!isset($this->storage['product_origin'])) {
            return '';
        }
        return $this->storage['product_origin'];
    }

    public function setProductOrigin(string $productOrigin): void
    {
        $this->storage['product_origin'] = $productOrigin;
    }

    public function getProductType(): string
    {
        if (!isset($this->storage['product_type'])) {
            return '';
        }
        return $this->storage['product_type'];
    }

    public function setProductType(string $productType): void
    {
        $this->storage['product_type'] = $productType;
    }

    public function getProductNoNo(): int
    {
        if (!isset($this->storage['product_no_no'])) {
            return 0;
        }
        return $this->storage['product_no_no'];
    }

    public function setProductNoNo(int $productNoNo): void
    {
        $this->storage['product_no_no'] = $productNoNo;
    }

    public function getQuantityunitLabel(): string
    {
        if (!isset($this->storage['quantityunit_label'])) {
            return '';
        }
        return $this->storage['quantityunit_label'];
    }

    public function setQuantityunitLabel(string $quantityunitLabel): void
    {
        $this->storage['quantityunit_label'] = $quantityunitLabel;
    }

    public function getQuantityunitResolution(): float
    {
        if (!isset($this->storage['quantityunit_resolution'])) {
            return 0;
        }
        return $this->storage['quantityunit_resolution'];
    }

    public function setQuantityunitResolution(float $quantityunitResolution): void
    {
        $this->storage['quantityunit_resolution'] = $quantityunitResolution;
    }

    public function getQuantityunitResolutionGroup(): string
    {
        if (!isset($this->storage['quantityunit_resolution_group'])) {
            return '';
        }
        return $this->storage['quantityunit_resolution_group'];
    }

    public function setQuantityunitResolutionGroup(string $quantityunitResolutionGroup): void
    {
        $this->storage['quantityunit_resolution_group'] = $quantityunitResolutionGroup;
    }
}

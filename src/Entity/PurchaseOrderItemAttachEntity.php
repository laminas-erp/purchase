<?php

namespace Lerp\Purchase\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

/**
 * Class PurchaseOrderItemAttachEntity
 * @package Lerp\Purchase\Entity
 * Use table view_purchase_order_item_attach.
 */
class PurchaseOrderItemAttachEntity extends AbstractEntity
{
    public array $mapping = [
        'purchase_order_item_attach_uuid' => 'purchase_order_item_attach_uuid',
        'purchase_order_item_uuid' => 'purchase_order_item_uuid',
        'product_uuid' => 'product_uuid',
        'purchase_order_item_attach_quantity' => 'purchase_order_item_attach_quantity',
        'quantityunit_uuid' => 'quantityunit_uuid',
        'purchase_order_item_attach_text_short' => 'purchase_order_item_attach_text_short',
        'purchase_order_item_attach_text_long' => 'purchase_order_item_attach_text_long',
        'purchase_order_item_attach_time_create' => 'purchase_order_item_attach_time_create',
        'purchase_order_item_attach_time_update' => 'purchase_order_item_attach_time_update',
        'product_text_short' => 'product_text_short',
        'product_text_part' => 'product_text_part',
        'product_structure' => 'product_structure',
        'product_no_no' => 'product_no_no',
        'quantityunit_label' => 'quantityunit_label',
        'quantityunit_resolution' => 'quantityunit_resolution',
        'quantityunit_resolution_group' => 'quantityunit_resolution_group',
        'purchase_order_item_text_short' => 'purchase_order_item_text_short',
        'purchase_order_uuid' => 'purchase_order_uuid',
        'purchase_order_no' => 'purchase_order_no',
        'purchase_order_time_create' => 'purchase_order_time_create',
        'purchase_order_time_create_unix' => 'purchase_order_time_create_unix',
        'purchase_order_time_send' => 'purchase_order_time_send',
        'purchase_order_time_update' => 'purchase_order_time_update',
        'supplier_uuid' => 'supplier_uuid',
        'purchase_order_user_uuid_create' => 'purchase_order_user_uuid_create',
        'purchase_order_user_uuid_update' => 'purchase_order_user_uuid_update',
    ];

    protected $primaryKey = 'purchase_order_item_attach_uuid';

    public function getPurchaseOrderItemAttachUuid(): string
    {
        if (!isset($this->storage['purchase_order_item_attach_uuid'])) {
            return '';
        }
        return $this->storage['purchase_order_item_attach_uuid'];
    }

    public function setPurchaseOrderItemAttachUuid(string $purchaseOrderItemAttachUuid): void
    {
        $this->storage['purchase_order_item_attach_uuid'] = $purchaseOrderItemAttachUuid;
    }

    public function getPurchaseOrderItemUuid(): string
    {
        if (!isset($this->storage['purchase_order_item_uuid'])) {
            return '';
        }
        return $this->storage['purchase_order_item_uuid'];
    }

    public function setPurchaseOrderItemUuid(string $purchaseOrderItemUuid): void
    {
        $this->storage['purchase_order_item_uuid'] = $purchaseOrderItemUuid;
    }

    public function getProductUuid(): string
    {
        if (!isset($this->storage['product_uuid'])) {
            return '';
        }
        return $this->storage['product_uuid'];
    }

    public function setProductUuid(string $productUuid): void
    {
        $this->storage['product_uuid'] = $productUuid;
    }

    public function getPurchaseOrderItemAttachQuantity(): float
    {
        if (!isset($this->storage['purchase_order_item_attach_quantity'])) {
            return 0;
        }
        return $this->storage['purchase_order_item_attach_quantity'];
    }

    public function setPurchaseOrderItemAttachQuantity(float $purchaseOrderItemAttachQuantity): void
    {
        $this->storage['purchase_order_item_attach_quantity'] = $purchaseOrderItemAttachQuantity;
    }

    public function getQuantityunitUuid(): string
    {
        if (!isset($this->storage['quantityunit_uuid'])) {
            return '';
        }
        return $this->storage['quantityunit_uuid'];
    }

    public function setQuantityunitUuid(string $quantityunitUuid): void
    {
        $this->storage['quantityunit_uuid'] = $quantityunitUuid;
    }

    public function getPurchaseOrderItemAttachTextShort(): string
    {
        if (!isset($this->storage['purchase_order_item_attach_text_short'])) {
            return '';
        }
        return $this->storage['purchase_order_item_attach_text_short'];
    }

    public function setPurchaseOrderItemAttachTextShort(string $purchaseOrderItemAttachTextShort): void
    {
        $this->storage['purchase_order_item_attach_text_short'] = $purchaseOrderItemAttachTextShort;
    }

    public function getPurchaseOrderItemAttachTextLong(): string
    {
        if (!isset($this->storage['purchase_order_item_attach_text_long'])) {
            return '';
        }
        return $this->storage['purchase_order_item_attach_text_long'];
    }

    public function setPurchaseOrderItemAttachTextLong(string $purchaseOrderItemAttachTextLong): void
    {
        $this->storage['purchase_order_item_attach_text_long'] = $purchaseOrderItemAttachTextLong;
    }

    public function getPurchaseOrderItemAttachTimeCreate(): string
    {
        if (!isset($this->storage['purchase_order_item_attach_time_create'])) {
            return '';
        }
        return $this->storage['purchase_order_item_attach_time_create'];
    }

    public function setPurchaseOrderItemAttachTimeCreate(string $purchaseOrderItemAttachTimeCreate): void
    {
        $this->storage['purchase_order_item_attach_time_create'] = $purchaseOrderItemAttachTimeCreate;
    }

    public function getPurchaseOrderItemAttachTimeUpdate(): string
    {
        if (!isset($this->storage['purchase_order_item_attach_time_update'])) {
            return '';
        }
        return $this->storage['purchase_order_item_attach_time_update'];
    }

    public function setPurchaseOrderItemAttachTimeUpdate(string $purchaseOrderItemAttachTimeUpdate): void
    {
        $this->storage['purchase_order_item_attach_time_update'] = $purchaseOrderItemAttachTimeUpdate;
    }

    public function getProductTextShort(): string
    {
        if (!isset($this->storage['product_text_short'])) {
            return '';
        }
        return $this->storage['product_text_short'];
    }

    public function setProductTextShort(string $productTextShort): void
    {
        $this->storage['product_text_short'] = $productTextShort;
    }

    public function getProductTextPart(): string
    {
        if (!isset($this->storage['product_text_part'])) {
            return '';
        }
        return $this->storage['product_text_part'];
    }

    public function setProductTextPart(string $productTextPart): void
    {
        $this->storage['product_text_part'] = $productTextPart;
    }

    public function getProductStructure(): string
    {
        if (!isset($this->storage['product_structure'])) {
            return '';
        }
        return $this->storage['product_structure'];
    }

    public function setProductStructure(string $productStructure): void
    {
        $this->storage['product_structure'] = $productStructure;
    }

    public function getProductNoNo(): int
    {
        if (!isset($this->storage['product_no_no'])) {
            return 0;
        }
        return $this->storage['product_no_no'];
    }

    public function setProductNoNo(int $productNoNo): void
    {
        $this->storage['product_no_no'] = $productNoNo;
    }

    public function getQuantityunitLabel(): string
    {
        if (!isset($this->storage['quantityunit_label'])) {
            return '';
        }
        return $this->storage['quantityunit_label'];
    }

    public function setQuantityunitLabel(string $quantityunitLabel): void
    {
        $this->storage['quantityunit_label'] = $quantityunitLabel;
    }

    public function getQuantityunitResolution(): float
    {
        if (!isset($this->storage['quantityunit_resolution'])) {
            return 0;
        }
        return $this->storage['quantityunit_resolution'];
    }

    public function setQuantityunitResolution(float $quantityunitResolution): void
    {
        $this->storage['quantityunit_resolution'] = $quantityunitResolution;
    }

    public function getQuantityunitResolutionGroup(): string
    {
        if (!isset($this->storage['quantityunit_resolution_group'])) {
            return '';
        }
        return $this->storage['quantityunit_resolution_group'];
    }

    public function setQuantityunitResolutionGroup(string $quantityunitResolutionGroup): void
    {
        $this->storage['quantityunit_resolution_group'] = $quantityunitResolutionGroup;
    }

    public function getPurchaseOrderItemTextShort(): string
    {
        if (!isset($this->storage['purchase_order_item_text_short'])) {
            return '';
        }
        return $this->storage['purchase_order_item_text_short'];
    }

    public function setPurchaseOrderItemTextShort(string $purchaseOrderItemTextShort): void
    {
        $this->storage['purchase_order_item_text_short'] = $purchaseOrderItemTextShort;
    }

    public function getPurchaseOrderUuid(): string
    {
        if (!isset($this->storage['purchase_order_uuid'])) {
            return '';
        }
        return $this->storage['purchase_order_uuid'];
    }

    public function setPurchaseOrderUuid(string $purchaseOrderUuid): void
    {
        $this->storage['purchase_order_uuid'] = $purchaseOrderUuid;
    }

    public function getPurchaseOrderNo(): int
    {
        if (!isset($this->storage['purchase_order_no'])) {
            return 0;
        }
        return $this->storage['purchase_order_no'];
    }

    public function setPurchaseOrderNo(int $purchaseOrderNo): void
    {
        $this->storage['purchase_order_no'] = $purchaseOrderNo;
    }

    public function getPurchaseOrderTimeCreate(): string
    {
        if (!isset($this->storage['purchase_order_time_create'])) {
            return '';
        }
        return $this->storage['purchase_order_time_create'];
    }

    public function setPurchaseOrderTimeCreate(string $purchaseOrderTimeCreate): void
    {
        $this->storage['purchase_order_time_create'] = $purchaseOrderTimeCreate;
    }

    public function getPurchaseOrderTimeCreateUnix()
    {
        if (!isset($this->storage['purchase_order_time_create_unix'])) {
            return '';
        }
        return $this->storage['purchase_order_time_create_unix'];
    }

    public function setPurchaseOrderTimeCreateUnix($purchaseOrderTimeCreateUnix): void
    {
        $this->storage['purchase_order_time_create_unix'] = $purchaseOrderTimeCreateUnix;
    }

    public function getPurchaseOrderTimeSend(): string
    {
        if (!isset($this->storage['purchase_order_time_send'])) {
            return '';
        }
        return $this->storage['purchase_order_time_send'];
    }

    public function setPurchaseOrderTimeSend(string $purchaseOrderTimeSend): void
    {
        $this->storage['purchase_order_time_send'] = $purchaseOrderTimeSend;
    }

    public function getPurchaseOrderTimeUpdate(): string
    {
        if (!isset($this->storage['purchase_order_time_update'])) {
            return '';
        }
        return $this->storage['purchase_order_time_update'];
    }

    public function setPurchaseOrderTimeUpdate(string $purchaseOrderTimeUpdate): void
    {
        $this->storage['purchase_order_time_update'] = $purchaseOrderTimeUpdate;
    }

    public function getSupplierUuid(): string
    {
        if (!isset($this->storage['supplier_uuid'])) {
            return '';
        }
        return $this->storage['supplier_uuid'];
    }

    public function setSupplierUuid(string $supplierUuid): void
    {
        $this->storage['supplier_uuid'] = $supplierUuid;
    }

    public function getPurchaseOrderUserUuidCreate(): string
    {
        if (!isset($this->storage['purchase_order_user_uuid_create'])) {
            return '';
        }
        return $this->storage['purchase_order_user_uuid_create'];
    }

    public function setPurchaseOrderUserUuidCreate(string $purchaseOrderUserUuidCreate): void
    {
        $this->storage['purchase_order_user_uuid_create'] = $purchaseOrderUserUuidCreate;
    }

    public function getPurchaseOrderUserUuidUpdate(): string
    {
        if (!isset($this->storage['purchase_order_user_uuid_update'])) {
            return '';
        }
        return $this->storage['purchase_order_user_uuid_update'];
    }

    public function setPurchaseOrderUserUuidUpdate(string $purchaseOrderUserUuidUpdate): void
    {
        $this->storage['purchase_order_user_uuid_update'] = $purchaseOrderUserUuidUpdate;
    }
}

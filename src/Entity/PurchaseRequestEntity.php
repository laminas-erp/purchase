<?php

namespace Lerp\Purchase\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class PurchaseRequestEntity extends AbstractEntity
{
    public array $mapping = [
        'purchase_request_uuid'        => 'purchase_request_uuid',
        'supplier_uuid'                => 'supplier_uuid',
        'purchase_request_no'          => 'purchase_request_no',
        'purchase_request_no_compl'    => 'purchase_request_no_compl',
        'purchase_request_time_create' => 'purchase_request_time_create',
        'purchase_request_time_update' => 'purchase_request_time_update',
        'user_uuid_create'             => 'user_uuid_create',
        'user_uuid_update'             => 'user_uuid_update',
        'purchase_request_lang_iso'    => 'purchase_request_lang_iso',
        'purchase_request_time_send'   => 'purchase_request_time_send',
        // for the first product in purchaseRequests
        'product_uuid'                 => 'product_uuid',
    ];

    protected $primaryKey = 'purchase_request_uuid';
    protected array $databaseFieldsInsert = ['purchase_request_uuid', 'supplier_uuid', 'purchase_request_no', 'purchase_request_no_compl', 'user_uuid_create'];

    public function getPurchaseRequestUuid(): string
    {
        if (!isset($this->storage['purchase_request_uuid'])) {
            return '';
        }
        return $this->storage['purchase_request_uuid'];
    }

    public function setPurchaseRequestUuid(string $purchaseRequestUuid): void
    {
        $this->storage['purchase_request_uuid'] = $purchaseRequestUuid;
    }

    public function getSupplierUuid(): string
    {
        if (!isset($this->storage['supplier_uuid'])) {
            return '';
        }
        return $this->storage['supplier_uuid'];
    }

    public function setSupplierUuid(string $supplierUuid): void
    {
        $this->storage['supplier_uuid'] = $supplierUuid;
    }

    public function getPurchaseRequestNo(): int
    {
        if (!isset($this->storage['purchase_request_no'])) {
            return 0;
        }
        return $this->storage['purchase_request_no'];
    }

    public function setPurchaseRequestNo(int $purchaseRequestNo): void
    {
        $this->storage['purchase_request_no'] = $purchaseRequestNo;
    }

    public function getPurchaseRequestNoCompl(): string
    {
        if (!isset($this->storage['purchase_request_no_compl'])) {
            return '';
        }
        return $this->storage['purchase_request_no_compl'];
    }

    public function setPurchaseRequestNoCompl(string $purchaseRequestNoCompl): void
    {
        $this->storage['purchase_request_no_compl'] = $purchaseRequestNoCompl;
    }

    public function getPurchaseRequestTimeCreate(): string
    {
        if (!isset($this->storage['purchase_request_time_create'])) {
            return '';
        }
        return $this->storage['purchase_request_time_create'];
    }

    public function setPurchaseRequestTimeCreate(string $purchaseRequestTimeCreate): void
    {
        $this->storage['purchase_request_time_create'] = $purchaseRequestTimeCreate;
    }

    public function getPurchaseRequestTimeUpdate(): string
    {
        if (!isset($this->storage['purchase_request_time_update'])) {
            return '';
        }
        return $this->storage['purchase_request_time_update'];
    }

    public function setPurchaseRequestTimeUpdate(string $purchaseRequestTimeUpdate): void
    {
        $this->storage['purchase_request_time_update'] = $purchaseRequestTimeUpdate;
    }

    public function getUserUuidCreate(): string
    {
        if (!isset($this->storage['user_uuid_create'])) {
            return '';
        }
        return $this->storage['user_uuid_create'];
    }

    public function setUserUuidCreate(string $userUuidCreate): void
    {
        $this->storage['user_uuid_create'] = $userUuidCreate;
    }

    public function getUserUuidUpdate(): string
    {
        if (!isset($this->storage['user_uuid_update'])) {
            return '';
        }
        return $this->storage['user_uuid_update'];
    }

    public function setUserUuidUpdate(string $userUuidUpdate): void
    {
        $this->storage['user_uuid_update'] = $userUuidUpdate;
    }

    public function getPurchaseRequestLangIso(): string
    {
        if (!isset($this->storage['purchase_request_lang_iso'])) {
            return '';
        }
        return $this->storage['purchase_request_lang_iso'];
    }

    public function setPurchaseRequestLangIso(string $purchaseRequestLangIso): void
    {
        $this->storage['purchase_request_lang_iso'] = $purchaseRequestLangIso;
    }

    public function getPurchaseRequestTimeSend(): string
    {
        if (!isset($this->storage['purchase_request_time_send'])) {
            return '';
        }
        return $this->storage['purchase_request_time_send'];
    }

    public function setPurchaseRequestTimeSend(string $purchaseRequestTimeSend): void
    {
        $this->storage['purchase_request_time_send'] = $purchaseRequestTimeSend;
    }

    public function getProductUuid(): string
    {
        if (!isset($this->storage['product_uuid'])) {
            return '';
        }
        return $this->storage['product_uuid'];
    }

    public function setProductUuid(string $productUuid): void
    {
        $this->storage['product_uuid'] = $productUuid;
    }
}

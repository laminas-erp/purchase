<?php

namespace Lerp\Purchase\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class PurchaseOrderEntity extends AbstractEntity
{
    public array $mapping = [
        'purchase_order_uuid'        => 'purchase_order_uuid',
        'supplier_uuid'              => 'supplier_uuid',
        'purchase_order_no'          => 'purchase_order_no',
        'purchase_order_no_compl'    => 'purchase_order_no_compl',
        'order_uuid'                 => 'order_uuid',
        'purchase_order_time_create' => 'purchase_order_time_create',
        'purchase_order_time_update' => 'purchase_order_time_update',
        'purchase_order_time_send'   => 'purchase_order_time_send',
        'user_uuid_create'           => 'user_uuid_create',
        'user_uuid_update'           => 'user_uuid_update',
        // for the first product in purchaseOrders
        'product_uuid'               => 'product_uuid',
    ];

    protected $primaryKey = 'purchase_order_uuid';
    protected array $databaseFieldsInsert = ['purchase_order_uuid', 'supplier_uuid', 'purchase_order_no', 'purchase_order_no_compl', 'order_uuid', 'user_uuid_create'];

    public function getPurchaseOrderUuid(): string
    {
        if (!isset($this->storage['purchase_order_uuid'])) {
            return '';
        }
        return $this->storage['purchase_order_uuid'];
    }

    public function setPurchaseOrderUuid(string $purchaseOrderUuid): void
    {
        $this->storage['purchase_order_uuid'] = $purchaseOrderUuid;
    }

    public function getSupplierUuid(): string
    {
        if (!isset($this->storage['supplier_uuid'])) {
            return '';
        }
        return $this->storage['supplier_uuid'];
    }

    public function setSupplierUuid(string $supplierUuid): void
    {
        $this->storage['supplier_uuid'] = $supplierUuid;
    }

    public function getPurchaseOrderNo(): int
    {
        if (!isset($this->storage['purchase_order_no'])) {
            return 0;
        }
        return $this->storage['purchase_order_no'];
    }

    public function setPurchaseOrderNo(int $purchaseOrderNo): void
    {
        $this->storage['purchase_order_no'] = $purchaseOrderNo;
    }

    public function getPurchaseOrderNoCompl(): string
    {
        if (!isset($this->storage['purchase_order_no_compl'])) {
            return '';
        }
        return $this->storage['purchase_order_no_compl'];
    }

    public function setPurchaseOrderNoCompl(string $purchaseOrderNoCompl): void
    {
        $this->storage['purchase_order_no_compl'] = $purchaseOrderNoCompl;
    }

    public function getOrderUuid(): string
    {
        if (!isset($this->storage['order_uuid'])) {
            return '';
        }
        return $this->storage['order_uuid'];
    }

    public function setOrderUuid(string $orderUuid): void
    {
        $this->storage['order_uuid'] = $orderUuid;
    }

    public function getPurchaseOrderTimeCreate(): string
    {
        if (!isset($this->storage['purchase_order_time_create'])) {
            return '';
        }
        return $this->storage['purchase_order_time_create'];
    }

    public function setPurchaseOrderTimeCreate(string $purchaseOrderTimeCreate): void
    {
        $this->storage['purchase_order_time_create'] = $purchaseOrderTimeCreate;
    }

    public function getPurchaseOrderTimeUpdate(): string
    {
        if (!isset($this->storage['purchase_order_time_update'])) {
            return '';
        }
        return $this->storage['purchase_order_time_update'];
    }

    public function setPurchaseOrderTimeUpdate(string $purchaseOrderTimeUpdate): void
    {
        $this->storage['purchase_order_time_update'] = $purchaseOrderTimeUpdate;
    }

    public function getPurchaseOrderTimeSend(): string
    {
        if (!isset($this->storage['purchase_order_time_send'])) {
            return '';
        }
        return $this->storage['purchase_order_time_send'];
    }

    public function setPurchaseOrderTimeSend(string $purchaseOrderTimeSend): void
    {
        $this->storage['purchase_order_time_send'] = $purchaseOrderTimeSend;
    }

    public function getUserUuidCreate(): string
    {
        if (!isset($this->storage['user_uuid_create'])) {
            return '';
        }
        return $this->storage['user_uuid_create'];
    }

    public function setUserUuidCreate(string $userUuidCreate): void
    {
        $this->storage['user_uuid_create'] = $userUuidCreate;
    }

    public function getUserUuidUpdate(): string
    {
        if (!isset($this->storage['user_uuid_update'])) {
            return '';
        }
        return $this->storage['user_uuid_update'];
    }

    public function setUserUuidUpdate(string $userUuidUpdate): void
    {
        $this->storage['user_uuid_update'] = $userUuidUpdate;
    }

    public function getProductUuid(): string
    {
        if (!isset($this->storage['product_uuid'])) {
            return '';
        }
        return $this->storage['product_uuid'];
    }

    public function setProductUuid(string $productUuid): void
    {
        $this->storage['product_uuid'] = $productUuid;
    }
}

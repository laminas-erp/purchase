<?php

namespace Lerp\Purchase\Entity;

use Bitkorn\Trinket\Validator\FloatValidator;
use Laminas\Validator\Uuid;

class PurchaseItemPriceEntity
{
    protected string $uuid;
    protected float $price;
    protected float $qntt;
    protected float $total;
    protected float $min;
    protected float $taxp;
    protected Uuid $uuidValidator;
    protected FloatValidator $floatValidator;

    /**
     * PurchaseItemPriceEntity constructor.
     */
    public function __construct()
    {
        $this->uuidValidator = new Uuid();
        $this->floatValidator = new FloatValidator();
        return $this;
    }

    /**
     * PurchaseItemPriceEntity constructor.
     * @param string $uuid
     * @param float $price
     * @param float $qntt Quantity
     * @param float $min
     * @param float $taxp
     * @return bool
     */
    public function compute(string $uuid, float $price, float $qntt, float $min, float $taxp): bool
    {
        $this->uuid = $uuid;
        $this->price = $price;
        $this->qntt = $qntt;
        $this->total = $this->price * $this->qntt;
        $this->min = $min;
        $this->taxp = $taxp;
        return $this->isValid();
    }

    /**
     * @param object $item Object with members: uuid, price, qntt, min, taxp
     * @return bool
     */
    public function computeArray(object $item): bool
    {
        if (!isset($item->uuid) || !isset($item->price) || !isset($item->qntt) || !isset($item->min) || !isset($item->taxp)) {
            return false;
        }
        $this->uuid = $item->uuid;
        $this->price = $item->price;
        $this->qntt = $item->qntt;
        $this->total = $this->price * $this->qntt;
        $this->min = $item->min;
        $this->taxp = $item->taxp;
        return $this->isValid();
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function getQntt(): float
    {
        return $this->qntt;
    }

    public function setQntt(float $qntt): void
    {
        $this->qntt = $qntt;
    }

    public function getTotal(): float
    {
        return $this->total;
    }

    public function setTotal(float $total): void
    {
        $this->total = $total;
    }

    public function getMin(): float
    {
        return $this->min;
    }

    public function setMin(float $min): void
    {
        $this->min = $min;
    }

    public function getTaxp(): float
    {
        return $this->taxp;
    }

    public function setTaxp(float $taxp): void
    {
        $this->taxp = $taxp;
    }

    public function isValid(): bool
    {
        return $this->uuidValidator->isValid($this->uuid)
            && $this->floatValidator->isValid($this->price)
            && $this->floatValidator->isValid($this->qntt)
            && $this->floatValidator->isValid($this->total)
            && $this->floatValidator->isValid($this->min)
            && $this->floatValidator->isValid($this->taxp);
    }
}

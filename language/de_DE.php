<?php
return [
    'can_not_delete_item_doc_exist' => 'Für dieses Item existiert ein Dokument. Es kann nicht gelöscht werden.',
    'can_not_add_item_root_already_send' => 'Es kann kein Item hinzu gefügt werden, weil schon versendet wurde.',
];

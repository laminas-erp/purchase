<?php
return [
    'can_not_delete_item_doc_exist' => 'A document exists for this item. It cannot be deleted.',
    'can_not_add_item_root_already_send' => 'No item can be added because it has already been sent.',
];
